<?php
$surl=isset($_GET['s'])?$_SERVER['REQUEST_URI'].'&':$_SERVER['REQUEST_URI'].'?';
if(isset($_GET['table']))
{
	$br='&table';
}
if(isset($_GET['map']))
{
	$br='&map';
}
if(isset($_GET['results']))
{
	$br='&results';
}
if(isset($_GET['locations']))
{
	$br1='&locations='.$_GET['locations'];
}
$surl=str_replace('sort=price','',$surl);
$surl=str_replace('sort=beds','',$surl);
$surl=str_replace('&sort=price','',$surl);
$surl=str_replace('&sort=beds','',$surl);
$surl=str_replace('&dir=low','',$surl);
$surl=str_replace('&dir=high','',$surl);
$surl=str_replace('&dir=all','',$surl);
$surl=str_replace('dir=all','',$surl);
$surl=str_replace('dir=high','',$surl);
$surl=str_replace('dir=low','',$surl);
$surl=str_replace('?map','',$surl);
$surl=str_replace('?table','',$surl);
$surl=str_replace('?results','',$surl);
$surl=str_replace('&map','',$surl);
$surl=str_replace('&table','',$surl);
$surl=str_replace('&results','',$surl);
$surl=str_replace('&locations=_kent','',$surl);
$surl=str_replace('&locations=_canterbury','',$surl);
$surl=str_replace('&locations=_ucl','',$surl);
$surl=str_replace('?locations=_kent','',$surl);
$surl=str_replace('?locations=_canterbury','',$surl);
$surl=str_replace('?locations=_ucl','',$surl);

?>
<div class="viewhead">
                <div class="viewheaddiv1">
                <div class="view1">View as:</div>
                <a class="view2" title="Table" href="<?php echo $surl ?><?php echo isset($_GET['s'])?'&':'' ?>table"></a>
                <a class="view3" title="Results" href="<?php echo $surl ?><?php echo isset($_GET['s'])?'&':'' ?>results"></a>
                <a class="view4" title="Map" href="<?php echo $surl ?><?php echo isset($_GET['s'])?'&':'' ?>map"></a>
                </div><!--viewheaddiv1-->
                <div class="viewheaddiv2">
                <div class="viewnew1" style="width:173px;">
                
                <select onchange="setLocation(this.value)"  name="locations">
  <option value="0">Which university/location?</option>
  <option <?php if($_GET['locations']=='_kent') { echo 'selected="selected"'; } ?> value="_kent">Kent University</option>
  <option <?php if($_GET['locations']=='_ucl') { echo 'selected="selected"'; } ?> value="_ucl">Canterbury Christ Church</option>
  <option <?php if($_GET['locations']=='_canterbury') { echo 'selected="selected"'; } ?> value="_canterbury">Canterbury Centre</option>
  <option <?php if($_GET['locations']=='_uca') { echo 'selected="selected"'; } ?> value="_uca">UCA Caterbury</option>
</select></div>
                <div  style="width:183px;" class="viewnew">
                 <select onchange="setLocationB(this.value)" style="font-size: 12px;height: 30px;width: 152;padding: 5px;float:left;"  name="locations">
  <option value="0">How Many Bedrooms?</option>
   <option value="1" <?php if($_GET['minbeds']=='1') { echo 'selected="selected"'; } ?>>1 Bedroom</option>
      <option value="2" <?php if($_GET['minbeds']=='2') { echo 'selected="selected"'; } ?>>2 Bedrooms</option>
      <option value="3" <?php if($_GET['minbeds']=='3') { echo 'selected="selected"'; } ?>>3 Bedrooms</option>
      <option value="4" <?php if($_GET['minbeds']=='4') { echo 'selected="selected"'; } ?>>4 Bedrooms</option>
      <option value="5" <?php if($_GET['minbeds']=='5') { echo 'selected="selected"'; } ?>>5 Bedrooms</option>
      <option value="6" <?php if($_GET['minbeds']=='6') { echo 'selected="selected"'; } ?>>6 Bedrooms</option>
</select>
                <a   class="view5" href="#"></a>
               
                </div><!--viewnew-->
                <div class="view6">
                
                <a id="view6-2" <?php echo ($_GET['dir']=='low')?'class="active"':'' ?> href="<?php echo $surl ?>sort=<?php echo $_GET['sort']=='price'?'price':'price' ?>&dir=low<?php echo $br; ?><?php echo $br1; ?>"></a>
                <a <?php echo ($_GET['dir']=='low')?'class="active"':'' ?> href="<?php echo $surl ?>sort=<?php echo $_GET['sort']=='price'?'price':'price' ?>&dir=low<?php echo $br; ?><?php echo $br1; ?>"><p>Price Low</p></a>
                <a id="view6-3" <?php echo ($_GET['dir']=='high')?'class="active"':'' ?> href="<?php echo $surl ?>sort=<?php echo $_GET['sort']=='price'?'price':'price' ?>&dir=high<?php echo $br; ?><?php echo $br1; ?>"></a>
                
                <a <?php echo ($_GET['dir']=='high')?'class="active"':'' ?> href="<?php echo $surl ?>sort=<?php echo $_GET['sort']=='price'?'price':'price' ?>&dir=high<?php echo $br; ?><?php echo $br1; ?>"><p>Price High</p></a>
                </div><!--view6-->
                <?php /*?><div class="view7"><a <?php echo ($_GET['dir']=='all')?'class="active"':'' ?> href="<?php echo $surl ?>sort=<?php echo $_GET['sort']=='price'?'price':'beds' ?>&dir=all<?php echo $br; ?><?php echo $br1; ?>" id="view7-1"></a>All</div><?php */?>
                </div><!--viewheaddiv2-->
                
                </div>
                
                <script type="text/javascript">
                function setLocation(val)
				{
					if(val!=0){
					document.location='<?php echo $surl?>sort=<?php echo $_GET['sort']=='price'?'price':'beds' ?>&dir=<?php echo $_GET['dir']=='high'?'high':'low' ?><?php echo $br?>&locations='+val;
					}
				}
                </script>