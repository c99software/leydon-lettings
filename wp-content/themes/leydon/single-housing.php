<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */



function assignPageTitle(){
	$_room_count = get_post_meta(get_the_ID(), '_rooms', true); 
$_postal_code = get_post_meta(get_the_ID(), '_postal_code', true);
$prop_type = wp_get_post_terms(get_the_ID(),'type');
if($prop_type[0]) { 
    $prop_holder = $prop_type[0]->name;
} 
if($prop_type[1]) {
    $prop_holder = $prop_holder.'/'.$prop_type[1]->name; 
}


$title_string = $_room_count.' bedroom '.$prop_holder.' House,'.$_postal_code;
  return $title_string ;    
}
add_filter('wp_title', 'assignPageTitle');

get_header(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
<div class="container vertical-shadow">




			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'housing' ); ?>
				<?php #twentythirteen_post_nav(); ?>
				<?php #comments_template(); ?>

			<?php endwhile; ?>
</div>
  <div id="testimonals">
            <div class="container">
                <h2> What did you say?</h2>
                <ul> 
                    <li>
                        <p class="quote">Dear Bob, Many thanks for the very comprehensive guidance. The advice in your email as well as the advice on your website were both very helpful in making the final decision. Indeed, the reason why I contacted your letting agency before any other...</p>
                        <p class="author">- W. N. (20/06/2013) </p>
                    </li>

                     <li>
                        <p class="quote">Thanks for this Bob.

            Being proactive, and ensuring tenants know how they can help reduce any risk, is something that we are also aiming to do, rather than just being reactive, and finding any issues once they are established.</p>
                        <p class="author">- Phil (22/12/13)</p>
                    </li>

                     <li>
                        <p class="quote">Hello, I was just e-mailing to say thanks for the prompt replacement and maintenance of our heating, myself and everyone else at 23 College Road really appreciate it.</p>
                        <p class="author">- Richard (13/02/2013)</p>
                    </li>

                     <li>
                        <p class="quote">Hello Bob We want to thank you SO much for going to our property at xxxxxxxx Road and assessing its suitability as a student rental. You gave us such an incredibly detailed report, we can see you are passionate about property and have lots of ideas on how to improve it. Your assessment has certainly helped us make a decision about what we should do we really appreciated your input so much, and thank you for it.</p>
                        <p class="author">John and Reena (23/12/2013)</p>
                    </li>
                     <li>
                        <p class="quote">Just wanted to let you know the lenders agreed to giving us a loan on xxxxx xxxxx Road and have accepted the tenancy contract as it is. So thank you and Bob for your advice when we needed it. Regards.</p>
                        <p class="author">- Cristina (Landlady) (15/112013) </p>
                    </li>

                     <li>
                        <p class="quote">You made a difficult and stressful situation a lot easier and I thank you for that. Perhaps if/when I need new students I call upon your services once again. Keep well and good luck with the 'rush' now on! With very best regards.</p>
                        <p class="author">- H. G. (Landlady) (16/8/2013)</p>
                    </li>
                     <li>
                        <p class="quote">I am really impressed. Yours is a really efficient and slick organisation with professional helpful staff.</p>
                        <p class="author">- Martin Minhall (Landlord) (22/10/2013)</p>
                    </li>
                    <li>
                        <p class="quote">I just wanted to thank you all as well, by far the best, friendliest letting agency I've met so far and a million times better than any of the ones I had dealings with today.</p>
                        <p class="author">- Josh Gardner (30/07/2013)</p>
                    </li>
                </ul>
            </div>
        </div>

        <?php 
            query_posts(array( 
                'post_type' => 'housing',
                'showposts' => 15
            ) );  
        ?>
       
        <div class="container housing_carousel">
            <div class="row">
                <div class="col-xs-12">
                    <h2> Your first choice accomodation </h2>
                    <ul style="">
                        <?php while (have_posts()) : the_post(); ?>
                            <?php if(get_the_post_thumbnail() != null): ?>
                                <?php 
                                $postcode = get_post_meta(get_the_ID(),'_postal_code',true);
                                $rooms = get_post_meta(get_the_ID(),'_rooms' , true); 
                                $tax = wp_get_post_terms(get_the_ID(),'type',array( 'fields' => 'slugs'));
                                $pcm = 4.3333333 * preg_replace("/[^0-9.]/","",get_post_meta(get_the_ID(),'_pcm' , true));
                                $pcm = number_format($pcm,0);
                                $preview = substr(get_the_content(),0,100).'...';

                                $ensuite = get_post_meta(get_the_ID(), '_en_suite', true);
                                $drive = get_post_meta(get_the_ID(),'_driveway' , true);
                                $bath = get_post_meta(get_the_ID(), '_shared_bath', true);
                                $disab = get_post_meta(get_the_ID(), '_Wheelchair', true);
                                $garden = get_post_meta(get_the_ID(), '_rear_garden', true);
                                $toilet_total = 0;
                                $toilet_total = $toilet_total + get_post_meta(get_the_ID(), '_shared_toilet', true);
                                
                                $ukct = get_post_meta(get_the_ID(),'_kentt' , true);
                                $cccut = get_post_meta(get_the_ID(),'_uclt' , true);
                                $ucat = get_post_meta(get_the_ID(),'_ucat' , true);
                                $citycentret = get_post_meta(get_the_ID(),'_canterburyt' , true);
                                $ukc = get_post_meta(get_the_ID(),'_kent' , true);
                                $cccu = get_post_meta(get_the_ID(),'_ucl' , true);
                                $uca = get_post_meta(get_the_ID(),'_uca' , true);
                                $citycentre = get_post_meta(get_the_ID(),'_canterbury' , true);

                                if (get_post_meta(get_the_ID(), '_toilet_ensuite_1', true) == "Yes")
                                    $toilet_total++;
                                if (get_post_meta(get_the_ID(), '_toilet_ensuite_2', true) == "Yes")
                                    $toilet_total++;
                                if (get_post_meta(get_the_ID(), '_toilet_ensuite_3', true) == "Yes")
                                    $toilet_total++;
                                if (get_post_meta(get_the_ID(), '_toilet_ensuite_4', true) == "Yes")
                                    $toilet_total++;
                                if (get_post_meta(get_the_ID(), '_toilet_ensuite_5', true) == "Yes")
                                    $toilet_total++;
                                if (get_post_meta(get_the_ID(), '_toilet_ensuite_6', true) == "Yes")
                                    $toilet_total++;
                                if (get_post_meta(get_the_ID(), '_toilet_ensuite_7', true) == "Yes")
                                    $toilet_total++;
                                if (get_post_meta(get_the_ID(), '_toilet_ensuite_8', true) == "Yes")
                                    $toilet_total++;
                                ?>
                                <li class="house room_<?php echo $rooms;?>">
                                    <div class="flipper">
                                        <div class="back">
                                            <div class="back-inner">
                                                <a href="javascript:void(0);" class="less-info"> </a>
                                                <h5>Description </h5>
                                                <p> <?php echo $preview; ?> </p>
                                                <h5>Where is it? </h5>
                                                <p>UKC - <?php echo $ukct; ?> <span>(<?php echo $ukc; ?>)</span> </p>
                                                <p>CCCU - <?php echo $cccut; ?> <span>(<?php echo $cccu; ?>)</span> </p>
                                                <p>UCA - <?php echo $ucat; ?> <span>(<?php echo $uca; ?>)</span> </p>
                                                <p>City Centre - <?php echo $citycentret; ?> <span>(<?php echo $citycentre; ?>)</span> </p>
                                                <a  class="glob_btn" href="<?php the_permalink() ?>">
                                                    See inside
                                                </a>
                                             </div>
                                        </div>
                                        <div class="front">
                                            <div class="thumb">
                                                <?php if($tax[0] == "professional" || $tax[1] == "professional"): ?>  
                                                <div class="house_type">
                                                    Professional 
                                                </div>
                                                <?php elseif($tax[0] == "student-only" || $tax[1] == "student-only"): ?>
                                                <div class="house_type">
                                                    Student
                                                </div>
                                                <?php elseif($tax[0] == "student-only" && $tax[1] == "professional"): ?>
                                                <div class="house_type">
                                                    Student/Professional
                                                </div>
                                                <?php elseif($tax[0] == "professional" && $tax[1] == "student-only"): ?>
                                                <div class="house_type">
                                                    Student/Professional
                                                </div>
                                                <?php else: ?>

                                                <?php endif; ?>
                                                <a href="<?php the_permalink() ?>">
                                                    <?php echo get_the_post_thumbnail();?>
                                                </a>
                                            </div>
                                            <div class="attributes">
                                                <div class="attr_toil <?php if($toilet_total > 0): echo 'active'; endif;?> col-xs-2">
                                                    <div class="tick">
                                                        <?php if($toilet_total > 0): echo $toilet_total; else: echo '&#10003;'; endif; ?>
                                                    </div>
                                                    <div class="tooltip tooltip_arrow_gg">
                                                         <?php if($toilet_total > 0): echo 'x'.$toilet_total; endif; ?> Toilet
                                                    </div>
                                                </div>
                                                <div class="attr_bath <?php if($bath > 0): echo 'active'; endif;?> col-xs-2">
                                                    <div class="tick">
                                                        <?php if($bath > 0): echo $bath; else: echo '&#10003;'; endif; ?>
                                                    </div>
                                                    <div class="tooltip tooltip_arrow_gg">
                                                        x<?php echo $bath; ?> Bath 
                                                    </div>
                                                </div>
                                                <div class="attr_shower <?php if($ensuite > 0): echo 'active'; endif;?> col-xs-2">
                                                    <?php if($ensuite > 0):?>
                                                    <div class="tick">
                                                        <?php echo $ensuite; ?>
                                                    </div>
                                                    <?php endif; ?>
                                                    <div class="tooltip tooltip_arrow_gg">
                                                        x<?php echo $ensuite; ?> Shower 
                                                    </div>
                                                </div>
                                                <div class="attr_park <?php if($garden == 'Yes'): echo 'active'; endif;?> col-xs-2">
                                                    <?php if($drive == 'Yes'):?>
                                                    <div class="tick">
                                                        <?php echo '&#10003;';?>
                                                    </div>
                                                    <?php endif; ?>
                                                    <div class="tooltip tooltip_arrow_gg">
                                                        Driveway
                                                    </div>
                                                </div>
                                                <div class="attr_garden <?php if($garden == 'Yes'): echo 'active'; endif;?> col-xs-2">
                                                    <?php if($garden == 'Yes'):?>
                                                    <div class="tick">
                                                        <?php echo '&#10003;';?>
                                                    </div>
                                                    <?php endif; ?>
                                                    <div class="tooltip tooltip_arrow_gg">
                                                        Garden 
                                                    </div>
                                                </div>
                                                 <div class="attr_disab <?php if($disab == 'Yes'): echo 'active'; endif;?> col-xs-2">
                                                    <?php if($disab == 'Yes'):?>
                                                    <div class="tick">
                                                        <?php echo '&#10003;';?>
                                                    </div>
                                                    <?php endif; ?>
                                                    <div class="tooltip tooltip_arrow_gg">
                                                        Disabled Access 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="details">
                                                <a href="<?php the_permalink() ?>">
                                                 <h2><?php echo $rooms; ?> Bedroom House</h2>
                                                 <h3> <?php echo $str_title = trim(preg_replace('/\s*\([^)]*\)/', '', get_the_title()));; ?>, <?php echo $postcode; ?> </h3>
                                                 <h4><span>&pound;<?php echo $pcm; ?></span>pcm</h4>

                                                </a>
                                                <a href="javascript:void(0);" class="more-info"> </a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>
                        <?php endwhile;?> 
                    </ul>
                </div>
            </div>
        </div>
	</div><!-- #content -->
</div><!-- #primary -->
<script>
jQuery('.housing_carousel ul').bxSlider({
  minSlides: 1,
  maxSlides: 15,
  pager: false,
  slideMargin: 20,
  slideWidth: 260,
});
jQuery('#testimonals ul').bxSlider({
  randomStart: 'true',
  slideMargin: 5,
  pager: false,
  auto: true,
  controls: false,
});

jQuery('.house .more-info').on('click',function(){
    jQuery(this).closest('.house').addClass('hover');
});
jQuery('.house .less-info').on('click',function(){
    jQuery(this).closest('.house').removeClass('hover');
});
</script>

<?php get_footer(); ?>