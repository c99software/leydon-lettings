<?php

/**

 * Template Name: TEMPLATE: HomePage

 * The main template file.

 *

 * This is the most generic template file in a WordPress theme and one of the

 * two required files for a theme (the other being style.css).

 * It is used to display a page when nothing more specific matches a query.

 * For example, it puts together the home page when no home.php file exists.

 *

 * Learn more: http://codex.wordpress.org/Template_Hierarchy

 *

 * @package WordPress

 * @subpackage Twenty_Thirteen

 * @since Twenty Thirteen 1.0

 */



get_header(); ?>

    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
            <div class="container-fluid hidden-xs">
                <div class="sliderbody row">
                    <div class="slider">
                        <div id="slider-div">
                            <?php echo do_shortcode("[metaslider id=271]"); ?>
                        </div><!--#slider div-->
                    </div>
                </div>
            </div>
            <div id="findbyroom_index" class="findbyroom " >
            <div class="container">
               <div class="row">
                    <ul class="list">
                        <li class="first room_me"><a href="/available-canterbury-student-accommodation/?room=1"><span class="icon"></span><span class="text"><span class="hidden-sm">Just4</span>Me</span></a></li>
                        <li class="room_1"><a href="/available-canterbury-student-accommodation/?room=1"><span class="icon"></span><span class="text">1 <span class="hidden-xs">Bed<span class="hidden-sm">room</span></span></span></a></li>
                        <li class="room_2"><a href="/available-canterbury-student-accommodation/?room=2"><span class="icon"></span><span class="text">2 <span class="hidden-xs">Bed<span class="hidden-sm">room</span></span></span></a></li>
                        <li class="room_3"><a href="/available-canterbury-student-accommodation/?room=3"><span class="icon"></span><span class="text">3 <span class="hidden-xs">Bed<span class="hidden-sm">room</span></span></span></a></li>
                        <li class="room_4"><a href="/available-canterbury-student-accommodation/?room=4"><span class="icon"></span><span class="text">4 <span class="hidden-xs">Bed<span class="hidden-sm">room</span></span></span></a></li>
                        <li class="room_5"><a href="/available-canterbury-student-accommodation/?room=5"><span class="icon"></span><span class="text">5 <span class="hidden-xs">Bed<span class="hidden-sm">room</span></span></span></a></li>
                        <li class="room_6"><a href="/available-canterbury-student-accommodation/?room=6"><span class="icon"></span><span class="text">6 <span class="hidden-xs">Bed<span class="hidden-sm">room</span></span></span></a></li>
                        <li class="room_7"><a href="/available-canterbury-student-accommodation/?room=7"><span class="icon"></span><span class="text">7 <span class="hidden-xs">Bed<span class="hidden-sm">room</span></span></span></a></li>
                        <li class="last map"><a href="/available-canterbury-student-accommodation/?view=3"><span class="text">Map</span><span class="icon"></span></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
            <div class="container no-padding">
                <div class=" row">
                    <div class="target-audience-container">
                        <div id="target-audience">
                            <div class="col-xs-6 col-sm-4">
                                <div class="frontpagefocus">
                                    <div class="thumb">
                                        <a href="/landlords-first-choice-canterbury-student-lettings-agency/">
                                            <img src="/wp-content/uploads/2014/07/landlords.jpg" alt="Landlords" title="Landlords" class="img-responsive" />
                                        </a>
                                    </div>
                                    <div class="expander">
                                        <a href="/landlords-first-choice-canterbury-student-lettings-agency/">
                                            <h4 class="hfocus">Landlords</h4>
                                            <p> The student lettings agency that goes the extra mile to be the first choice for landlords</p>
                                        </a>
                                    </div>
                                </div><!-- #Front Page Focus END -->

                            </div>

                            <div class="col-xs-6 col-sm-4">

      
                                <div class="frontpagefocus">
                                    <div class="thumb">
                                        <a href="/booking-process/">
                                            <img src="/wp-content/uploads/2014/07/prospective-tenants2.jpg" alt="Landlords" title="Landlords" class="img-responsive" />
                                        </a>
                                    </div>
                                    <div class="expander">
                                        <a href="/booking-process/">
                                            <h4 class="hfocus">Students</h4>
                                            <p> Quality accommodation based near the universities from the leading student letting agency in Canterbury</p>
                                        </a>
                                    </div>
                                </div><!-- #Front Page Focus END -->
                            </div>

                            <div class="col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-0">


                                <div class="frontpagefocus">
                                    <div class="thumb">
                                        <a href="/students-why-make-leydon-lettings-your-first-choice/">
                                            <img src="/wp-content/uploads/2014/07/current-tenants.jpg" alt="Landlords" title="Landlords" class="img-responsive" />
                                        </a>
                                    </div>
                                    <div class="expander">
                                        <a href="/students-why-make-leydon-lettings-your-first-choice/">
                                            <h4 class="hfocus">Professionals</h4>
                                            <p> Friendly and knowledgeable staff and over 27 years of experience renting accommodation in Canterbury </p>
                                        </a>
                                    </div>
                                </div><!-- #Front Page Focus END -->
                            </div>

                            <div class="clearBoth"></div>

                        </div><!-- #Target Audience #END -->

                    </div>
                </div>
            </div>
            <div class="home-container container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 home-post">
                        <?php the_content(); ?>
                        <a href="#">Find out more >></a>

                        
                    </div>
                    <div class="col-xs-12 col-sm-4 home-features">
                        <h2> Property Features </h2>
                        <ul>
                            <li class="col-xs-12 col-sm-6">Broadband/Fibre* </li>
                            <li class="col-xs-12 col-sm-6">Telephone Line* </li>
                            <li class="col-xs-12 col-sm-6">Bills-package*</li>
                            <li class="col-xs-12 col-sm-6">24/7 Maintenance*</li>
                            <li class="col-xs-12 col-sm-6">Free Gardening</li>  
                            <li class="col-xs-12 col-sm-6">Double Beds*</li>
                            <li class="col-xs-12 col-sm-6">Double Kitchens*</li>
                            <li class="col-xs-12 col-sm-6">Dishwashers*</li>
                            <li class="col-xs-12 col-sm-6">Washer/Dryers*</li>
                            <li class="col-xs-12 col-sm-6">Outside Storage*</li>
                            <li class="col-xs-12 col-sm-6">Car Parking*</li>
                        </ul>
                         <div class="twitter_timeline" style="margin-top:30px; float:left; width:100%;">
                     <a class="twitter-timeline"  href="https://twitter.com/leydonlettings" data-widget-id="686942318960721920">Tweets by @leydonlettings</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
          
                       
                        </div>
                    </div>
                     
                </div>
            </div>

                
            <div class="clearBoth"></div>

               

        </div>
        <div id="testimonals">
            <div class="container">
                <h2> What did you say?</h2>
                <ul> 
                    <li>
                        <p class="quote">Dear Bob, Many thanks for the very comprehensive guidance. The advice in your email as well as the advice on your website were both very helpful in making the final decision. Indeed, the reason why I contacted your letting agency before any other...</p>
                        <p class="author">- W. N. (20/06/2013) </p>
                    </li>

                     <li>
                        <p class="quote">Thanks for this Bob.

            Being proactive, and ensuring tenants know how they can help reduce any risk, is something that we are also aiming to do, rather than just being reactive, and finding any issues once they are established.</p>
                        <p class="author">- Phil (22/12/13)</p>
                    </li>

                     <li>
                        <p class="quote">Hello, I was just e-mailing to say thanks for the prompt replacement and maintenance of our heating, myself and everyone else at 23 College Road really appreciate it.</p>
                        <p class="author">- Richard (13/02/2013)</p>
                    </li>

                     <li>
                        <p class="quote">Hello Bob We want to thank you SO much for going to our property at xxxxxxxx Road and assessing its suitability as a student rental. You gave us such an incredibly detailed report, we can see you are passionate about property and have lots of ideas on how to improve it. Your assessment has certainly helped us make a decision about what we should do we really appreciated your input so much, and thank you for it.</p>
                        <p class="author">John and Reena (23/12/2013)</p>
                    </li>
                     <li>
                        <p class="quote">Just wanted to let you know the lenders agreed to giving us a loan on xxxxx xxxxx Road and have accepted the tenancy contract as it is. So thank you and Bob for your advice when we needed it. Regards.</p>
                        <p class="author">- Cristina (Landlady) (15/112013) </p>
                    </li>

                     <li>
                        <p class="quote">You made a difficult and stressful situation a lot easier and I thank you for that. Perhaps if/when I need new students I call upon your services once again. Keep well and good luck with the 'rush' now on! With very best regards.</p>
                        <p class="author">- H. G. (Landlady) (16/8/2013)</p>
                    </li>
                     <li>
                        <p class="quote">I am really impressed. Yours is a really efficient and slick organisation with professional helpful staff.</p>
                        <p class="author">- Martin Minhall (Landlord) (22/10/2013)</p>
                    </li>
                    <li>
                        <p class="quote">I just wanted to thank you all as well, by far the best, friendliest letting agency I've met so far and a million times better than any of the ones I had dealings with today.</p>
                        <p class="author">- Josh Gardner (30/07/2013)</p>
                    </li>
                </ul>
            </div>
        </div>

        <?php 
            $args =array( 
                'post_type' => 'housing',
                'showposts' => 15,
                'meta_key' => '_featured', 
                'meta_value' => 'Yes'                
            );  
            $query_2 = new WP_Query( $args );
        ?>
       
        <div class="container housing_carousel">
            <div class="row">
                <div class="col-xs-12">
                    <h2> Your first choice accomodation </h2>
                    <ul style="">
                        <?php 
                        while ($query_2->have_posts()): 
                            $query_2->the_post(); 
                            $currentid = get_the_ID();
                            $postcode = get_post_meta($currentid,'_postal_code',true);
                            $rooms = get_post_meta($currentid,'_rooms' , true); 
                            $tax = wp_get_post_terms($currentid,'type',array( 'fields' => 'slugs'));
                            $pcm = 4.3333333 * preg_replace("/[^0-9.]/","",get_post_meta($currentid,'_pcm' , true));
                            $pcm = number_format($pcm,0);
                            $preview2 = substr(get_the_content(),0,100).'...';


                            $ensuite = get_post_meta($currentid,'_shared_shower', true);
                            $drive = get_post_meta($currentid,'_driveway' , true);
                            $bath = get_post_meta($currentid, '_shared_bath', true);
                            $disab = get_post_meta($currentid, '_Wheelchair', true);
                            $garden = get_post_meta($currentid, '_rear_garden', true);
                            $toilet_total = 0;
                            $toilet_total = $toilet_total + get_post_meta($currentid, '_shared_toilet', true);
                            
                            $ukct2 = get_post_meta($currentid,'_kentt' , true);
                            $cccut2 = get_post_meta($currentid,'_uclt' , true);
                            $ucat2 = get_post_meta($currentid,'_ucat' , true);
                            $citycentret2 = get_post_meta($currentid,'_canterburyt' , true);
                            $ukc2 = get_post_meta($currentid,'_kent' , true);
                            $cccu2 = get_post_meta($currentid,'_ucl' , true);
                            $uca2 = get_post_meta($currentid,'_uca' , true);
                            $citycentre2 = get_post_meta($currentid,'_canterbury' , true);

                        ?> 
                            <?php if(get_the_post_thumbnail() != null): 

                                if (get_post_meta(get_the_ID(), '_toilet_ensuite_1', true) == "Yes")
                                    $toilet_total++;
                                if (get_post_meta(get_the_ID(), '_toilet_ensuite_2', true) == "Yes")
                                    $toilet_total++;
                                if (get_post_meta(get_the_ID(), '_toilet_ensuite_3', true) == "Yes")
                                    $toilet_total++;
                                if (get_post_meta(get_the_ID(), '_toilet_ensuite_4', true) == "Yes")
                                    $toilet_total++;
                                if (get_post_meta(get_the_ID(), '_toilet_ensuite_5', true) == "Yes")
                                    $toilet_total++;
                                if (get_post_meta(get_the_ID(), '_toilet_ensuite_6', true) == "Yes")
                                    $toilet_total++;
                                if (get_post_meta(get_the_ID(), '_toilet_ensuite_7', true) == "Yes")
                                    $toilet_total++;
                                if (get_post_meta(get_the_ID(), '_toilet_ensuite_8', true) == "Yes")
                                    $toilet_total++;
                                ?>
                                <li class="house room_<?php echo $rooms;?>">
                                    <div class="flipper">
                                        <div class="back">
                                            <div class="back-inner">
                                                <a href="javascript:void(0);" class="less-info"> </a>
                                                <h5>Description </h5>
                                                <p> <?php echo $preview2; ?> </p>
                                                <h5>Where is it? </h5>
                                                <p>UKC - <?php echo $ukct2; ?> <span>(<?php echo $ukc2; ?>)</span> </p>
                                                <p>CCCU - <?php echo $cccut2; ?> <span>(<?php echo $cccu2; ?>)</span> </p>
                                                <p>UCA - <?php echo $ucat2; ?> <span>(<?php echo $uca2; ?>)</span> </p>
                                                <p>City Centre - <?php echo $citycentret2; ?> <span>(<?php echo $citycentre2; ?>)</span> </p>
                                                <a  class="glob_btn" href="<?php the_permalink() ?>">
                                                    See inside
                                                </a>
                                             </div>
                                        </div>
                                        <div class="front">
                                            <div class="thumb">
                                                <?php if($tax[0] == "professional" || $tax[1] == "professional"): ?>  
                                                <div class="house_type">
                                                    Professional 
                                                </div>
                                                <?php elseif($tax[0] == "student-only" || $tax[1] == "student-only"): ?>
                                                <div class="house_type">
                                                    Student
                                                </div>
                                                <?php elseif($tax[0] == "student-only" && $tax[1] == "professional"): ?>
                                                <div class="house_type">
                                                    Student/Professional
                                                </div>
                                                <?php elseif($tax[0] == "professional" && $tax[1] == "student-only"): ?>
                                                <div class="house_type">
                                                    Student/Professional
                                                </div>
                                                <?php else: ?>

                                                <?php endif; ?>
                                                <a href="<?php the_permalink() ?>">
                                                    <?php echo get_the_post_thumbnail();?>
                                                </a>
                                            </div>
                                            <div class="attributes">
                                                <div class="attr_toil <?php if($toilet_total > 0): echo 'active'; endif;?> col-xs-2">
                                                    <div class="tick">
                                                        <?php if($toilet_total > 0): echo $toilet_total; else: echo '&#10003;'; endif; ?>
                                                    </div>
                                                    <div class="tooltip tooltip_arrow_gg">
                                                         <?php if($toilet_total > 0): echo 'x'.$toilet_total; endif; ?> Toilet
                                                    </div>
                                                </div>
                                                <div class="attr_bath <?php if($bath > 0): echo 'active'; endif;?> col-xs-2">
                                                    <div class="tick">
                                                        <?php if($bath > 0): echo $bath; else: echo '&#10003;'; endif; ?>
                                                    </div>
                                                    <div class="tooltip tooltip_arrow_gg">
                                                        x<?php echo $bath; ?> Bath 
                                                    </div>
                                                </div>
                                                <div class="attr_shower <?php if($ensuite > 0): echo 'active'; endif;?> col-xs-2">
                                                    <?php if($ensuite > 0):?>
                                                    <div class="tick">
                                                        <?php echo $ensuite; ?>
                                                    </div>
                                                    <?php endif; ?>
                                                    <div class="tooltip tooltip_arrow_gg">
                                                        x<?php echo $ensuite; ?> Shower 
                                                    </div>
                                                </div>
                                                <div class="attr_park <?php if($garden == 'Yes'): echo 'active'; endif;?> col-xs-2">
                                                    <?php if($drive == 'Yes'):?>
                                                    <div class="tick">
                                                        <?php echo '&#10003;';?>
                                                    </div>
                                                    <?php endif; ?>
                                                    <div class="tooltip tooltip_arrow_gg">
                                                        Driveway
                                                    </div>
                                                </div>
                                                <div class="attr_garden <?php if($garden == 'Yes'): echo 'active'; endif;?> col-xs-2">
                                                    <?php if($garden == 'Yes'):?>
                                                    <div class="tick">
                                                        <?php echo '&#10003;';?>
                                                    </div>
                                                    <?php endif; ?>
                                                    <div class="tooltip tooltip_arrow_gg">
                                                        Garden 
                                                    </div>
                                                </div>
                                                 <div class="attr_disab <?php if($disab == 'Yes'): echo 'active'; endif;?> col-xs-2">
                                                    <?php if($disab == 'Yes'):?>
                                                    <div class="tick">
                                                        <?php echo '&#10003;';?>
                                                    </div>
                                                    <?php endif; ?>
                                                    <div class="tooltip tooltip_arrow_gg">
                                                        Disabled Access 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="details">
                                                <a href="<?php the_permalink() ?>">
                                                 <h2><?php echo $rooms; ?> Bedroom House</h2>
                                                 <h3> <?php echo $str_title = trim(preg_replace('/\s*\([^)]*\)/', '', get_the_title()));; ?>, <?php echo $postcode; ?> </h3>
                                                 <h4><span>&pound;<?php echo $pcm; ?></span>pcm</h4>

                                                </a>
                                                <a href="javascript:void(0);" class="more-info"> </a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endif; ?>
                        <?php endwhile;?> 
                    </ul>
                </div>
            </div>
        </div>
        <div class="clearBoth"></div>

        <br/>

        <script>
        (function($) {
             $('.navbar .findbyroom').hide();
            $(document).ready(function() {
              
            });
            var distance = $('#findbyroom_index').offset().top,
            $window = $(window);
            $window.scroll(function() {
                if ( $window.scrollTop() >= distance ) {
                    $('.navbar .findbyroom').show();
                }
                else
                {
                    $('.navbar .findbyroom').hide();
                }
            });

        })(jQuery);


        jQuery('.house .more-info').on('click',function(){
            jQuery(this).closest('.house').addClass('hover');
        });
        jQuery('.house .less-info').on('click',function(){
            jQuery(this).closest('.house').removeClass('hover');
        });
            jQuery('#testimonals ul').bxSlider({
              randomStart: 'true',
              slideMargin: 5,
              pager: false,
              auto: true,
              controls: false,
              pause: 7000,
            });
            jQuery('.housing_carousel ul').bxSlider({
              minSlides: 1,
              maxSlides: 15,
              pager: false,
              slideMargin: 20,
              slideWidth: 260,
            });
        
           
        </script>

    </div><!-- #content END-->

</div><!-- #primary END-->

</div><!-- #main END-->

<?php get_footer(); ?>