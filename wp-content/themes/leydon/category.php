<?php
/**
 * The template for displaying Category pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<div class="container page_style">
	<div class="row">
        <?php RethinkBreadcrumb();?>
    </div>
	<div class="row">
		<div class="col-xs-12 col-sm-8">
			<h1 class="category-title"> <?php single_cat_title(); ?></h1> 
		  	<?php if ( have_posts() ) : ?>
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
			<div class="post-div">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>	
					<header class="entry-header">
						
						<?php if ( is_single() ) : ?>
						<h2 class="entry-title"><?php the_title(); ?></h2>
						<?php else : ?>
						<h2 class="entry-title">
							<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
						</h2>
						<?php endif; // is_single() ?>
					</header><!-- .entry-header -->
				  	<div class="entry-content">
				  	 	<?php the_excerpt(); ?>
				  	 	<div class="entry-meta">
							<?php leydon_entry_meta(); ?>
							<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
						</div><!-- .entry-meta -->
					  	<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
						  <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail(); ?></a>
						</div>
						<?php endif; ?>

				  
				  	</div><!-- .entry-content -->
		   		</article><!-- #post -->
			</div>
			<?php endwhile; ?>

			<?php twentythirteen_paging_nav(); ?>

			<?php else : ?>
				<?php get_template_part( 'content', 'none' ); ?>
			<?php endif; ?>
		</div><!--innerpage-->
		<?php get_sidebar('sidebar-1'); ?>
	</div><!--contentallign-->
</div><!--contentallign-->


<?php get_footer();?></div><!--content-->