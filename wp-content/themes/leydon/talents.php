<?php /* Template Name: TEMPLATE: Current Tenants  */ get_header(); ?>






<div class="container page_style">
	<div class="row">
        <?php RethinkBreadcrumb();?>
    </div>
	<div class="row">
		
		<div class="col-xs-12 col-sm-8">

<?php /* The loop */ ?>

			<?php while ( have_posts() ) : the_post(); ?>



				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				  <div id="page-slider"><?php echo do_shortcode("[metaslider id=3741]"); ?></div>



<h1 class="innerpagehead"><?php the_title(); ?></h1>

					<div class="entry-content">

						<?php the_content(); ?>

						

					</div><!-- .entry-content -->



					

				</article><!-- #post -->



				

			<?php endwhile; ?>

</div><!--innerpage-->

<?php get_sidebar('talents'); ?>

</div><!--contentallign-->





<?php get_footer();?></div><!--content-->