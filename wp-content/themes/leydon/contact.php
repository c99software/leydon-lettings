<?php /* Template Name: TEMPLATE: Contact  */ get_header(); ?>


<div class="container page_style">
	<div class="row">
        <?php RethinkBreadcrumb();?>
    </div>
	<div class="row">
		
		<div class="col-xs-12 col-sm-8">
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div id="page-slider"><?php echo do_shortcode("[metaslider id=3743]"); ?></div>
				  <h5 class="innerpagehead"><?php the_title(); ?></h5>

					<div class="entry-content">
						<?php the_content(); ?>
						
					</div><!-- .entry-content -->

					
				</article><!-- #post -->

				
			<?php endwhile; ?>

		</div><!--innerpage-->
		<?php get_sidebar('contact'); ?>
	</div>
</div><!--contentallign-->


<?php get_footer();?></div><!--content-->