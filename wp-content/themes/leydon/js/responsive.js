/**
 * Created by Huankai on 16/07/14.
 */
$(document).ready(function(){
        $("#r-navbar-collapse-2 ul > li.menu-item-has-children").addClass("dropdown");
        $(".dropdown > a").addClass('dropdown-toggle').attr('data-toggle','dropdown');
        $(".dropdown > ul").addClass('dropdown-menu').addClass('dropdown-menu-1');

        $("#r-navbar-collapse-2 ul > li.menu-item-has-children > ul.dropdown-menu-1 > li").addClass("dropdown-submenu");
        $(".dropdown-submenu").find(".dropdown-submenu").removeClass("dropdown-submenu");
        $(".dropdown-submenu").find(".dropdown-menu-1").removeClass("dropdown-menu-1");
        var $shower_count=$("#tabs-3 .ensuite_shower_count").text();
        $("#tabs-2 .house-info .sharedshower").find("p:eq(1)").remove();
        /*$('.carousel').carousel('interval : 9999'); */
        var sidebarheight = $(".subpage_outer").parent().height();
        $(".subpage_outer").height(sidebarheight);
    }
)
