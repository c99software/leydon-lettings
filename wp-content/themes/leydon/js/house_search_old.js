$ = jQuery;

var houseSearch = $('#house_search');
var houseResults = $('#house_results');
var searchForm = houseSearch.find('form');
var roomoverwrite = 0;
var roomcanover = 0;
var location_holder = 0;
var viewoverwrite = 0;
var viewcanover = 0;
function loading_results(){
	$('#house_results .results_overlay').show();
}
function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}  

var master_store ;
var nextAddress = 0;
var geocoder;
var map;
var addresses=[];
var bounds;
var latlng
var lat_cache;
var lng_cache; 
var markerCount = 0;
var markers = [];
var last_zoom;
var geocoder;
var map;
$.getJSON( "/wp-content/themes/leydon/new_map_data.json", function(data) {
                    master_store = data;
                  
});


function start_map(){
    geocoder = new google.maps.Geocoder();
    latlng = new google.maps.LatLng(51.280277,1.079282);
    var styles = [
        {
            featureType: 'poi.school',
            elementType: 'geometry.fill',
            stylers: [
                { color: '#4d6278'},
                { lightness: '50'}
            ]
        },
        {
            featureType: 'poi.school',
            elementType: 'geometry.stroke',
            stylers: [
                { color: '#4d6278'}
            ]
        },
    ];
    var mapOptions = {
         mapTypeControlOptions: {
                mapTypeIds: ['Styled']
            },
        zoom: 14,
        center: latlng,
        mapTypeId: 'Styled'
    }
    map = new google.maps.Map(document.getElementById('mapview'), mapOptions);
    bounds = new google.maps.LatLngBounds();
    var styledMapType = new google.maps.StyledMapType(styles, { name: 'Styled' });
    map.mapTypes.set('Styled', styledMapType);
    theNext();


}


// delay between geocode requests - at the time of writing, 100 miliseconds seems to work well
var delay = 100;


// ====== Create map objects ======
var infowindow = new google.maps.InfoWindow();

//  var geo = new google.maps.Geocoder();
//  var map = new google.maps.Map(document.getElementById("mapview"), mapOptions);
// var bounds = new google.maps.LatLngBounds();

// ====== Geocoding ======
function success(){}
function findPurpose(purposeName) {
        for (var i = 0, len = master_store.length; i < len; i++) {
            if (master_store[i]["address"]["key"] == purposeName){
                lat_cache =  master_store[i]["address"]["lat"];
                lng_cache =  master_store[i]["address"]["lng"];
                return true;
            }                 
        }
        return false;
}

function getAddress(searchi, next) {
    
    var cache_check = false;
    var postcode = searchi.postcode;
    var key = postcode.replace(/ /g,'');
    nextAddress++;
   	var pcm=4.3333333*searchi.pcm;

   	var marker_content ='<div class="infoout"><img src="'+searchi.thumb[0]+'" height="100" style="float:left; margin-right:10px;" /><p class="priceval" id="gridhead">£'+pcm.toFixed(2)+'pcm</p><p id="gridsubhead">'+searchi.rooms+'bedroom House<br />'+searchi.address+','+searchi.postcode+'<div class="gridbtn"><a href="'+searchi.permalink+'" class="gridb1">View full detals</a></div></div></div>';
		
    var check_cache = findPurpose(key);
    if(check_cache) { 

        createMarker(searchi.postcode,lat_cache,lng_cache,marker_content,searchi.marker);                        
        next();
    
    } 
    else
    {
    geocoder.geocode({address:searchi.postcode}, function (results,status)
        {


            //console.log(searchi.postcode);
            // If that was successful
            if (status == google.maps.GeocoderStatus.OK) {
                // Lets assume that the first marker is the one we want
                var p = results[0].geometry.location;
                var lat=p.lat();
                var lng=p.lng();
                // Output the data
                var msg = 'address="' + searchi.postcode+ '" lat=' +lat+ ' lng=' +lng+ '(delay='+delay+'ms)<br>';
                //   console.log(msg)
                // Create a mamsgrker
                createMarker(searchi.postcode,lat,lng,marker_content,searchi.marker);


                var cache_data = [];
                cache_data[0] = searchi.postcode+"key_lat";
                cache_data[1] = searchi.postcode+"key_lng";
                cache_data[2] = lat=p.lat();
                cache_data[3] = lng=p.lng();

                var temp_store = 
                    {
                    "address":
                        {
                            "lat":[lat=p.lat()],
                            "lng":[temp_storelng=p.lng()],
                            "key":[key]
                        }
                    }

                
                master_store = $.merge(master_store,[temp_store])
                
                var to_send = JSON.stringify(master_store);
                
                if (temp_store != null){
                       
                    $.ajax({
                          type: "POST",
                          url: "/wp-content/themes/leydon/json.php",
                          data: to_send,
                          success: success,
                           dataType: "json"
                        });
                }
                
            }
            // ====== Decode the error status ======
            else {
                // === if we were sending the requests to fast, try this one again and increase the delay
                if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                    var reason="Code "+status;
                    var msg = 'address="' + searchi.postcode + '" error=' +reason+ '(delay='+delay+'ms)<br>';
                    //console.log(msg)
                    delay++;
                } else {
                    var reason="Code "+status;
                    var msg = 'address="' + searchi.postcode + '" error=' +reason+ '(delay='+delay+'ms)<br>';
                    //console.log(msg)
                    // document.getElementById("messages").innerHTML += msg;
                }
            }
            next();
        }

    );
    }
}

// ======= Function to create a marker
function createMarker(add,lat,lng,infoss,image) {

    var contentString = infoss;
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat,lng),
        map: map,
        icon: image,
        zIndex: Math.round(latlng.lat()*-100000)<<5
    }); 
    markers[markerCount] = marker;


    //when the map zoom changes, resize the icon based on the zoom level so the marker covers the same geographic area
    


    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(contentString);
        infowindow.open(map,marker);
    });

    bounds.extend(marker.position);
    markerCount++;


 
}





// ======= Function to call the next Geocode operation when the reply comes back

function theNext() {
    if (nextAddress < addresses.length) {

        getAddress(addresses[nextAddress],theNext);


        // console.log(nextAddress)
    } else {
        // We're done. Show map bounds
        //   map.fitBounds(bounds);
    }
}
//	setTimeout(function() { theNext(); }, 1000);
// ======= Call that function for the first time =======


// This Javascript is based on code provided by the
// Community Church Javascript Team
// http://www.bisphamchurch.org.uk/
// http://econym.org.uk/gmap/

//]]>




searchForm.submit(function(e){
	e.preventDefault();

	var has_ensuite = 0;
	if(houseSearch.find('#has_ensuite').prop("checked"))
		has_ensuite = 1;
	var has_wheel = 0;
	if(houseSearch.find('#has_wheel').prop("checked"))
		has_wheel = 1;
	var has_parking = 0;
	if(houseSearch.find('#has_parking').prop("checked"))
		has_parking = 1;
	var has_garden = 0;
	if(houseSearch.find('#has_garden').prop("checked"))
		has_garden = 1;
	var has_bathroom = 0;
	if(houseSearch.find('#has_bathroom').prop("checked"))
		has_bathroom = 1;
	var has_toilet = 0;
	if(houseSearch.find('#has_toilet').prop("checked"))
		has_toilet = 1;
	var avail_this = 0;
	if(houseSearch.find('#avail_this').prop("checked"))
		avail_this = 1;

	var avail_next = 0;
	if(houseSearch.find('#avail_next').prop("checked"))
		avail_next = 1;

	var viewTemp = $('#view_input').val();
	var per_page = $('#per_page_select').val();
	var sort_order = $('#sort_select').val();

	viewoverwrite = getUrlParameter("view");
	if (viewoverwrite != null){
		if(viewcanover != '1'){
			viewTemp = viewoverwrite;
		}
	}

	roomoverwrite = getUrlParameter("room");
	location_holder = getUrlParameter("loc");
	var roomTemp =  houseSearch.find('#room_count').val();
	if (roomoverwrite != null && roomcanover != 1){
		roomTemp = roomoverwrite;
		console.log(roomTemp);
	}
	var data = {
		action : "house_search",
		rooms: roomTemp,
		tag: location_holder,
		lettype: houseSearch.find('#let_type').val(),
		ensuite: has_ensuite,
		wheel: has_wheel,
		parking: has_parking,
		garden: has_garden,
		bathroom: has_bathroom,
		toilet: has_toilet,
		view: viewTemp,
		availthis: avail_this,
		availnext: avail_next,
		perpage: per_page,
		sort: sort_order,
		location: houseSearch.find('#location').val(),
	};
	
	$.ajax({
		url : ajax_url,
		beforeSend: loading_results(),
		data : data,
		success : function(response){
			houseResults.find('#list_results #house_results_container').empty();
			houseResults.find('#grid_results ul').empty();
			houseResults.find('#mapview').empty();
			houseResults.find('#mapview').hide();
			houseResults.find('#map_key').hide();
			$('#search_message').empty();
			if (response == null){
				
				var html = '<h2> No Properties were found. </h3>';
				
				$('#search_message').append(html);
				
				$('#house_results .results_overlay').hide();

				return;
			}
			$('#distance_from').empty();
			var distance_type = $('#location').val();
				var distance_get = "";
				if(distance_type == "1" || distance_type == "0" ){
					distance_get = "ukc";
					$('#distance_from').text('UKC');
				}
				if(distance_type == "2"){
					distance_get = "cccu";
					$('#distance_from').text('CCCU');
				}
				if(distance_type == "3"){
					distance_get = "uca";
					$('#distance_from').text('UCA');
				}
				if(distance_type == "4"){
					distance_get = "citycentre";					
					$('#distance_from').text('City Centre');
			}
			for(var i = 0; i < response.length; i++){

				var av_this_counter = 0;
		        var av_next_counter = 0;
		        if(viewTemp == 1){  
					var html = '<div class="flipTable_item room_'+response[i].rooms+'"><a class="overlay_link" href="'+response[i].permalink+'"> </a><div class="back" style="display:none;"><div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 back_title" ><a style="color:inherit"  href="'+response[i].permalink+'">'+response[i].title+'</a></div>';
					if(response[i].tax[0] == "professional" || response[i].tax[1] == "professional"){
						html =  html+'<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8"><h4> Price Per Week </h4><div class="rmvalue flipTable_room"  style="width:100%; float:left;background-color:#9BDF87;">&pound;'+response[i].ppm+'</div></div>';
					}
					else {
						html =  html+'<div class="col-lg-5 col-md-4 col-sm-4 col-xs-4"><h4> 2015 Availability </h4>';
			          
			           	var sper = 100 / response[i].rooms;
			            for(var j= 1;j <= response[i].rooms;j++)
	                    {
	                    	if (response[i].avail_this[j] == "Yes"){
	                    		var style_temp = 'background-color: #9BDF87;color:#272727';	
	                    		av_this_counter++;
	                    	}
	                    	else{
	                    		var style_temp = 'background-color:#9F1919; color:#f8f8f8;';
	                    		
	                    		
	                    	}
	                    	html =  html+'<div class="rmvalue flipTable_room" style="width:'+sper+'%; float:left;'+style_temp+'">&pound;'+response[i].room_ppw[j]+'</div>';
	                       

	                    }
			           	html =  html+'</div>';    

			           	html =  html+'<div class="col-lg-5 col-md-4 col-sm-4 col-xs-4"><h4> 2016 Availability </h4>';
			           	for(var k= 1;k <= response[i].rooms;k++)
	                    {
	                    	if (response[i].avail_this[j] == "Yes"){
	                    		var style_temp = 'background-color:#9F1919; color:#f8f8f8;';
	                    	}
	                    	else{
	                    		var style_temp = 'background-color: #9BDF87;color:#272727';	

	                    		av_next_counter++;
	                    	}
	                    	html =  html+'<div class="rmvalue flipTable_room" style="width:'+sper+'%; float:left;'+style_temp+'">&pound;'+response[i].room_ppw[k]+'</div>';
	                    }
	                    html =  html+'</div>';  
					}
					
					html =  html+'</div><div class="front"><div class="col-lg-2 col-md-2 col-sm-3 col-xs-3"><a style="color:inherit"  href="<?php the_permalink(); ?>">'+response[i].title+'</a></div><div class="col-lg-4 col-md-3 hidden-xs hidden-sm listing_icons">';
					if(response[i].shared_bath !=''){
						html = html+'<div title="Shared Bathrooms" class="wrapper-housing-icon view9_r active"><div class="ccount">'+response[i].shared_bath+'</div></div>';
					}
					if(response[i].shared_toilet !=''){
						html = html+'<div title="Shared Toilet" class="wrapper-housing-icon view10_r active"><div class="ccount">'+response[i].toilet_tot+'</div></div>';
					}
					if(response[i].ensuite != ''){
						html = html+'<div title="En Suite" class="wrapper-housing-icon view6_r active"><div class="ccount">'+response[i].ensuite+'</div></div>';
					}
					if(response[i].wheel != '' && response[i].wheel != 'No'){
						html = html+'<div title="Wheelchair Friendly" class="wrapper-housing-icon view5_r active"><div class="ccount">&#10003;</div></div>';
					}
					
					if(response[i].parking != ''){
						if (response[i].driveway_space != '' ){
							$park_spaces = response[i].driveway_space;
						}
						else
						{
							$park_spaces = '&#10003;'
						}
						html = html+'<div title="Parking Spaces" class="wrapper-housing-icon  view7_r active"><div class="ccount">'+$park_spaces+'</div></div>';
					}
					if(response[i].reargarden != ''){
						html = html+'<div title="Back Garden" class="wrapper-housing-icon view8_r active"><div class="ccount">&#10003;</div></div>';
					}
				
					html =  html+'</div>';

					html =  html+'<div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">'+response[i].postcode+'</div><div class="col-lg-1 col-md-2 col-sm-2 col-xs-2">'+av_this_counter+' <span class="mini">/ '+response[i].rooms+'</span></div><div class="col-lg-1 col-md-2 col-sm-2 col-xs-2">'+av_next_counter+' <span class="mini">/ '+response[i].rooms+'</span></div><div class="col-lg-1 col-md-1 col-sm-2 col-xs-2 strip_padding">'+response[i][distance_get]+'</div>';
					html =  html+'</div>';
					
					houseResults.find('#list_results #house_results_container').append(html);
					if (getUrlParameter("view") == "1"){
						  $('#flip_all').text('Summary of Features');
					      $('#flip_all').addClass('active');
					      $('.back').show();
					      $('.front').hide();
					}	
				}

				if(viewTemp == 2){  
					var exerpt_short = response[i].excerpt;
					exerpt_short = exerpt_short.substring(0,190);
					exerpt_short = exerpt_short+"...";
					var html ='<div class="col-xs-12 col-sm-6 col-md-4 item"><li class="gridbed'+response[i].rooms+'">';
					var pcm=4.3333333*response[i].pcm;
					html = html+'<a href="'+response[i].permalink+'"><img src="'+response[i].thumb[0]+'" alt="'+response[i].title+'" title="'+response[i].title+'" width="100%" class="img-responsive" /></a>';
					html = html+'<div class="gridcontent">';
					html = html+'<a href="'+response[i].permalink+'"><h4 class="gridhead"><span class="priceval">£'+pcm.toFixed(0)+'pcm</span> - '+response[i].rooms+' bed House</h4></a>';
					html = html+'<div class="gridsubhead">'+response[i].address+',<br />'+response[i].postcode+'</div>';	  
					html = html+'<div class="rsiconmynewblck">';
					
					if(response[i].shared_bath !=''){
						html = html+'<div title="Shared Bathrooms" class="wrapper-housing-icon view9_r active"><div class="ccount">'+response[i].shared_bath+'</div></div>';
					}
					if(response[i].shared_toilet !=''){
						html = html+'<div title="Shared Toilet" class="wrapper-housing-icon view10_r active"><div class="ccount">'+response[i].toilet_tot+'</div></div>';
					}
					if(response[i].ensuite != ''){
						html = html+'<div title="En Suite" class="wrapper-housing-icon view6_r active"><div class="ccount">'+response[i].ensuite+'</div></div>';
					}
					if(response[i].wheel != '' && response[i].wheel != 'No'){
						html = html+'<div title="Wheelchair Friendly" class="wrapper-housing-icon view5_r active"><div class="ccount">&#10003;</div></div>';
					}
					
					if(response[i].parking != ''){
						if (response[i].driveway_space != '' ){
							$park_spaces = response[i].driveway_space;
						}
						else
						{
							$park_spaces = '&#10003;'
						}
						html = html+'<div title="Parking Spaces" class="wrapper-housing-icon  view7_r active"><div class="ccount">'+$park_spaces+'</div></div>';
					}
					if(response[i].reargarden != ''){
						html = html+'<div title="Back Garden" class="wrapper-housing-icon view8_r active"><div class="ccount">&#10003;</div></div>';
					}	
					
					html = html+'</div>';
					html = html+'<div class="overlay"><div class="gridbtn"><a class="gridb2" href="/tenants-prospective-tenants-contact-form/">Contact us</a><a class="gridb1" href="'+response[i].permalink+'">View full details</a></div><a href="'+response[i].permalink+'"><p>'+exerpt_short+'</p></a>';
					html = html+'<div class="griddistance"><span class="distanceheader">Where is it?</span></div>';
					html = html+'UKC - '+response[i].ukct+' <span class="distancemiles">('+response[i].ukc+')</span><br />';
					html = html+'CCCU- '+response[i].cccut+' <span class="distancemiles">('+response[i].cccu+')</span><br />';
					html = html+'City Centre - '+response[i].citycentret+' <span class="distancemiles">('+response[i].citycentre+')</span><br />';
	 				html = html+'<div class="entry-meta">';
	 				for(var t = 1; t < response[i].terms.length; t++){
	 					if (t == 1){
	 						html = html+response[i].terms[t]['name'];
	 					}
	 					else
	 					{
	 						html = html+' - '+response[i].terms[t]['name'];
	 					}
	 				}
	 				
	 				html = html+'</div></div>';
					html = html+'</div>';
						
					html = html+'</li></div>';

					houseResults.find('#grid_results ul').append(html);
				}
			
				
				
				
			}
			$('.type a').removeClass('active');
			if(viewTemp == 1){ 
				houseResults.find('#list_results #house_results_container').append('<div class="clear_both"> </div>');
				$('.type_list').addClass('active');
				$('.flipTable_header').show();
				$('#flip_all').fadeIn();
				$('.flip_key').fadeIn();
			}
			if(viewTemp == 2){ 
				houseResults.find('#grid_results ul').append('<div class="clear_both"> </div>');
				$('.type_grid').addClass('active');
				$('.flipTable_header').hide();
				$('#flip_all').fadeOut();
				$('.flip_key').fadeOut();

			}
			if(viewTemp == 3){ 
				$('.type_map').addClass('active');
				$('.flipTable_header').hide();
				houseResults.find('#mapview').show();
				houseResults.find('#map_key').show();
				addresses = response;
				nextAddress = 0;
				start_map();
				$('#flip_all').fadeOut();
				$('.flip_key').fadeOut();

			}
			$('#room_count option').each(function(){
				if (roomcanover != 1){
					if ($(this).val() == roomoverwrite){
						$(this).attr('selected','selected');
					}	
				}
				
			});
			$('#search_message').empty();
			$('#search_message').append('<h2> '+response.length+' Properties Found.</h2>');
			$('#house_results .results_overlay').hide();
		}
	
	
	});

});

$("#house_results_container").on("mouseover", ".flipTable_item", function() {
    $(this).find('.front').hide();
    $(this).find('.back').show();
}).on("mouseleave", ".flipTable_item", function() {
    if(!$('#flip_all').hasClass('active')){
            $(this).find('.back').hide();
            $(this).find('.front').show();
    }
});
$('#search_top .type a').click(function(){

	$('#search_top .type a').removeClass('active')
	var viewtype = $(this).attr('data-view');
	if(viewtype == "1"){
		$('.flipTable_header').show();
	}
	else {
		$('.flipTable_header').hide();
	}
	$(this).addClass('active');
	$('#view_input').val(viewtype);

	searchForm.submit();
});


$('#house_search .check').click(function(){
	if($(this).find('input').prop("checked")){
		$(this).addClass('active');
	}
	else
	{
		$(this).removeClass('active');
	}
});
$('.type_grid').click(function(){
	viewcanover = 1;
	viewTemp = 2;
	searchForm.submit();
});
$('.type_list').click(function(){
	viewcanover = 1;
	viewTemp = 1;
	searchForm.submit();
});
$('.type_map').click(function(){
	viewcanover = 1;
	viewTemp = 3;
	searchForm.submit();
});
$('#room_count').click(function(){
	roomcanover = 1;
});
if ($(window).width() >= 1200){
	$("#house_search").affix({
      offset: { 
        top:0,
        bottom: ($('#footer').height() + 170)
   	}
  });
}


$('body').append('<div style="position:fixed; top:500px; padding:6px 15px;right:0px; z-index:999;" class="flip_container"> <div id="flip_all"> Show Room Availability </div> <div class="flip_key"> <div class="green">Available</div> <div class="red"> Reserved</div> </div> </div>')
$('#flip_all').click(function() {
                if($(this).hasClass('active')){
                    $(this).text('Show Room Availability');
                    $(this).removeClass('active');
                    $(".flipTable_item").flip(false);
                    $('.back').hide();
                    $('.front').show();
                }
                else{
                    $(this).text('Summary of Features');
                    $(this).addClass('active');
                    $(".flipTable_item").flip(true);
                    $('.back').show();
                    $('.front').hide();
                }
                
            });

searchForm.submit();

