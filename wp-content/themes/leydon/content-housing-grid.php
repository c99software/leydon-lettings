<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

<?php
$thumb_id = get_post_thumbnail_id();
$_address = get_post_meta(get_the_ID(), '_address', true);
$_postal_code = get_post_meta(get_the_ID(), '_postal_code', true);
$_latss = get_post_meta(get_the_ID(), '_lat', true);
$_lngss = get_post_meta(get_the_ID(), '_lng', true);
$ltd=$_latss.','.$_lngss;
/*$rek=getDistance($ltd);

if(get_post_meta(get_the_ID(), '_ucl', FALSE)) { update_post_meta(get_the_ID(), '_ucl', $rek[0]['distance']['text']); } else { add_post_meta(get_the_ID(), '_ucl', $rek[0]['distance']['text']); }
if(get_post_meta(get_the_ID(), '_uclt', FALSE)) { update_post_meta(get_the_ID(), '_uclt', $rek[0]['duration']['text']); } else { add_post_meta(get_the_ID(), '_uclt', $rek[0]['duration']['text']); }

add_post_meta(get_the_ID(), '_ucl', $rek[0]['distance']['text']);

if(get_post_meta(get_the_ID(), '_kent', FALSE)) { update_post_meta(get_the_ID(), '_kent', $rek[1]['distance']['text']); } else { add_post_meta(get_the_ID(), '_kent', $rek[1]['distance']['text']); }
if(get_post_meta(get_the_ID(), '_kentt', FALSE)) { update_post_meta(get_the_ID(), '_kentt', $rek[1]['duration']['text']); } else { add_post_meta(get_the_ID(), '_kentt', $rek[1]['duration']['text']); }

if(get_post_meta(get_the_ID(), '_canterbury', FALSE)) { update_post_meta(get_the_ID(), '_canterbury', $rek[2]['distance']['text']); } else { add_post_meta(get_the_ID(), '_canterbury', $rek[2]['distance']['text'	]); }
if(get_post_meta(get_the_ID(), '_canterburyt', FALSE)) { update_post_meta(get_the_ID(), '_canterburyt', $rek[2]['duration']['text']); } else { add_post_meta(get_the_ID(), '_canterburyt', $rek[2]['duration']['text']); }

if(get_post_meta(get_the_ID(), '_uca', FALSE)) { update_post_meta(get_the_ID(), '_uca', $rek[3]['distance']['text']); } else { add_post_meta(get_the_ID(), '_uca', $rek[3]['distance']['text']); }
if(get_post_meta(get_the_ID(), '_ucat', FALSE)) { update_post_meta(get_the_ID(), '_ucat', $rek[3]['duration']['text']); } else { add_post_meta(get_the_ID(), '_ucat', $rek[3]['duration']['text']); }
*/

?>



<?php 
		$url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
		$url= wp_get_attachment_image_src($thumb_id,'large', true);
		$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
			if(!$url){
				$url= get_bloginfo('template_url').'/images/fimg.png';
			}
		$_rooms = get_post_meta(get_the_ID(), '_rooms', true);
		?>
<div class="col-xs-12 col-sm-6 col-md-4 item">
<li class="gridbed<?php echo $_rooms;?>">
                                    
                                    
                                    $_bed1 = ("1");
                                    $_bed2 = ("2");
                                    $_bed3 = ("3");
                                    $_bed4 = ("4");
                                    $_bed5 = ("5");
                                    $_bed6 = ("6");
                                
                                    if ($_rooms == $_bed1 ){
                                        echo "gridbed1";
                                    }
                                
                                    elseif ($_rooms == $_bed2 ){
                                        echo "gridbed2";
                                    }
                                    
                                    elseif ($_rooms == $_bed3 ){
                                        echo "gridbed3";
                                    }
                                    
                                    elseif ($_rooms == $_bed4 ){
                                        echo "gridbed4";
                                    }
                                    
                                    elseif ($_rooms == $_bed5 ){
                                        echo "gridbed5";
                                    }
                                    
                                    else {
                                        echo "gridbed6";
                                    }
                                ?>">
<a href="<?php the_permalink(); ?>"><img src="<?php echo $url[0] ?>" alt="<?php echo $alt; ?>" title="<?php echo $alt; ?>" width="100%" class="img-responsive" /></a>
<div class="gridcontent">
<?php $pcm=get_post_meta(get_the_ID(), '_pcm', true);
$pcm=4.3333333*$pcm;
 ?>
  <a href="<?php the_permalink(); ?>"><h4 class="gridhead"><span class="priceval">£<?php echo round($pcm,2); ?>pcm</span> - <?php echo get_post_meta(get_the_ID(), '_rooms', true); ?> bedroom House</h4></a>

<div class="gridsubhead">
<?php echo get_post_meta(get_the_ID(), '_address', true); ?>,<br />
<?php echo get_post_meta(get_the_ID(), '_postal_code', true); ?>
</div>
<div class="rsiconmynewblck">
        <?php
    $_wheel = get_post_meta(get_the_ID(), '_Wheelchair', true);
	$_shower = get_post_meta(get_the_ID(), '_en_suite', true);
	$_shared_bath = get_post_meta(get_the_ID(), '_shared_bath', true);
	$_parking = get_post_meta(get_the_ID(), '_driveway', true);
	$_parking_bay = get_post_meta(get_the_ID(), '_parking_bay', true);
	$_drivespace = get_post_meta(get_the_ID(), '_driveway', true);
	$_garden = get_post_meta(get_the_ID(), '_rear_garden', true);
	$_parking_per=get_post_meta(get_the_ID(), '_parkingpermit', true);
	$_rooms = get_post_meta(get_the_ID(), '_rooms', true);
	$_toilet = get_post_meta(get_the_ID(), '_shared_toilet', true);

                    if($_rooms){
						$j=0;
						for($i=1;$i<=$_rooms;$i++)
				{
					$_Fensucount = get_post_meta(get_the_ID(), '_full_ensuite_'.$i, true);	
					$_ensucount = get_post_meta(get_the_ID(), '_shower_ensuite_'.$i, true);
					
					if($_Fensucount=='Yes' || $_ensucount=='Yes')
					{
						$j=$j+1;
					}
					 }
                        }
	?>
	<?php if($_wheel=='Yes') { ?>
		<div title="Wheelchair Friendly" class="wrapper-housing-icon rsic1">
          
		</div><?php } ?>
        <?php if($_shower!='') { ?>
		<div title="En Suite" class="wrapper-housing-icon rsic2">
        <div class="ccount"><?php echo $j; ?></div>
		</div><?php } ?>
        <?php if($_parking!='') { ?>
		<div title="Parking Spaces" class="wrapper-housing-icon rsic3">
        <div class="ccount"><?php echo $_drivespace; ?></div>
		</div><?php } ?>
        <?php if($_garden!='') { ?>
	  	<div title="Back Garden" class="wrapper-housing-icon rsic4">
        
		</div><?php } ?>
        <?php if($_shared_bath!='') { ?>
	  	<div title="Shared Bathrooms" class="wrapper-housing-icon rsic5">
        <div class="ccount"><?php echo $_shared_bath; ?></div>
		</div><?php } ?>
</div>

<div class="overlay">
	<div class="gridbtn">
		<a href="/tenants-prospective-tenants-contact-form/" class="gridb2">Contact us</a>
		<a href="<?php the_permalink(); ?>" class="gridb1">View full details</a>
	</div><!--gridbtn-->
	<a href="<?php echo the_permalink() ?>">
<p><?php echo excerpt(40); ?>
</p>
</a>
<div class="griddistance">
<?php 
	if(!empty($_GET['locations'])){
		$loc=$_GET['locations'];
	}
	else
		{
		$loc='_canterbury';
	}

if($loc=='_kent'){ $loce='UKC';}
	elseif
		($loc=='_ucl'){ $loce='CCCU';}
	elseif
		($loc=='_uca'){ $loce='UCA';}
	elseif
		($loc=='_canterbury'){ $loce='Canterbury';
	}
		//$distance= get_post_meta(get_the_ID(),'_kent' , true);
		//$distance= get_post_meta(get_the_ID(),$loc , true); 
			echo "<span class=\"distanceheader\">Where is it?</span>";
?>
</div>
<br /><?php  ?>UKC -  <?php  echo get_post_meta(get_the_ID(),'_kentt' , true) ?> <span class="distancemiles">(<?php  echo get_post_meta(get_the_ID(),'_kent' , true) ?>)</span><br />
<?php  ?>CCCU -  <?php echo get_post_meta(get_the_ID(),'_uclt' , true) ?> <span class="distancemiles">(<?php echo  get_post_meta(get_the_ID(),'_ucl' , true) ?>)</span><br />
<?php  ?>Canterbury Center -  <?php  echo get_post_meta(get_the_ID(),'_canterburyt' , true) ?> <span class="distancemiles">(<?php  echo get_post_meta(get_the_ID(),'_canterbury' , true) ?>)</span><br />
<!--<br /><?php  ?>UCA Caterbury -  <?php  echo get_post_meta(get_the_ID(),'_ucat' , true) ?> <span class="distancemiles">(<?php  echo get_post_meta(get_the_ID(),'_uca' , true) ?>)</span> -->

  <div class="entry-meta">
				<?php the_terms( $post->ID, 'location', '', ' - ' ); ?>	
	 		</div><!-- #entry meta END -->
</div>

<div class="gridbtn">
	<a href="/tenants-prospective-tenants-contact-form/" class="gridb2">Contact us</a>
	<a href="<?php the_permalink(); ?>" class="gridb1">View full details</a>
</div><!--gridbtn-->

</div><!--gridcontent-->
</li>
</div>