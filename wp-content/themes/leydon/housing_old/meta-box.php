<?php
function print_extra_fields($array,$id)
{
	echo '<table class="form-table">
<tbody>
';

	foreach($array as $index => $fields)
	{
		
		$_value = get_post_meta($id, $fields['name'], true);
		if(isset($fields['value']))
		{
			$_value=$fields['value'];
		}
		if($fields['type']=='text')
		{
			echo '<tr valign="top">
<th scope="row"><label for="'.$fields['label'].'">'.$fields['label'].'</label></th>
<td><input  type="text" name="data['.$fields['name'].']" value="' . $_value  . '" class="regular-text">';
if(isset($fields['info']))
{
	echo '<p class="description">'.$fields['info'].'</p>';
}
echo'</td>
</tr>';
		}
		if($fields['type']=='textarea')
		{
			echo '<tr valign="top">
<th scope="row"><label for="'.$fields['label'].'">'.$fields['label'].'</label></th>
<td>
<textarea name="data['.$fields['name'].']" id="'.$fields['name'].'" class="large-text code" rows="3">' . $_value  . '</textarea></td>
</tr>';
		}
		if($fields['type']=='select')
		{
			$optionsarray=$fields['options'];
			echo '<tr>
<th scope="row"><label for="'.$fields['label'].'">'.$fields['label'].'</label></th>
<td>';

			echo '<select  name="data['.$fields['name'].']">';
			echo '<option value="">Select Option</option>';
			foreach($optionsarray as $options)
			{
				$se='';
				if($_value==$options){ $se='selected="selected"';}
				echo '<option '.$se.' value="'.$options.'">'.$options.'</option>';
			}
			echo '</select></td></tr>';
		}
		if($fields['type']=='radio')
		{
			$optionsarray=$fields['options'];
			echo '<tr><th scope="row">'.$fields['label'].'</th><td><fieldset><legend class="screen-reader-text"><span>'.$fields['label'].'</span></legend>';			
			foreach($optionsarray as $options)
			{
				$se='';
				if($_value==$options){ $se='checked="checked"';}
				echo '<label title="'.$options.'"><input type="radio" name="data['.$fields['name'].']" '.$se.' value="'.$options.'"> <span>'.$options.'</span></label><br>';
			
			}
			echo '</fieldset></td></tr>';
		}
		if($fields['type']=='checkbox')
		{
			$optionsarray=$fields['options'];
			echo '<tr><th scope="row">'.$fields['label'].'</th><td><fieldset><legend class="screen-reader-text"><span>'.$fields['label'].'</span></legend>';			
			foreach($optionsarray as $options)
			{
				$se='';
				if($_value==$options){ $se='checked="checked"';}
				echo '<label title="'.$options.'"><input type="checkbox" name="data['.$fields['name'].'][]" '.$se.' value="'.$options.'"> <span>'.$options.'</span></label><br>';
			
			}
			echo '</fieldset></td></tr>';
		}
		if($fields['type']=='singlecheck')
		{
			
			echo '<tr><th scope="row">'.$fields['label'].'</th><td><fieldset><legend class="screen-reader-text"><span>'.$fields['label'].'</span></legend>';			
			
				$se='';
				$se='';$seo='';
				
				if($_value=='Yes'){ $se='selected="selected"';}else { $seo='selected="selected"';}
				echo '<select name="data['.$fields['name'].'_'.$i.']">';
				echo '<option value="0"> </option>';
				echo '<option '.$se.' value="Yes">Yes</option>';
				echo '<option '.$seo.' value="No">No</option>';
				echo '</select>';
			
			
			echo '</fieldset></td></tr>';
		}
		
	}
	echo '</tbody></table>';
	
}

function print_extra_fields_table($array,$id)
{
	
	$_rooms = get_post_meta($id, '_rooms', true);
	if(intval($_rooms))
	{
	echo '<table class="form-table ldata">
<tbody>
<tr><td>
';
echo '<table class="form-table">
                <tbody>';
echo ' <tr> <td class="room_description">&nbsp;</td>';
				for($i=1;$i<=$_rooms;$i++)
				{					
                   echo '<td class="room_description">Room '.$i.'</td>';
                                               
                                         
				}
				echo '</tr>';

	foreach($array as $index=>$fields)
	{
		
		
		
		
				
				
				echo ' <tr> <td class="room_description"><label for="'.$fields['label'].'">'.$fields['label'].'</label></td>';
				for($i=1;$i<=$_rooms;$i++)
				{

					$_value = get_post_meta($id, $fields['name'].'_'.$i, true);					
                   echo '<td class="room_description">';
				   if($fields['type']=='text')
					{
				   echo '<input  type="text" name="data['.$fields['name'].'_'.$i.']" value="' . $_value  . '" class="regular-text">';
					}
					
					 if($fields['type']=='radio')
					{
						$se='';$seo='';
				if($_value=='Yes'){ $se='selected="selected"';}else { $seo='selected="selected"';}
                if ($fields['name']=='_availability_next_next_year'){
                    if(!$_value) {
                        $se='selected="selected"';
                        $seo='';
                    }
                }
				echo '<select name="data['.$fields['name'].'_'.$i.']">';
				echo '<option value="0"> </option>';
				echo '<option '.$se.' value="Yes">Yes</option>';
				echo '<option '.$seo.' value="No">No</option>';
				echo '</select>';
			/*	echo '<div class="twocheck">';
				echo '<label>Yes</label><input type="radio" name="data['.$fields['name'].'_'.$i.']" '.$se.' value="Yes">';
				echo '<label>No</label><input type="radio" name="data['.$fields['name'].'_'.$i.']" '.$seo.' value="No">';
				echo '</div>';*/
					}
				 if($fields['type']=='select')
		{
			$optionsarray=$fields['options'];
			

			echo '<select  name="data['.$fields['name'].'_'.$i.']">';
			echo '<option value="">Select Option</option>';
			foreach($optionsarray as $options)
			{
				$se='';
				if($_value==$options){ $se='selected="selected"';}
				echo '<option '.$se.' value="'.$options.'">'.$options.'</option>';
			}
			echo '</select>';
		}  
				   echo '</td>';
                                               
                                         
				}
				echo '</tr>';
				
				
				
	}
	echo '</tbody></table>';
	echo '<tr><td></tbody></table>';
	}
}


function print_gallery($id)
{
	//
	  echo '<div class="galleryholder">';
	echo '<div class="outputimg">';
	$key='_gallery_images';
	$_value = get_post_meta($id, $key, true);
	$attids=explode(',',$_value);
	$size='thumb';
	if(count($attids)>0)
	{
		foreach($attids as $attid){
			if(!empty($attid)){
		if(is_numeric($attid)){
        $image = wp_get_attachment_image_src($attid, $size);
        if(!empty($image))
           echo '<div class="add_imagesdb" ><input  type="hidden" name="add_img[]" value="'.$attid.'"><img src="'.$image[0].'" height="58" ><div title="Remove Image" class="foldericon remove"></div></div>';
		}
		else
		{
			 echo '<div class="add_imagesdb" ><input  type="hidden" name="add_img[]" value="'.$attid.'"><img src="'.$attid.'" height="58" ><div title="Remove Image" class="foldericon remove"></div></div>';
		}
			}
		}
		
        
	}
   echo '</div>';
	//echo '<div class="add_images upload_buttons"><div class="foldericon"></div></div>';
	
	
	echo '<input type= "button" class="button  upload_image_button" name="_gallery_images" id="_gallery_images" value="Add Image(s)" />';
	   echo '</div>';
}


function print_certificates($id)
{
	//
	  echo '<div class="galleryholder">';
	echo '<div class="outputimg1">';
	$size="thumb";
	$key2='_certificates_images';
	$_value2 = get_post_meta($id, $key2, true);
	$attids2=explode(',',$_value2);
	if(count($attids2)>0)
	{
		foreach($attids2 as $attid2){
			if(!empty($attid2)){
		if(is_numeric($attid2)){
        $image2 = wp_get_attachment_image_src($attid2, $size);
        if(!empty($image2))
           echo '<div class="add_imagesdb1" ><input  type="hidden" name="add_img2[]" value="'.$attid2.'"><img src="'.$image2[0].'" height="58" ><div title="Remove Image" class="foldericon remove"></div></div>';
		}
		else
		{
			 echo '<div class="add_imagesdb1" ><input  type="hidden" name="add_img2[]" value="'.$attid2.'"><img src="'.$attid2.'" height="58" ><div title="Remove Image" class="foldericon remove"></div></div>';
		}
			}
		}
		
        
	}
   echo '</div>';
	//echo '<div class="add_images upload_buttons1"><div class="foldericon"></div></div>';
	echo '<input type= "button" class="button upload_image_button1" name="_certificates_images" id="_certificates_images" value="Add Image(s)" />';
	   echo '</div>';
	

}

function print_floorplans($id)
{
	//
	  echo '<div class="galleryholder">';
	echo '<div class="outputimg2">';
	$key2='_floorplans';
		$size="thumb";
	$_value2 = get_post_meta($id, $key2, true);
	$attids2=explode(',',$_value2);
	if(count($attids2)>0)
	{
		foreach($attids2 as $attid2){
			if(!empty($attid2)){
		if(is_numeric($attid2)){
        $image2 = wp_get_attachment_image_src($attid2, $size);
        if(!empty($image2))
           echo '<div class="add_imagesdb1" ><input  type="hidden" name="add_img3[]" value="'.$attid2.'"><img src="'.$image2[0].'" height="58" ><div title="Remove Image" class="foldericon remove"></div></div>';
		}
		else
		{
			 echo '<div class="add_imagesdb1" ><input  type="hidden" name="add_img3[]" value="'.$attid2.'"><img src="'.$attid2.'" height="58" ><div title="Remove Image" class="foldericon remove"></div></div>';
		}
			}
		}
		
        
	}
   echo '</div>';
	//echo '<div class="add_images upload_buttons1"><div class="foldericon"></div></div>';
	echo '<input type= "button" class="button upload_image_button2" name="_floorplans" id="_floorplans" value="Add Image(s)" />';

	
	echo '</div>';
}

/* ISSUE FOUND FROM THIS */

add_action('do_meta_boxes', 'change_image_box');
function change_image_box()
{
    remove_meta_box( 'postimagediv', 'housing', 'side' );
    add_meta_box('postimagediv', __('Main Image'), 'post_thumbnail_meta_box', 'housing', 'side', 'high');
}

function do_thumb($content){
	 return str_replace(__('Set featured image'), __('Set main image'),$content);
}
function print_fields_front($array,$id)
{
	
	foreach($array as $index=>$fields)
	{
		
		$_value = get_post_meta($id, $fields['name'], true);
		if ($_value != ""){
			if($fields['type']=='text')
			{
				
				if($fields['link']!='')
				{
					echo '<p class="'.str_replace('_','',$fields['name']).'"><a href="/'.$fields['link'].'">'.$fields['label'].' : ' . $_value  . '</a></p>';
				}
				
				
				elseif($fields['name']=='_nonrefundfee')
				{
					echo '<p class="'.str_replace('_','',$fields['name']).'">'.$fields['label'].' : £'. $_value  . '</p>';
					}
				else{
				echo '<p class="'.str_replace('_','',$fields['name']).'">'.$fields['label'].' : ' . $_value  . '</p>';
				}
				
			}
			if($fields['type']=='textarea')
			{
				echo '<p class="'.str_replace('_','',$fields['name']).'">'.$fields['label'].' : ' . $_value  . '</p>';
			}
			if($fields['type']=='select')
			{
				if($fields['link']!='')
				{
					echo '<p class="'.str_replace('_','',$fields['name']).'"><a href="/'.$fields['link'].'">'.$fields['label'].' : ' . $_value  . '</a></p>';
				}
				elseif($fields['linkcont']!='')
				{
					if($_value=='Silver Contract') {
					echo '<p class="'.str_replace('_','',$fields['name']).'"><a href="/silver-contract">'.$fields['label'].' : ' . $_value  . '</a></p>'; }
					elseif($_value=='Golden Contract') {
					echo '<p class="'.str_replace('_','',$fields['name']).'"><a href="/golden-contract">'.$fields['label'].' : ' . $_value  . '</a></p>'; }
				}
				else{
				if($fields['img']=='Wheel'){
					$siteur=get_bloginfo('template_url');
					if($_value=='Yes'){ echo '<img title="Wheelchair Friendly" src="'.$siteur.'/images/wheelchair.png">'; }
					
					}
				else{
				echo '<p class="'.str_replace('_','',$fields['name']).'">'.$fields['label'].' : ' . $_value  . '</p>';
				}
				}
			}
			if($fields['type']=='radio')
			{
				echo '<p class="'.str_replace('_','',$fields['name']).'">'.$fields['label'].' : ' . $_value  . '</p>';
			}
			if($fields['type']=='checkbox')
			{
				echo '<p class="'.str_replace('_','',$fields['name']).'">'.$fields['label'].' : ' . $_value  . '</p>';
			}
			if($fields['type']=='singlecheck')
			{
				if($_value && $_value!='No'){
					echo '<p class="'.str_replace('_','',$fields['name']).'">'.$fields['label'].' : Yes</p>';
				}
				else
				{
					echo '<p class="'.str_replace('_','',$fields['name']).'">'.$fields['label'].' : No</p>';
				}
			}
		}
		
	}
	
}
function print_extra_fields_table_front($array,$id)
{
	
	$_rooms = get_post_meta($id, '_rooms', true);
	if(intval($_rooms))
	{
	echo '<table class="form-table ldata">
<tbody>
<tr><td>
';
echo '<table class="form-table">
                <tbody>';
echo ' <tr> <td class="room_description">&nbsp;</td>';
				for($i=1;$i<=$_rooms;$i++)
				{					
                   echo '<td id="Rm-'.$i.'" class="room_description">Room '.$i.'</td>';
                                               
                                         
				}
				echo '</tr>';

	foreach($array as $index=>$fields)
	{
        if($fields['label']=="Shower Ensuite"){
            $Ensuite_count=0;
            for($i=1;$i<=$_rooms;$i++){
                $_av = get_post_meta($id, '_availability_this_year_'.$i, true);
                $_value = get_post_meta($id, $fields['name'].'_'.$i, true);
                $j=$_av=='No'?'style="background-color:#DDF0FF; color:black;"':'style="background-color: rgb(211, 255, 205);color:#141412"';
                if($fields['type']=='radio')
                {
                    $se='';
                    if($_value=='Yes'){
                        $Ensuite_count++;
                    }
                }
            }
            echo "<div class='ensuite_shower_count' style='display:none;'>".$Ensuite_count."</div>";
        }
		echo ' <tr> <td class="room_description"><label for="'.$fields['label'].'">'.$fields['label'].'</label></td>';
				for($i=1;$i<=$_rooms;$i++)
				{

					$_av = get_post_meta($id, '_availability_this_year_'.$i, true);	
					$_value = get_post_meta($id, $fields['name'].'_'.$i, true);	
					$j=$_av=='No'?'style="background-color:#DDF0FF; color:black;"':'style="background-color: rgb(211, 255, 205);color:#141412"';			
                   echo '<td class="room_description rmvalue " '.$j.' >';
				   if($fields['type']=='text')
					{
						if('_rent'==$fields['name'])
						echo '&pound;'.$_value;
						else if ($_value=='Yes') {
                                $Ensuite_count++;
								?> <img src="<?php bloginfo('template_url'); ?>/images/yes.png" /> <?php
							}
							else if ($_value=='No') {
								?> <img src="<?php bloginfo('template_url'); ?>/images/nooo.png" /> <?php
							}
							else { 
								echo $_value;
							}
						
					}
					 if($fields['type']=='radio')
					{
						$se='';
				if($_value=='Yes'){ ?> <img src="<?php bloginfo('template_url'); ?>/images/yes.png" /> <?php }
				else
				{
					?> <img src="<?php bloginfo('template_url'); ?>/images/nooo.png" /> <?php ;
				}
				
					}
				   
				   echo '</td>';
                                               
                                         
				}
        echo '</tr>';

	}
	echo '</tbody></table>';
	echo '</td></tr></tbody></table>';
	}
}
function print_extra_fields_table_front_only($array,$id)
{
	 $fields=array(
	 array('label'=>'Car Space','type'=>'singlecheck','name'=>'_parking_bay'),	
		array('label'=>'En Suite','type'=>'text','name'=>'_en_suite'),
	
		array('label'=>'Bath','type'=>'text','name'=>'_shared_bath'),
		array('label'=>'WC','type'=>'select','name'=>'_outhouse_wc','options'=>$_yesno),
	
	array('label'=>'Ensuite','type'=>'radio','name'=>'_shower_ensuite','options'=>array('Yes','No')),
	);
	
	echo '<table class="form-table ldata">
<tbody>
<tr><td>
';
echo '<table class="form-table">
                <tbody>';
echo ' <tr>';
					foreach($fields as $field)
					{
						 echo '<td class="room_description">'.$field['label'].'</td>';
					}
                /*   echo '<td class="room_description">Shower</td>';
				    echo '<td class="room_description">Toilet</td>';
					 echo '<td class="room_description">Bathroom</td>';
                            */                   
                                         
				
				echo '</tr>';
				echo ' <tr>';
			foreach($fields as $field)
					{
						 echo '<td class="room_description">'.get_post_meta($id, $field['name'], true).'</td>';
					}
                  /* echo '<td class="room_description">'.get_post_meta($id, '_shared_shower', true).'</td>';
				     echo '<td class="room_description">'.get_post_meta($id, '_shared_toilet', true).'</td>';
					  echo '<td class="room_description">'.get_post_meta($id, '_shared_bath', true).'</td>';
                      */                         
                                         
				
				echo '</tr>';

	//$_value = get_post_meta($id, $fields['name'].'_'.$i, true);
	echo '</tbody></table>';
	echo '</td></tr></tbody></table>';
	
}
?>