<?php
@session_start();
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
 

$rooms=get_post_meta(get_the_ID(), '_rooms', true);
global $wpdb;
global $post;
/*$wheelchairquery = "
    SELECT DISTINCT(CAST(meta_value AS UNSIGNED)) AS met 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '_Wheelchair' AND post_id='".get_the_ID()."'
	ORDER BY met ASC
	LIMIT 1
 ";
 $wheel=$wpdb->get_results($wheelchairquery, ARRAY_N); */
$_wheel = get_post_meta(get_the_ID(), '_Wheelchair', true);
$_shower = get_post_meta(get_the_ID(), '_en_suite', true);
$_parking = get_post_meta(get_the_ID(), '_parking_bay', true);
$_garden = get_post_meta(get_the_ID(), '_rear_garden', true);
$_shared_bath = get_post_meta(get_the_ID(), '_shared_bath', true);
$_shared_toilet = get_post_meta(get_the_ID(), '_shared_toilet', true);
$rentslowquery = "
    SELECT DISTINCT(CAST(meta_value AS UNSIGNED)) AS met 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '%_rent_%' AND post_id='".get_the_ID()."'
	ORDER BY met ASC
	LIMIT 1
 ";
$rentslow = $wpdb->get_results($rentslowquery, ARRAY_N);
$rentshighquery = "
    SELECT DISTINCT(CAST(meta_value AS UNSIGNED)) AS met 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '%_rent_%' AND post_id='".get_the_ID()."'
	ORDER BY met DESC
	LIMIT 1
 ";
$rentshigh = $wpdb->get_results($rentshighquery, ARRAY_N);
$availabilitythisquery = "
    SELECT count(*) 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '".get_option('at_current_year')."_%' AND meta_value = 'Yes' AND post_id='".get_the_ID()."'
 ";
$availabilitythis = $wpdb->get_results($availabilitythisquery, ARRAY_N);
$athiswidth=0;
$athis=$availabilitythis[0][0];
$athiswidth=$athis/$rooms*100;
$thisrestwidth=100-$athiswidth;
$availabilitynextquery = "
    SELECT count(*) 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '".get_option('at_next_year')."_%' AND meta_value = 'Yes' AND post_id='".get_the_ID()."'
 ";
$availabilitynext = $wpdb->get_results($availabilitynextquery, ARRAY_N);
$anextwidth=0;
$anext=$availabilitynext[0][0];
$anextwidth=$anext/$rooms*100;
$anextrestwidth=100-$anextwidth;
?>
<?php 
if(!empty($_GET['locations']))
{
	$loc=$_GET['locations'];
}
else
{
	$loc='_kent';
	$distance='';
}
$distance= get_post_meta(get_the_ID(),$loc , true); ?>
<?php 
if($loc=='_kent'){ $loce='UKC';}
elseif($loc=='_uc1'){ $loce='CCCU';}
elseif($loc=='_uca'){ $loce='UCA';}
elseif($loc=='_canterbury'){ $loce='Canterbury';}
$_SESSION['in']+=1;
?>

    <div class="flipTable_item">
        <div class="back" style="display:none;">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 back_title" >
               <a style="color:inherit"  href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </div>
            <?php $args = array( 'fields' => 'slugs'); ?>
            <?php $tax_holder =  wp_get_post_terms(get_the_ID(),'type',$args); ?>
            
            <?php $ppm = get_post_meta(get_the_ID(), '_pcm', true); ?>
            <?php if($tax_holder[0] == "professional" || $tax_holder[1] == "professional"): ?>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <h4> Price Per Week </h4>
               <div class="rmvalue flipTable_room"  style="width:100%; float:left;background-color:#9BDF87;"><?php echo '&pound;'.$ppm; ?></div>
            </div>
         
        <?php elseif($tax_holder[0] == "student-only" || $tax_holder[1] == "student-only"): ?>
             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <h4> 2015 Availability </h4>
                <?php $av_roomsThis = 0; ?>
                <?php $av_roomsNext = 0; ?>
                 <?php
                        $sper=100/$rooms;
                        for($i=1;$i<=$rooms;$i++)
                        {
                            $_av = get_post_meta($id, get_option('at_current_year').'_'.$i, true);
                            $_value = get_post_meta($id, '_rent_'.$i, true);    
                            $j=$_av=='No'?'background-color:#9F1919; color:#f8f8f8;':'background-color: #9BDF87;color:#272727';
                            if($_av=='Yes'){$av_roomsThis++;}
                            ?>
                            <div class="rmvalue flipTable_room"  style="width:<?= $sper ?>%; float:left;<?php echo $j ?>"><?php echo '&pound;'.$_value; ?></div>
                            <?php
                        }
                        ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                 <h4> 2016 Availability </h4>
                        <?php
                        $sper=100/$rooms;
                        for($i=1;$i<=$rooms;$i++)
                        {
                            $_av = get_post_meta($id, get_option('at_next_year').'_'.$i, true);
                            $_value = get_post_meta($id, '_rent_'.$i, true);    
                            $j=$_av=='No'?'background-color:#9F1919; color:#f8f8f8;':'background-color: #9BDF87;color:#272727';
                            if($_av=='Yes'){$av_roomsNext++;}
                            ?>
                            
                            <div class="rmvalue flipTable_room" style=" width:<?= $sper ?>%; float:left;<?php echo $j ?>"><?php echo '&pound;'.$_value; ?></div>
                           
                            <?php
                        }
                        ?>

                  
            </div>
        <?php endif; ?>

            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 back_button">
                <a class="button" href="<?php the_permalink(); ?>" title="More Details" alt="More Details"> More Details </a>
            </div>
          
        </div>
        <div class="front">
             <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
               <a style="color:inherit"  href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </div>
             <div class="col-lg-3 col-md-3 hidden-xs hidden-sm listing_icons">
                                <?php if($_wheel=='Yes') {  ?>
                                
                        <div title="Wheelchair Friendly" class="wrapper-housing-icon srsic1">

                         </div><?php } ?>
                        <?php if($_shower!='') { ?>
                            <div title="En Suite" class="wrapper-housing-icon srsic2">
                            <div class="ccount"><?php echo $_shower; ?></div>
                            </div><?php } ?>
                        <?php if($_parking!='') { ?>
                            <div title="Parking Spaces" class="wrapper-housing-icon srsic3">

                            </div><?php } ?>    
                        <?php if($_garden!='') { ?>
                            <div title="Back Garden" class="wrapper-housing-icon srsic4">

                            </div><?php } ?>
                            <?php if($_shared_bath!='') { ?>
                            <div title="Shared Bathrooms" class="wrapper-housing-icon srsic5">
                            <div class="ccount"><?php echo $_shared_bath; ?></div>
                            </div><?php } ?>
                             <?php if($_shared_toilet!='') { ?>
                            <div title="Shared Toilet" class="wrapper-housing-icon srsic6">
                            <div class="ccount"><?php echo $_shared_toilet; ?></div>
                            </div><?php } ?>
              
            </div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
                <?php echo get_post_meta(get_the_ID(), '_postal_code', true); ?>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <?php echo $av_roomsThis; ?>/
                <?php echo $rooms; ?>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <?php echo $av_roomsNext; ?>/
                <?php echo $rooms; ?>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                <?php if($distance)
                     { echo $distance  ?> Miles <?php }  ?>
            </div>
        </div>
        
    </div>



<script type="text/javascript">
jQuery(function(){
	jQuery( ".predef" ).click(function( event ) {
  event.preventDefault();
				});
	jQuery('.indiv').each(function(index, element) {
        jQuery(this).height(jQuery(this).parent('td').height())
		
		
    });
	
	})
</script>