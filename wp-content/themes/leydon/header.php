<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <title><?php wp_title( ':', true, 'right' ); ?></title>

    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/fancybox/jquery-1.4.3.min.js"></script>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/fancybox/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="shortcut icon" type="image/png" href="/favicon.ico"/>
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/fancybox/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    </script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/fancybox/fancybox/jquery.mousewheel-3.0.4.pack.js">
    </script>
    <script>
        $(document).ready(function() {
            $(window).scroll(function(){
                $('.header').css({
                    'top': $(this).scrollTop()
                });
            });
        });
    </script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-66211226-1', 'auto');
  ga('send', 'pageview');

</script>
    <script type="text/javascript">
        function showMore(d)
        {
            jQuery('#'+d).slideToggle();
        }
    </script>

    <script type="text/javascript">var switchTo5x=true;</script>
    <!-- <script type="text/javascript">stLight.options({publisher: "8533b2a5-b08a-41d9-8a7f-30ce63f016c4", doNotHash: false, doNotCopy: false, hashAddressBar: true});</script> -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="<?php echo get_template_directory_uri(); ?>/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/responsive.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.flip.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.sticky-kit.js"></script>
    <script src="<?php bloginfo('template_url') ?>/housing/scripts/jquery.bxslider.js"></script>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/responsive.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/fonts/stylesheet.css">
    <link href="//leydon-canterbury.fixflo.com/Content/buttons/Button2.css" rel="stylesheet" type="text/css" />
</head>
<?php if (isset($_GET['rview'])) {
    $_SESSION['r-view'] = $_GET['rview'];
}?>
<body <?php body_class(); ?><?php //if (!(isset($_SESSION['r-view'])&&($_SESSION['r-view']=="mobile"))){ echo 'style="min-width:990px"';} ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=286745381506599&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!--
    <div class="header">
        <div class="headerinner">
            <div class="headercontent">
                <form action="" method="get">
                    <div class="search1">
                        <p><input type="text" name="s" class="inputsearchbox" placeholder="Search.."></p>
                        <button class="searchicon" type="submit"></button>
                    </div>
                </form>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="headerlogo"></a>
                <div class="headercontact">
                    <a href="mailto:info@leydonlettings.co.uk" class="headeremail">info@leydonlettings.co.uk</a>
                    <div class="headerphone">01227 713 913 </div>
                </div>
                <div class="headermenu">

                    <div class="menu">

                        <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    -->

<div class="social_fixed">
    <a class="expand" href="javascript:void(0)"> </a>
    <div class="social_btns">
        <a class="twitter" href="https://twitter.com/leydonlettings" target="new"></a>
        <a class="facebook" href="https://www.facebook.com/LeydonLettingsCanterbury" target="new"></a>
        <a class="linkedin" href="http://www.linkedin.com/pub/bob-leydon/1b/80b/55b" target="new"></a>
        <a class="youtube" href="#" target="new"></a>
    </div>
</div>
<script>
jQuery('.social_fixed .expand').on('click',function(){
    if(jQuery('.social_fixed').hasClass('active')){
       jQuery('.social_fixed').removeClass('active');
       jQuery(this).removeClass('open');
    }
    else
    {
        jQuery('.social_fixed').addClass('active');
        jQuery(this).addClass('open');
    }
    
});
jQuery('.social_fixed .mini').on('click',function(){

});
</script>
<div class="r-header">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="findbyroom_container">

            <div class="container-fluid nav_top" >
                <div class="container">
                    <div class="nav_top_left">
                        <button type="button" class="navbar-toggle btn-xs pull-right" data-toggle="collapse" data-target="#r-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                        <button type="button" class="navbar-toggle btn-xs pull-right" data-toggle="collapse" data-target="#r-navbar-collapse-2">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="glyphicon glyphicon-align-justify"></span>
                        </button>
                        <a alt="Leydon Lettings" title="Leydon Lettings" href="<?php echo home_url();?>" class="r-header-logo pull-left visible-lg"><img src="<?php echo get_template_directory_uri().'/images/leydon-lettings-logo.png';?>"/></a>
                        <a alt="Leydon Lettings" title="Leydon Lettings" href="<?php echo home_url();?>" class="r-header-logo pull-left hidden-lg"><img src="<?php echo get_template_directory_uri().'/images/leydon-lettings-logo.png';?>"/></a>
                    </div>
                    <div class="nav_top_right"> 
                        <div class="navbar-collapse collapse" id="r-navbar-collapse-1">
                            <ul class="nav navbar-right">
                                
                                <li>
                                    <a href="#"><span class="glyphicon glyphicon-earphone"></span> 01227 713 913</a>
                                </li>
                                <li>
                                    <a href="mailto:enquiries@leydonlettings.co.uk"><span class="glyphicon glyphicon-envelope"></span> enquiries@leydonlettings.co.uk</a>
                                </li>
                                <li class="col-lg-5  col-md-4 col-xs-12 col-sm-3 search_container">
                                    <form action="/" method="get">
                                       
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="s" class="form-control inputsearchbox" placeholder="Search..">
                                            <span class="arrow-down">
                                            </span>
                                            <select name="post_type">
                                                <option value="housing">Properties</option>
                                                <option value="post">Website</option>
                                            </select>
                                            <div class="input-group-btn">
                                                <button type="submit" class="btn btn-default">
                                                    <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                            </div>
                                        </div>
                                    
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--
            <div class="container">
                <div class="findbyroom">
                    <div class="room_selector col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 strip_padding">
                            <div class="col-xs-3 strip_padding">
                                <a class="findbyroom_link link0" href="/available-canterbury-student-accommodation/?view=1" title="Just4Me" alt="1 Bedroom"><span class="box"> </span><span class="text"> <span>Just4</span>Me</span></a>
                             </div>
                            <div class="col-xs-3 strip_padding">
                                <a class="findbyroom_link link1" href="/available-canterbury-student-accommodation/?view=1" title="1 Bedroom" alt="1 Bedroom"><span class="box"> </span><span class="text"> 1 <span>Rooms</span></span></a>
                             </div>
                            <div class="col-xs-3 strip_padding">
                                <a class="findbyroom_link link2" href="/available-canterbury-student-accommodation/?room=2" title="2 Bedroom" alt="2 Bedroom"><span class="box"> </span><span class="text"> 2 <span>Rooms</span></span></a>
                             </div>
                            <div class="col-xs-3 strip_padding">
                                <a class="findbyroom_link link3" href="/available-canterbury-student-accommodation/?room=3" title="3 Bedroom" alt="3 Bedroom"><span class="box"> </span><span class="text"> 3 <span>Rooms</span></span></a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 strip_padding">
                            <div class="col-xs-3 strip_padding">
                                <a class="findbyroom_link link4" href="/available-canterbury-student-accommodation/?room=4" title="4 Bedroom" alt="4 Bedroom"><span class="box"> </span><span class="text"> 4 <span>Rooms</span></span></a>
                            </div>
                            <div class="col-xs-3 strip_padding">
                                <a class="findbyroom_link link5" href="/available-canterbury-student-accommodation/?room=5" title="5 Bedroom" alt="5 Bedroom"><span class="box"> </span><span class="text"> 5 <span>Rooms</span></span></a>
                            </div>
                            <div class="col-xs-3 strip_padding">
                                <a class="findbyroom_link link6" href="/available-canterbury-student-accommodation/?room=6" title="6 Bedroom" alt="6 Bedroom"><span class="box"> </span><span class="text"> 6 <span>Rooms</span></span></a>
                            </div>
                            <div class="col-xs-3 strip_padding">
                                <a class="findbyroom_link link7" href="/available-canterbury-student-accommodation/?room=7" title="7 Bedroom" alt="7 Bedroom"><span class="box"> </span><span class="text"> 7+ <span>Rooms</span></span></a>
                            </div>
                        </div>
                    </div>
                        <a href="/available-canterbury-student-accommodation/?view=3" title="Map View" alt="Map View" class="mapIcon">
                            <span class="icon"> </span>
                          <span> Map </span>
                        </a>
                    </div>
                   
                </div>
            </div>
            -->
        <div class="container-fluid nav_bottom">
            <div class="nav_container container">
                <div class="row">
                    <div class="headermenu">

                        <div class="menu">

                            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>

                        </div><!--menu-->
                    </div><!--headermenu-->
              
                
                    <div class="visible-xs">
                        <?php wp_nav_menu( array(
                    'theme_location' => 'primary',
                    'menu'            => '',
                    'container'       => 'div', // "div" or "nav" (use "nav" for HTML5)
                    'container_class' => 'collapse navbar-collapse',
                    'container_id'    => 'r-navbar-collapse-2',
                    'menu_class'      => 'nav navbar-nav',
                    'menu_id'         => '',
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul class="nav navbar-nav">%3$s</ul>',
                    'depth'           => 0,
                    'walker'          => '' ) ); ?>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="findbyroom" >
            <div class="container">
                <div class="row">
                    <ul class="list">
                        <li class="first room_me"><a href="/available-canterbury-student-accommodation/?view=1"><span class="icon"></span><span class="text"><span class="hidden-sm">Just4</span>Me</span></a></li>
                        <li class="room_1"><a href="/available-canterbury-student-accommodation/?room=1"><span class="icon"></span><span class="text">1 <span class="hidden-xs">Bed<span class="hidden-sm">room</span></span></span></a></li>
                        <li class="room_2"><a href="/available-canterbury-student-accommodation/?room=2"><span class="icon"></span><span class="text">2 <span class="hidden-xs">Bed<span class="hidden-sm">room</span></span></span></a></li>
                        <li class="room_3"><a href="/available-canterbury-student-accommodation/?room=3"><span class="icon"></span><span class="text">3 <span class="hidden-xs">Bed<span class="hidden-sm">room</span></span></span></a></li>
                        <li class="room_4"><a href="/available-canterbury-student-accommodation/?room=4"><span class="icon"></span><span class="text">4 <span class="hidden-xs">Bed<span class="hidden-sm">room</span></span></span></a></li>
                        <li class="room_5"><a href="/available-canterbury-student-accommodation/?room=5"><span class="icon"></span><span class="text">5 <span class="hidden-xs">Bed<span class="hidden-sm">room</span></span></span></a></li>
                        <li class="room_6"><a href="/available-canterbury-student-accommodation/?room=6"><span class="icon"></span><span class="text">6 <span class="hidden-xs">Bed<span class="hidden-sm">room</span></span></span></a></li>
                        <li class="room_7"><a href="/available-canterbury-student-accommodation/?room=7"><span class="icon"></span><span class="text">7 <span class="hidden-xs">Bed<span class="hidden-sm">room</span></span></span></a></li>
                        <li class="last map"><a href="/available-canterbury-student-accommodation/?view=3"><span class="text">Map</span><span class="icon"></span></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</div>

</div>
<div id="page" class="content">
   

    <?php /*?><header id="masthead" class="site-header" role="banner">
			<a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
				<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
				<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
			</a>

			<div id="navbar" class="navbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<h3 class="menu-toggle"><?php _e( 'Menu', 'twentythirteen' ); ?></h3>
					<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php _e( 'Skip to content', 'twentythirteen' ); ?></a>
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
					<?php get_search_form(); ?>
				</nav><!-- #site-navigation -->
			</div><!-- #navbar -->
		</header><?php */?><!-- #masthead -->

    <div id="main" class="site-main">