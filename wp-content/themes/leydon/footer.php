<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

</div><!-- #main -->

<?php /*?><footer id="colophon" class="site-footer" role="contentinfo">
			<?php get_sidebar( 'main' ); ?>

			<div class="site-info">
				<?php do_action( 'twentythirteen_credits' ); ?>
				<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'twentythirteen' ) ); ?>" title="<?php esc_attr_e( 'Semantic Personal Publishing Platform', 'twentythirteen' ); ?>"><?php printf( __( 'Proudly powered by %s', 'twentythirteen' ), 'WordPress' ); ?></a>
			</div><!-- .site-info -->
		</footer><?php */?><!-- #colophon -->
</div><!-- #page -->
<footer id="footer" class="hidden-xs">
    <div class="container">
        <div class="footerinner">
            <div class="col-sm-6 col-md-3">
                <h4>Visit Us </h4>
                <p>49-50 Castle Street, <br />
                Canterbury, Kent, CT1 2PY </p>
                <p>54 Northgate, <br />
                Canterbury, Kent, CT1 1BE</p>
                <h4> Talk to us</h4>
                <p> Email: enquiries@leydonlettings.co.uk </p>
                <p> Phone: +44 01227 713 913</p>
            </div>
             <div class="col-sm-6 col-md-2">
                <h4> Company </h4>
                <a href="/about-us/"> About Us </a>
                <a href="/company-history/"> Company History </a>
                <a href="/meet-the-leydon-lettings-team/"> Our Team </a>
                <a href="/complaints-compliments-procedure/"> Complaints and Compliments </a>
                <div id="footersocial">
                    <h4> Connect with us</h4>
                    <a class="plus" href="https://plus.google.com/110472107535465952395"></a><!---->
                    <a class="linkedin" href="http://www.linkedin.com/company/leydon-lettings"></a><!---->
                    <a class="tw" href="https://twitter.com/leydonlettings"></a><!---->
                    <a class="fb" href="https://www.facebook.com/LeydonLettingsCanterbury"></a><!---->
                </div>
            </div>
            <div class="col-sm-6 col-md-2">
                <h4> Tenant </h4>
                <a href="/booking-process/"> Bookings </a>
                <a href="/application-forms/"> Application Forms </a>
                <a href="/terms-and-conditions/"> Terms and Conditions </a>
                <a href="/bills-package-rent/"> Bills Package and Rent </a>
                <a style="margin-top:10px;" href="https://leydon-canterbury.fixflo.com/" class="glob_btn alt">Request Repair</a>
            </div>
            <div class="col-sm-6 col-md-2">
                <h4> Landlord </h4>
                <a href="/rental-documents/">Requirements</a>
                <a href="/standards/">Standards</a>
                <a href="/lettings-glossary/">Glossary</a>
                <a href="/landlord-fees/">Fees</a>
            </div>
            <div  id="footerlatest" class="col-sm-6 col-md-3">
                    <ul class="footerresources">
                        <li style="color:white"><h4>Latest</h4></li>
                        <?php $args = array( 'posts_per_page' => 2, 'orderby' => 'post_date', 'offset'=> 0, 'category' => 13 );
                        $myposts = get_posts( $args );
                        foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                        <?php 
                           $preview = strip_tags(substr(get_the_content(),0,100).'...');
                           ?>
                            <li>
                                <a href="<?php the_permalink() ?>">
                                    <?php  echo the_post_thumbnail( array(135,135) );?>
                                    <h6>
                                        <?php the_title(); ?>
                                    </h6> 
                                    <p><?php echo $preview; ?> </p>
                                </a>
                            </li>
                        <?php endforeach; wp_reset_postdata();?>

                    </ul>
            </div>


            
        </div><!--footerinner-->
    </div>
</footer>

<!-- Responsive Footer Start -->
<div class="r-footer visible-xs">
    <div class="container r-footer-container">
        <div class="row r-footer-top">
            <div class="col-xs-12  col-sm-6 r-company-address">
                <p>Leydon House</p>
                <p>49-50 Castle Street,</p>
                <p>Canterbury,Kent,CT1 2PY</p>
            </div>
            <div class="col-xs-12  col-sm-6  r-company-contact">
                <p>&nbsp;</p>
                <p>01227 713 913</p>
                <p><a href="mailto:enquiries@leydonlettings.co.uk">enquiries@leydonlettings.co.uk</a></p>
            </div>
        </div>
       
        <div class="col-xs-12 r-middle-social text-center">
            <a href="https://plus.google.com/110472107535465952395" class="plus"></a>
            <a href="http://www.linkedin.com/company/leydon-lettings" class="linkedin"></a>
            <a href="https://twitter.com/leydonlettings" class="tw"></a>
            <a href="https://www.facebook.com/LeydonLettingsCanterbury" class="fb"></a>
        </div>
        <div class="col-xs-12 r-middle-links">
            <p>
                <a href="/sitemap/">Sitemap</a> | <a href="/privacy-policy/">Privacy Policy</a>
                <br />
                &copy; Leydon Lettings 2013 - <?php echo date("Y"); ?>
            </p>
        </div>
    </div>
    <div class="row r-footer-bottom">
        <div class="col-xs-12 r-bottom-switch">
        </div>
    </div>
</div>
<div class="container footer_middle">
     <div class="memberships_container">



                    <ul> 



                        <li  > <img src="<?php bloginfo('stylesheet_directory'); ?>/images/sla-membership-logo.png" alt="temp" /> <p class="member_num">9855 </p> </li>



                        <li  > <img src="<?php bloginfo('stylesheet_directory'); ?>/images/rla-membership-logo.png" alt="temp" /> <p class="member_num"> 37768/O</p></li>



                        <li class="col-lg-2 col-md-2 col-sm-2 col-xs-4"> <img src="<?php bloginfo('stylesheet_directory'); ?>/images/property-ombudsman-membership-logo.png" alt="temp" /> <p class="member_num"> D7229</p></li>



                        <li class="col-lg-1 col-md-2 col-sm-2 col-xs-4"> <img src="<?php bloginfo('stylesheet_directory'); ?>/images/ombudsman-services-membership.png" alt="temp" /> </li>



                        <li  > <img src="<?php bloginfo('stylesheet_directory'); ?>/images/safe-agent-membership-logo.png" alt="temp" /><p class="member_num">S4887 </p> </li>



                        <li  > <img src="<?php bloginfo('stylesheet_directory'); ?>/images/home-stamp--membership-logo.png" alt="temp" /> </li>


                        <li  > <img src="<?php bloginfo('stylesheet_directory'); ?>/images/arla-membership-logo.png" alt="temp" /> <p class="member_num"> M0200930</p></li>


                        <li  > <img src="<?php bloginfo('stylesheet_directory'); ?>/images/fsb-membership-logo.png" alt="temp" /> <p class="member_num"> 2177811</p></li>

                        <li  > <img src="<?php bloginfo('stylesheet_directory'); ?>/images/glm-membership-logo.png" alt="temp" /> <p class="member_num"> CF402</p></li>



                        <li  > <img src="<?php bloginfo('stylesheet_directory'); ?>/images/ukala-membership-logo.png" alt="temp" /><p class="member_num"> 145506</p></li>



                        <li  > <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nla-membership-logo.png" alt="temp" /><p class="member_num">  003145 </p></li>



                        



                    </ul>



                </div>
</div>
<script> 
jQuery('.memberships_container ul').bxSlider({
  minSlides: 1,
  maxSlides: 15,
  pager: false,
  slideMargin: 20,
  slideWidth: 170,
});
</script>
<div class="footer_lower">
    <div class="container"> 
    <div class="fleft">
        <p>  &copy; Leydon Lettings 2013 - <?php echo date("Y"); ?> </p>
    </div>
    <div class="fright">
        <a class="foota" href="/sitemap/">Sitemap</a> | <a class="foota" href="/privacy-policy/">Privacy Policy</a>
    </div>
   </div>
</div>
</div>
<!-- Responsive Footer End -->
<div class="container visible-xs">

</div>
<?php wp_footer(); ?>
</body>
</html>