<?php
/*
 Template Name: Page Template
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
		<div class="container">
<div class="subpage">
<p>Sub Pages</p>
<ul>
<li><a href="#">Company History</a></li>
<li><a href="#">Complaints Procedure</a></li>
<li><a href="#">The Team</a></li>
<li><a href="#">Latest</a></li>

</ul>
</div><!--subpage-->

<div class="innerpage">
<p class="innerpagehead">ABOUT LEYDON LETTINGS</p>
</div><!--innerpage-->

</div>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>