<?php
@session_start();
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
 
?>
<script>

</script>
<?php
$rooms=get_post_meta(get_the_ID(), '_rooms', true);
global $wpdb;
global $post;
/*$wheelchairquery = "
    SELECT DISTINCT(CAST(meta_value AS UNSIGNED)) AS met 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '_Wheelchair' AND post_id='".get_the_ID()."'
	ORDER BY met ASC
	LIMIT 1
 ";
 $wheel=$wpdb->get_results($wheelchairquery, ARRAY_N); */
$_wheel = get_post_meta(get_the_ID(), '_Wheelchair', true);
$_shower = get_post_meta(get_the_ID(), '_shared_shower', true);
$_parking = get_post_meta(get_the_ID(), '_driveway', true);
$_garden = get_post_meta(get_the_ID(), '_rear_garden', true);
$_shared_bath = get_post_meta(get_the_ID(), '_shared_bath', true);
// print_r($shower);
$rentslowquery = "
    SELECT DISTINCT(CAST(meta_value AS UNSIGNED)) AS met 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '%_rent_%' AND post_id='".get_the_ID()."'
	ORDER BY met ASC
	LIMIT 1
 ";
$rentslow = $wpdb->get_results($rentslowquery, ARRAY_N);
$rentshighquery = "
    SELECT DISTINCT(CAST(meta_value AS UNSIGNED)) AS met 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '%_rent_%' AND post_id='".get_the_ID()."'
	ORDER BY met DESC
	LIMIT 1
 ";
$rentshigh = $wpdb->get_results($rentshighquery, ARRAY_N);
$availabilitythisquery = "
    SELECT count(*) 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '".get_option('at_current_year')."_%' AND meta_value = 'Yes' AND post_id='".get_the_ID()."'
 ";
$availabilitythis = $wpdb->get_results($availabilitythisquery, ARRAY_N);
$athiswidth=0;
$athis=$availabilitythis[0][0];
$athiswidth=$athis/$rooms*100;
$thisrestwidth=100-$athiswidth;
$availabilitynextquery = "
    SELECT count(*) 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '".get_option('at_next_year')."_%' AND meta_value = 'Yes' AND post_id='".get_the_ID()."'
 ";
$availabilitynext = $wpdb->get_results($availabilitynextquery, ARRAY_N);
$anextwidth=0;
$anext=$availabilitynext[0][0];
$anextwidth=$anext/$rooms*100;
$anextrestwidth=100-$anextwidth;
?>
<?php 
if(!empty($_GET['locations']))
{
	$loc=$_GET['locations'];
}
else
{
	$loc='_kent';
	$distance='';
}
$distance= get_post_meta(get_the_ID(),$loc , true); ?>
<?php 
if($loc=='_kent'){ $loce='UKC';}
elseif($loc=='_uc1'){ $loce='CCCU';}
elseif($loc=='_uca'){ $loce='UCA';}
elseif($loc=='_canterbury'){ $loce='Canterbury';}
$_SESSION['in']+=1;
?>

<tr class="tr<?= $_SESSION['in']%2==0?'odd':'even' ?>">
    <?php /* ?>
<td colspan="2" align="center">
<?php
$sper=100/$rooms;
for($i=1;$i<=$rooms;$i++)
{
	$_av = get_post_meta($id, get_option('at_current_year').'_'.$i, true);
	$_value = get_post_meta($id, '_rent_'.$i, true);	
	$j=$_av=='No'?'background-color:#c93939; color:#f8f8f8;':'background-color: #61e54d;color:#272727';
	?>
    <div class="rmvalue"  style="border-right:1px solid #fff;font-size: 12px;line-height: 29px; height:28px;width:<?= $sper ?>%; float:left;<?php echo $j ?>"><?php echo '&pound;'.$_value; ?></div>
    <?php
}
?>

 <?php */ ?>
<?php /* - SECONDARY START?>

<div class="indiv" style="height:auto; float:left;background:#e84848;overflow:hidden; width:<?php echo $thisrestwidth ?>%">&nbsp;</div>

<div class="indiv"  style=" height:auto; float:left;background:#d3ffcd;overflow:hidden; width:<?php echo $athiswidth ?>%"><?php echo $athiswidth>0?$athis.' of '.$rooms:''; ?></div> 

<?php SECONDARY END*/?>
<?php /* ?>
</td>
 <?php */ ?>
<td bgcolor="" align="left" id="alleft" class="wheel"><a style="color:inherit"  href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    <?php /* $fields=array(array('label'=>'Wheelchair Friendly','type'=>'select','name'=>'_Wheelchair','img'=>'Wheel'));
print_fields_front($fields,get_the_ID());*/?>
    <div class="hidden-xs hidden-sm pull-right">
    <?php if($_wheel=='Yes') {  ?>
    
        <div title="Wheelchair Friendly" class="wrapper-housing-icon srsic1">

        </div><?php } ?>
    <?php if($_shower!='0') { ?>
        <div title="En Suite" class="wrapper-housing-icon srsic2">
        <div class="ccount"><?php echo $_shower; ?></div>
        </div><?php } ?>
    <?php if($_parking!='') { ?>
        <div title="Parking Spaces" class="wrapper-housing-icon srsic3">

        </div><?php } ?>
    <?php if($_garden!='') { ?>
        <div title="Back Garden" class="wrapper-housing-icon srsic4">

        </div><?php } ?>
        <?php if($_shared_bath!='') { ?>
        <div title="Shared Bathrooms" class="wrapper-housing-icon srsic5">
        <div class="ccount"><?php echo $_shared_bath; ?></div>
        </div><?php } ?>
    </div>
</td>
<td align="center" bgcolor=""><?php echo get_post_meta(get_the_ID(), '_postal_code', true); ?></td>
<td align="center" bgcolor=""><?php echo $rooms; ?></td>


<?php /*?><td bgcolor="#FFFFFF" align="center" ><span class="priceval">£<?php echo $rentslow[0][0]  ?></span> to <span class="priceval">£<?php echo $rentshigh[0][0]  ?></span></td><?php */?>
<td align="center" bgcolor="" style="width: 86px;" ><?php if($distance)
{ echo $distance  ?> Miles <?php }  ?></td>
<td bgcolor=""><?php /*?><div class="tablebtn" style="float:left; width:55px;background-position: 93%;" id="alleft"><a class="predef" href="#" onclick="showMore(<?php echo get_the_ID(); ?>)"></a></div><?php */?><div class="tablebtn" id="alleft"><a href="<?php the_permalink(); ?>"><span class="glyphicon glyphicon-search"></span> </a></div></td>
<td colspan="2" align="center">
<?php
$sper=100/$rooms;
for($i=1;$i<=$rooms;$i++)
{
	$_av = get_post_meta($id, get_option('at_next_year').'_'.$i, true);
	$_value = get_post_meta($id, '_rent_'.$i, true);	
	$j=$_av=='No'?'background-color:#c93939; color:#f8f8f8;':'background-color: #61e54d;color:#272727';
	?>
    <div class="rmvalue" style="border-right:1px solid #fff;font-size: 12px;line-height: 29px; height:28px;width:<?= $sper ?>%; float:left;<?php echo $j ?>"><?php echo '&pound;'.$_value; ?></div>
   
    <?php
}
?>
<?php /*?>
<div class="indiv"  style=" float:left;background:#e84848; overflow:hidden; width:<?php echo $anextrestwidth ?>%">&nbsp;</div><div class="indiv"  style=" float:left;background:#d3ffcd;overflow:hidden; width:<?php echo $anextwidth ?>%"><?php echo $anextwidth>0?$anext.' of '.$rooms:''; ?></div> <?php */?> </td>
</tr>
<tr class="no-display" id="<?php echo get_the_ID(); ?>"> <td colspan="11" bgcolor="">
<?php
 	$fields=array(
	array('label'=>'Rent','type'=>'text','name'=>'_rent')	
	);
	//echo '<h4 class="title  htitle instab">Room Details/Prices</h4>';
	//echo '<h4 class="title htitle instab1">House Info</h4>';
		/*echo '<div class="binfo allinfo instab">';	
		$_rooms = get_post_meta(get_the_ID(), '_rooms', true);
		if($_rooms){
		print_extra_fields_table_front($fields,get_the_ID());
		}
	echo '</div>';  */
	
	echo '<div class="hinfo allinfo instab1" style="width:100%">';	
	print_extra_fields_table_front_only($fields,get_the_ID());
	echo '</div>';
?>
</td></tr>
<script type="text/javascript">
jQuery(function(){
	jQuery( ".predef" ).click(function( event ) {
  event.preventDefault();
				});
	jQuery('.indiv').each(function(index, element) {
        jQuery(this).height(jQuery(this).parent('td').height())
		
		
    });
	
	})
</script>