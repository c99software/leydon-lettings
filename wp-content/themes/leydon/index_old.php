<?php
/**
 * Template Name: TEMPLATE: Landing Page
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
            


            <div class="container slider-container">

                <div class="sliderbody">
                    <div class="slider">
                        <div class="visible-xs col-xs-12">
                            <div id="target-audience">
                                <div class="col-xs-12 col-sm-4">
                                    <div class="frontpagefocus">
                                        <a href="/procedures/"><img src="/wp-content/uploads/2014/07/landlords.jpg" alt="Landlords" title="Landlords" class="img-responsive" />
                                            <h4 class="hfocus">Landlords</h4></a>
                                    </div><!-- #Front Page Focus END -->
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <div class="frontpagefocus">
                                        <a href="/booking-process/"><img src="/wp-content/uploads/2014/07/prospective-tenants2.jpg" alt="Prospective Tenants" title="Prospective Tenants" class="img-responsive"/>
                                            <h4 class="hfocus">Students</h4></a>
                                    </div><!-- #Front Page Focus #END -->
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <div class="frontpagefocus-last">
                                        <a href="/why-leydon-lettings/"><img src="/wp-content/uploads/2014/07/current-tenants.jpg" alt="Current Tenants" title="Current Tenants" class="img-responsive"/>
                                            <h4 class="hfocus">Professionals</h4></a>
                                    </div><!-- #Front Page Focus Last #END -->
                                </div>
                                <div class="clearBoth"></div>
                            </div><!-- #Target Audience #END -->
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <div id="button-div">
                                <ul>
                                    <li class="col-xs-6 col-sm-12"><a href="/available-canterbury-student-accommodation/?room=1" id="bed1">1 BEDROOM </a></li>
                                    <li class="col-xs-6 col-sm-12"><a href="/available-canterbury-student-accommodation/?room=2" id="bed2">2 BEDROOMS</a></li>
                                    <li class="col-xs-6 col-sm-12"><a href="/available-canterbury-student-accommodation/?room=3" id="bed3">3 BEDROOMS</a></li>
                                    <li class="col-xs-6 col-sm-12"><a href="/available-canterbury-student-accommodation/?room=4" id="bed4">4 BEDROOMS</a></li>
                                    <li class="col-xs-6 col-sm-12"><a href="/available-canterbury-student-accommodation/?room=5" id="bed5">5 BEDROOMS</a></li>
                                    <li class="col-xs-6 col-sm-12"><a href="/available-canterbury-student-accommodation/?room=6" id="bed6">6 BEDROOMS</a></li>
                                    <li class="col-xs-6 col-sm-12"><a href="/available-canterbury-student-accommodation/?room=7" id="bed6">7 BEDROOMS</a></li>
                                    <li class="col-xs-12 col-sm-12"><a href="/available-canterbury-student-accommodation/" id="b1"  >View all  </a></li>
                                </ul>
                            </div> <!--#button div-->
                        </div>

                        <div class="hidden-xs col-xs-12 col-sm-9 col-slider">
                            <div id="slider-div">
                                <?php echo do_shortcode("[metaslider id=271]"); ?>
                            </div><!--#slider div-->
                        </div>

                    </div><!-- #slider -->
                </div><!-- #siledrbody -->
                <div class="container target-audience-container hidden-xs">
                    <div id="target-audience">
                        <div class="col-xs-12 col-sm-4">
                            <div class="frontpagefocus">
                                <a href="/procedures/"><img src="/wp-content/uploads/2014/07/landlords.jpg" alt="Landlords" title="Landlords" class="img-responsive" />
                                    <h4 class="hfocus">Landlords</h4></a>
                            </div><!-- #Front Page Focus END -->
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="frontpagefocus">
                                <a href="/booking-process/"><img src="/wp-content/uploads/2014/07/prospective-tenants2.jpg" alt="Prospective Tenants" title="Prospective Tenants" class="img-responsive"/>
                                    <h4 class="hfocus">Students</h4></a>
                            </div><!-- #Front Page Focus #END -->
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <div class="frontpagefocus-last">
                                <a href="/why-leydon-lettings/"><img src="/wp-content/uploads/2014/07/current-tenants.jpg" alt="Current Tenants" title="Current Tenants" class="img-responsive"/>
                                    <h4 class="hfocus">Professionals</h4></a>
                            </div><!-- #Front Page Focus Last #END -->
                        </div>
                        <div class="clearBoth"></div>
                    </div><!-- #Target Audience #END -->
                </div>
                <div class="hidden-xs col-sm-12 feature-container">
                    <h3 id="ftitle">Features</h3>

                    <div class="col-xs-6 col-sm-3">
                        <ul class="feature-list">
                            <li>Broadband points in every room</li>
                            <li>TV-bills-package</li>
                            <li>Free gardening</li>
                        </ul>
                    </div><!-- #fcol1 END -->

                    <div class="col-xs-6 col-sm-3">
                        <ul class="feature-list">
                            <li>En suites</li>
                            <li>Telephone Sockets</li>
                            <li>Same day maintenance</li>
                        </ul>
                    </div><!-- #fcol2 END -->

                    <div class="col-xs-6 col-sm-3">
                        <ul class="feature-list">
                            <li>Double kitchens</li>
                            <li>Washer / Dryers</li>
                            <li>Parking</li>
                        </ul>
                    </div><!-- #fcol3 END -->

                    <div class="col-xs-6 col-sm-3">
                        <ul class="feature-list">
                            <li>Double beds</li>
                            <li>Dishwashers</li>
                            <li>Outside Storage</li>
                        </ul>
                    </div><!-- #fcol4 END -->
                </div>
                <div class="clearBoth"></div>
                <div class="memberships_container">

                    <ul> 

                        <li class="col-lg-1 col-md-1 col-sm-2 col-xs-4"> <img src="<?php bloginfo('stylesheet_directory'); ?>/images/sla-membership-logo.png" alt="temp" />  </li>

                        <li class="col-lg-1 col-md-1 col-sm-2 col-xs-4"> <img src="<?php bloginfo('stylesheet_directory'); ?>/images/rla-membership-logo.png" alt="temp" /> <p class="member_num"> 37768/O</p></li>

                        <li class="col-lg-2 col-md-2 col-sm-2 col-xs-4"> <img src="<?php bloginfo('stylesheet_directory'); ?>/images/property-ombudsman-membership-logo.png" alt="temp" /> <p class="member_num"> D7229</p></li>

                        <li class="col-lg-2 col-md-2 col-sm-2 col-xs-4"> <img src="<?php bloginfo('stylesheet_directory'); ?>/images/ombudsman-services-membership.png" alt="temp" /> </li>

                        <li class="col-lg-1 col-md-1 col-sm-2 col-xs-4"> <img src="<?php bloginfo('stylesheet_directory'); ?>/images/safe-agent-membership-logo.png" alt="temp" /><p class="member_num">S4887 </p> </li>

                        <li class="col-lg-1 col-md-1 col-sm-2 col-xs-4"> <img src="<?php bloginfo('stylesheet_directory'); ?>/images/home-stamp--membership-logo.png" alt="temp" /> </li>

                        <li class="col-lg-1 col-md-1 col-sm-2 col-xs-4"> <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nfopp-membership-logo.png" alt="temp" /> </li>

                        <li class="col-lg-1 col-md-1 col-sm-2 col-xs-4"> <img src="<?php bloginfo('stylesheet_directory'); ?>/images/glm-membership-logo.png" alt="temp" /> <p class="member_num"> CF402</p></li>

                        <li class="col-lg-1 col-md-1 col-sm-2 col-xs-4"> <img src="<?php bloginfo('stylesheet_directory'); ?>/images/ukala-membership-logo.png" alt="temp" /><p class="member_num"> 145506</p></li>

                        <li class="col-lg-1 col-md-1 col-sm-2 col-xs-4"> <img src="<?php bloginfo('stylesheet_directory'); ?>/images/nla-membership-logo.png" alt="temp" /><p class="member_num">  003145 </p></li>

                        

                    </ul>

                </div>
            </div>
            <div class="clearBoth"></div>
            <br/>

        </div><!-- #content END-->
    </div><!-- #primary END-->
    </div><!-- #main END-->
<?php get_footer(); ?>