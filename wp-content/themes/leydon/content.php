<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
<div class="post-div">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		

		<?php if ( is_single() ) : ?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php else : ?>
		<h2 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
		</h2>
		<?php endif; // is_single() ?>

		<div class="entry-meta">
			<?php twentythirteen_entry_meta(); ?>
			<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
		</div><!-- .entry-meta -->
	  
	</header><!-- .entry-header -->
<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
		<div class="entry-thumbnail-content">
			<?php the_post_thumbnail(); ?>
		</div>
		<?php endif; ?>
	<?php if ( is_search() || is_category() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
	</div><!-- .entry-content -->
	<?php endif; ?>

  <footer id="social-share">	   <div id="sharethis">
<span class='st_fblike' displayText='Facebook Like'></span>
<span class='st_plusone' displayText='Google +1'></span>
<span class='st_twitter' displayText='Tweet' st_via='leydonlettings' st_username='leydonlettings'></span>
<span class='st_linkedin' displayText='LinkedIn'></span>
<span class='st_reddit' displayText='Reddit'></span>
<span class='st_stumbleupon' displayText='StumbleUpon'></span>
	  </div></footer>
</article><!-- #post -->

</div>