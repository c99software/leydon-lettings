<?php /* Template Name: Sample Template  */ get_header(); ?>

<style>
    .innerpage {
        width: 100%;
        margin-left:0;
    }
</style>
<div class="container">
    <div class="col-xs-12">
        <div class="innerpage">
            <p class="innerpagehead"><?php the_title(); ?></p>
            <?php /* The loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


                    <div class="entry-content">
                        <?php the_content(); ?>

                    </div><!-- .entry-content -->


                </article><!-- #post -->


            <?php endwhile; ?>

        </div><!--innerpage-->
    </div>
</div><!--contentallign-->


<?php get_footer();?></div><!--content-->