<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

<div class="container page_style">
	<div class="row">
        <?php RethinkBreadcrumb();?>
    </div>
	<div class="row">
		<div class="col-xs-12 col-sm-8">
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
		  		<?php get_template_part( 'content', get_post_format() ); ?>		
		  		<?php #twentythirteen_post_nav(); ?>
			<?php endwhile; ?>
			<div id="related-posts">
				<?php wp_related_posts()?>
			</div><!-- #related posts END -->  
		  	<?php if ( function_exists( 'get_author_bio_box' ) ) echo get_author_bio_box(); ?>
			  	<?php comments_template(); ?>
		</div><!-- innerpage END -->
		<?php get_sidebar('sidebar-1'); ?>

	</div><!-- contentallign END -->
</div><!-- contentallign END -->

<?php get_footer();?></div><!-- content END-->