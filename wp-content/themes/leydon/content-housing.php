 <?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */


?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>


<?php 
    $current_id = get_the_ID();
    $postcode = get_post_meta(get_the_ID(),'_postal_code',true);
    $rooms = get_post_meta(get_the_ID(),'_rooms' , true); 
    $tax = wp_get_post_terms(get_the_ID(),'type',array( 'fields' => 'slugs'));
    $pcm = number_format(4.33333 * preg_replace("/[^0-9.]/","",get_post_meta(get_the_ID(),'_pcm' , true)),2,'.',',');
    $pw = preg_replace("/[^0-9.]/","",get_post_meta(get_the_ID(),'_pcm' , true));
    $preview = substr(get_the_content(),0,100).'...';
    $pcm_n = number_format(4.333333 * preg_replace("/[^0-9.]/","",get_post_meta(get_the_ID(),'_pcm_n' , true)),2,'.',','); 
    $ensuite = get_post_meta($currentid,'_shared_shower', true);
   
    $drive = get_post_meta(get_the_ID(),'_driveway' , true);
    $bath = get_post_meta(get_the_ID(), '_shared_bath', true);
    $disab = get_post_meta(get_the_ID(), '_Wheelchair', true);
    $garden = get_post_meta(get_the_ID(), '_rear_garden', true);

    $ukct = get_post_meta($current_id,'_kentt' , true);
    $cccut = get_post_meta($current_id,'_uclt' , true);
    $ucat = get_post_meta($current_id,'_ucat' , true);
    $citycentret = get_post_meta($current_id,'_canterburyt' , true);
    $ukc = get_post_meta($current_id,'_kent' , true);
    $cccu = get_post_meta($current_id,'_ucl' , true);
    $uca = get_post_meta($current_id,'_uca' , true);

    $parent_guaran = get_post_meta(get_the_ID(),'_guarantor' , true); 
    $deposit = get_post_meta(get_the_ID(),'_deposit' , true); 
    $deposit_amount = get_post_meta(get_the_ID(),'_deposit_amount' , true); 
    $non_ref_admin = get_post_meta(get_the_ID(),'_nonrefundfee' , true); 
    $max_students = get_post_meta(get_the_ID(),'_maxstudents' , true); 
    $contract_type = get_post_meta(get_the_ID(),'_contracttype' , true); 

    if($deposit == 'Required' && $deposit_amount != null){
        $deposit_print = $deposit."(&pound;".$deposit_amount.")";
    }
    else
    {
        $deposit_print = $deposit;
    }

    $toilet_total = 0;
    $toilet_total = $toilet_total + get_post_meta(get_the_ID(), '_shared_toilet', true);
    $prop_type = "";
    if($tax[0] == "professional" || $tax[1] == "professional"):  
        $prop_type = "Professional";
    elseif($tax[0] == "student-only" || $tax[1] == "student-only"):
        $prop_type = "Student";
    elseif($tax[0] == "student-only" && $tax[1] == "professional"): 
        $prop_type = "Student/Professional";
    elseif($tax[0] == "professional" && $tax[1] == "student-only"):   
        $prop_type = "Student/Professional";    
    endif;

    // Get related link

    $rel_args =array(
        "post_type" =>"housing",
        "posts_per_page" => $perpage,
        
    );  

    $rel_args['meta_query'][] = array(
        'key' => '_rooms',
        'value' => $rooms,
        'compare' => "="
    );

    
    if ($avail_this == "1"){
        $rel_args['meta_query'][] = array(
            'relation' => 'OR',
            array(
                'key' => '_availability_this_year_1',
                'value' => 'Yes',
                'compare' => "="
            ),
            array(
                'key' => '_availability_this_year_2',
                'value' => 'Yes',
                'compare' => "="
            ),
             array(
                'key' => '_availability_this_year_3',
                'value' => 'Yes',
                'compare' => "="
            ),
              array(
                'key' =>'_availability_this_year_4',
                'value' => 'Yes',
                'compare' => "="
            ),
               array(
                'key' => '_availability_this_year_5',
                'value' => 'Yes',
                'compare' => "="
            ),
                array(
                'key' => '_availability_this_year_6',
                'value' => 'Yes',
                'compare' => "="
            ),
                 array(
                'key' => '_availability_this_year_7',
                'value' => 'Yes',
                'compare' => "="
            )
        );
    }
       
    $introtext = get_post_meta(get_the_ID() , '_introtext', true);
    $related_query =new WP_Query($rel_args);  
?>  <div id="house_page">
        <div  class="container content_box section_1">
            <div class="row">
                <div class="main_col col-xs-12 col-sm-12 col-md-9">
                    <div class="title">
                        <h1><?php echo $rooms;?> Bedroom <?php echo $prop_type; ?> House</h1>
                        <h3> <?php echo $str_title = trim(preg_replace('/\s*\([^)]*\)/', '', get_the_title()));; ?>, <?php echo $postcode; ?> </h3>
                    </div>
                    <div class="price-box">
                        <p class="price">from <span>&pound;<?php echo $pw; ?></span>pw <span>|</span> <span>&pound;<?php echo $pcm; ?></span>pcm </p>
                    </div>
                    <p class="description"><?php echo $introtext; ?></p> 
                    <div class="image">
                        <div class="sliderhousing">
                            <?php
                            $key='_gallery_images';
                            $_value = get_post_meta($id, $key, true);
                            $attids=explode(',',$_value);
                            $main_image = ""; 
                            ?>
                            <div id="carousel-house" class="carousel" data-ride="carousel">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php

                                    if(count($attids)>0)
                                    {
                                        $k=0;
                                        foreach($attids as $attid){
                                            if(!empty($attid)){
                                                if(is_numeric($attid)){
                                                    $image = wp_get_attachment_image_src($attid, 'house-full',false);
                                                    $alt = get_post_meta($attid , '_wp_attachment_image_alt', true);

                                                    if(!empty($image))
                                                        ?>
                                    <div class="item <?php echo ($k==0)?'active':'';?>"><img src="<?php echo $image[0] ?>" alt="<?php echo $alt; ?>" title="<?php echo $alt; ?>"/></div>

                                                <?php
                                                    $main_image = $image[0];
                                                }
                                                else
                                                {
                                                    ?>
                                    <div class="item <?php echo ($k==0)?'active':'';?>"><img src="<?php echo $attid ?>" alt="<?php echo $alt; ?>" title="<?php echo $alt; ?>"/></div>
                                                <?php
                                                }
                                            }
                                            $k++;
                                        }
                                    }
                                    ?>
                                </div>                            
                            </div>
                            <div class="c-pager">
                                <ul class="">
                                    <?php
                                    if(count($attids)>0)
                                    {
                                        $k=0;
                                        $size = array(130,80);
                                        foreach($attids as $attid){
                                            if(!empty($attid)){
                                                if(is_numeric($attid)){
                                                    $image = wp_get_attachment_image_src($attid,'custom-size',false);
                                                    $alt = get_post_meta($attid , '_wp_attachment_image_alt', true);
                                                    //$immeta = wp_get_attachment_metadata( $attid);
                                                    //print_r($immeta);
                                                    if(!empty($image))
                                                        ?>
                                                        <li data-target="#carousel-house" data-slide-to="<?php echo $k ?>" <?php echo ($k==0)?'class="active"':'';?>>
                                                    <img src="<?php echo $image[0] ?>" width="<?php echo($image[1]); ?>" height="<?php echo($image[2]); ?>" alt="<?php echo $alt; ?>" title="<?php echo $alt ?>" />
                                                    </li>
                                                <?php
                                                }
                                                else
                                                {
                                                    ?>
                                                    <li data-target="#carousel-house" data-slide-to="<?php echo $k ?>" <?php echo ($k==0)?'class="active"':'';?>>
                                                        <img src="<?php echo $attid ?>" width="<?php echo($image[1]); ?>" height="<?php echo($image[2]); ?>" alt="<?php echo $alt; ?>" title="<?php echo $alt; ?>" />
                                                    </li>

                                                <?php
                                                }
                                            }
                                            $k++;
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div><!-- #SliderHousing END -->
                    </div>

                    <div class="enquire">
                        <p> Do you like this property? </p>
                        <a class="glob_btn lg" id="single-1" href="<?php bloginfo('template_url'); ?>/enquiry.php?pr=<?php the_title(); ?>&amp;name=Request More Details">Request Details</a>
                        <a class="glob_btn lg" id="single" href="<?php bloginfo('template_url'); ?>/enquiry.php?pr=<?php the_title(); ?>&amp;prid=<?php the_ID(); ?>&amp;name=Arrange a Viewing">Arrange a Viewing</a>
                    </div>
                </div>
                <div class="side_col hidden-xs hidden-sm col-md-3">
                    <div class="side_nav_links">
                        <a href="/available-canterbury-student-accommodation/"> <span><<</span>Back to results</a> 
                        <?php $counter2 = 0; ?>
                         <?php while($related_query->have_posts()): 
                            $related_query->the_post(); ?>
                            <?php if($counter2 == 0 && get_the_ID() != $current_id): ?>
                                <a href="<?php echo the_permalink(); ?>"> Next property <span>>></span></a>
                                <?php $counter2++; ?>
                            <?php endif; ?>
                         <?php endwhile; ?>
                         
                         <?php wp_reset_query(); ?>
                    </div>  
                    <div class="attributes">
                        <div class="attr_toil <?php if($toilet_total > 0): echo 'active'; endif;?> col-xs-2">
                            <div class="tick">
                                <?php if($toilet_total > 0): echo $toilet_total; else: echo '&#10003;'; endif; ?>
                            </div>
                            <div class="tooltip tooltip_arrow_gg">
                                 <?php if($toilet_total > 0): echo 'x'.$toilet_total; endif; ?> Toilet
                            </div>
                        </div>
                        <div class="attr_bath <?php if($bath > 0): echo 'active'; endif;?> col-xs-2">
                            <div class="tick">
                                <?php if($bath > 0): echo $bath; else: echo '&#10003;'; endif; ?>
                            </div>
                            <div class="tooltip tooltip_arrow_gg">
                                x<?php echo $bath; ?> Bath 
                            </div>
                        </div>
                        <div class="attr_shower <?php if($ensuite > 0): echo 'active'; endif;?> col-xs-2">
                            <?php if($ensuite > 0):?>
                            <div class="tick">
                                <?php echo $ensuite; ?>
                            </div>
                            <?php endif; ?>
                            <div class="tooltip tooltip_arrow_gg">
                                x<?php echo $ensuite; ?> Shower 
                            </div>
                        </div>
                        <div class="attr_park <?php if($garden == 'Yes'): echo 'active'; endif;?> col-xs-2">
                            <?php if($drive == 'Yes'):?>
                            <div class="tick">
                                <?php echo '&#10003;';?>
                            </div>
                            <?php endif; ?>
                            <div class="tooltip tooltip_arrow_gg">
                                Driveway
                            </div>
                        </div>
                        <div class="attr_garden <?php if($garden == 'Yes'): echo 'active'; endif;?> col-xs-2">
                            <?php if($garden == 'Yes'):?>
                            <div class="tick">
                                <?php echo '&#10003;';?>
                            </div>
                            <?php endif; ?>
                            <div class="tooltip tooltip_arrow_gg">
                                Garden 
                            </div>
                        </div>
                         <div class="attr_disab <?php if($disab == 'Yes'): echo 'active'; endif;?> col-xs-2">
                            <?php if($disab == 'Yes'):?>
                            <div class="tick">
                                <?php echo '&#10003;';?>
                            </div>
                            <?php endif; ?>
                            <div class="tooltip tooltip_arrow_gg">
                                Disabled Access 
                            </div>
                        </div>
                    </div>
                   
                    <div class="details">

                        <h2> House Details </h2>
                        <p>UK Guarantor: <span><?php echo $parent_guaran; ?></span></p>
                        <p>Deposit: <span><?php echo $deposit_print; ?></span></p>
                        <p>Admin Fee (non-refundable):<span> &pound;<?php echo $non_ref_admin; ?></span></p>
                        <?php if($bills_package != null): ?>
                         <p>Bills Package: <span><?php echo $bills_package; ?></span></p>
                        <?php endif; ?>
                        <?php if($pcm_n != null && $pcm_n != "" && $pcm_n > 0): ?>
                            <p>Price Next year(pcm): <span><?php echo $pcm_n; ?></span></p>
                        <?php endif; ?>

                        <a href="#">Fees</a>
                    </div>
                    <?php
                    $_address = get_post_meta(get_the_ID(), '_address', true);
                    $_postal_code = get_post_meta(get_the_ID(), '_postal_code', true);
                    if($_address){
                        echo ' <div id="mapviews" class="mapfront" ></div>';
                        echo '<script type="text/javascript">jQuery(function(){
                            addmap("'.$_address.', '.$_postal_code.'","'.$_address.', '.$_postal_code.'");
                        })</script>';
                    }
                    ?>
                    <div class="location">
                        <h2> Where is it?</h2>
                        <p>CCCU <span>- <?php echo $cccut; ?> <span>(<?php echo $cccu; ?>)</span></span></p>
                        <p>UCA <span>- <?php echo $ucat; ?> <span>(<?php echo $uca; ?>)</span></span></p>
                        <p>UKC <span>- <?php echo $ukct; ?> <span>(<?php echo $ukc; ?>)</span></span></p>
                        <p>City Centre <span>- <?php echo $citycentret; ?> mins <span>(<?php echo $citycentre; ?>)</span></span></p>
                    </div>
                    <div class="share_links">
                        <?php 
                            $crunchifyURL = get_permalink();
                     
                            // Get current page title
                            $crunchifyTitle = str_replace( ' ', '%20', get_the_title());
                            
                            // Get Post Thumbnail for pinterest
                            $crunchifyThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
                            
                            // Construct sharing URL without using any script
                            $twitterURL = 'https://twitter.com/intent/tweet?text='.$crunchifyTitle.'&amp;url='.$crunchifyURL.'&amp;';
                            $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$crunchifyURL;
                            $googleURL = 'https://plus.google.com/share?url='.$crunchifyURL;
                            
                            // Based on popular demand added Pinterest too
                            $pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$crunchifyURL.'&amp;media='.$crunchifyThumbnail[0].'&amp;description='.$crunchifyTitle;
                        ?>
                        <div class="crunchify-social">
                            <h2> Show your friends!</h2>
                            <a class="crunchify-link fb" href="'.$facebookURL.'" target="_blank"></a>
                            <a class="crunchify-link tw" href="'. $twitterURL .'" target="_blank"></a>
                            <!-- <a class="crunchify-link linkedin" href="'.$googleURL.'" target="_blank"></a> -->
                            <!-- <a class="crunchify-link crunchify-pinterest" href="'.$pinterestURL.'" target="_blank"></a> -->
                            <a class="crunchify-link plus" href="'.$googleURL.'" target="_blank"></a>
                            <a class="email" href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo $crunchifyURL; ?>"
                               title="Share by Email">
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="container content_box section_2">
            <div class="row">
                <div class="main_col col-xs-12 col-sm-12 col-md-9">
                    <div class="house_tab_nav">
                        <ul>
                            <li> <a class="active" data-housetab="1" href="javascript:void(0);">Description</a></li>
                            <li> <a data-housetab="2" href="javascript:void(0);">Information</a></li>
                            <li> <a data-housetab="3" href="javascript:void(0);">Rooms</a></li>
                            <li> <a data-housetab="4" href="javascript:void(0);">Floor Plans</a></li>
                            <li> <a data-housetab="5" href="javascript:void(0);">Certificates</a></li>
                            <li> <a class="emerg" data-housetab="6" href="javascript:void(0);">Emergencies</a></li>
                        <ul>
                    </div>
                    <div class="house_tab_cont">
                        <ul>
                            <li class="active" id="housetab_1">
                                <?php the_content(); ?>
                            </li>
                            <li style="display:none" id="housetab_2">
                                <?php
                                    $fields=array(
                                        array('label'=>'<b>Power Sockets - (per room)</b>','type'=>'select','name'=>'_power_sockets'),
                                        array('label'=>'<b>TV License</b>','type'=>'select','name'=>'_tv_license','options'=>$_yesno),
                                        array('label'=>'<b>Outhouse (WC)</b>','type'=>'select','name'=>'_outhouse_wc','options'=>$_yesno),
                                        array('label'=>'<b>Outhouse (Bike Storage)</b>','type'=>'select','name'=>'_outhouse_bike','options'=>$_yesno),
                                        array('label'=>'<b>Utility Room</b>','type'=>'select','name'=>'_utility_room'),
                                        array('label'=>'<b>Shared Shower</b>','type'=>'select','name'=>'_shared_shower'),
                                        array('label'=>'<b>Ensuite</b>','type'=>'select','name'=>'_en_suite'),
                                        array('label'=>'<b>Shared Toilet</b>','type'=>'select','name'=>'_shared_toilet'),
                                        array('label'=>'<b>Shared Sink</b>','type'=>'select','name'=>'_shared_sink'),
                                        array('label'=>'<b>Shared Bath</b>','type'=>'select','name'=>'_shared_bath'),
                                        array('label'=>'<b>Boiler Type</b>','type'=>'text','name'=>'_boiler_type'),
                                        array('label'=>'<b>Cavity Insulation - Wall</b>','type'=>'select','name'=>'cavity_insulation_wall'),
                                        array('label'=>'<b>Cavity Insulation - Loft</b>','type'=>'select','name'=>'cavity_insulation_loft'),
                                        array('label'=>'<b>Driveway (Max Spaces)</b>','type'=>'select','name'=>'_driveway'),
                                        array('label'=>'<b>Parking (Permit - available from CCC)</b>','type'=>'select','name'=>'_parkingpermit'),
                                        array('label'=>'<b>Parking (On Road)</b>','type'=>'singlecheck','name'=>'_parking_road'),
                                        array('label'=>'<b>Parking (Road Bays)</b>','type'=>'singlecheck','name'=>'_parking_bay'),
                                        array('label'=>'<b>Garden Shed</b>','type'=>'select','name'=>'_garden_shed','options'=>$_yesno),
                                        array('label'=>'<b>Front Garden</b>','type'=>'select','name'=>'_front_garden','options'=>$_yesno),
                                        array('label'=>'<b>Rear Garden</b>','type'=>'select','name'=>'_rear_garden','options'=>$_yesno),
                                        array('label'=>'<b>Bus Routes</b>','type'=>'textarea','name'=>'_bus_routes'),
                                    );
                                    $fields2=array(
                                        array('label'=>'<b>Shared Shower</b>','type'=>'select','name'=>'_shared_shower'),
                                        array('label'=>'<b>Shared Toilet</b>','type'=>'select','name'=>'_shared_toilet'),
                                        array('label'=>'<b>Shared Sink</b>','type'=>'select','name'=>'_shared_sink'),
                                        array('label'=>'<b>Ensuite</b>','type'=>'select','name'=>'_en_suite'),
                                        array('label'=>'<b>Utility Room</b>','type'=>'select','name'=>'_utility_room'),
                                        array('label'=>'<b>Outhouse (WC)</b>','type'=>'select','name'=>'_outhouse_wc','options'=>$_yesno),
                                        array('label'=>'<b>Outhouse (Bike Storage)</b>','type'=>'select','name'=>'_outhouse_bike','options'=>$_yesno),
                                        array('label'=>'<b>Garden Shed</b>','type'=>'select','name'=>'_garden_shed','options'=>$_yesno),
                                        array('label'=>'<b>Front Garden</b>','type'=>'select','name'=>'_front_garden','options'=>$_yesno),
                                        array('label'=>'<b>Rear Garden</b>','type'=>'select','name'=>'_rear_garden','options'=>$_yesno),
                                        array('label'=>'<b>Power Sockets - (per room)</b>','type'=>'select','name'=>'_power_sockets'),
                                        array('label'=>'<b>TV License</b>','type'=>'select','name'=>'_tv_license','options'=>$_yesno),
                                        /* 
                                        array('label'=>'<b>Fridge</b>','type'=>'select','name'=>'_fridge'),
                                        array('label'=>'<b>Kitchen Type</b>','type'=>'select','name'=>'_kitchen_type'),
                                        array('label'=>'<b>Fridge</b>','type'=>'select','name'=>'_fridge'),
                                        array('label'=>'<b>Freezer</b>','type'=>'select','name'=>'_freezer'),
                                        array('label'=>'<b>Wash Machine</b>','type'=>'select','name'=>'_wash_machine'),
                                        array('label'=>'<b>Dryer</b>','type'=>'select','name'=>'_dryer'),
                                        array('label'=>'<b>Microwave</b>','type'=>'select','name'=>'_microwave'),
                                        array('label'=>'<b>Hobs</b>','type'=>'select','name'=>'_hobs'),
                                        array('label'=>'<b>Hood Extractor Fans</b>','type'=>'select','name'=>'_hood_extractor_fans'),
                                        array('label'=>'<b>Washer Dryer</b>','type'=>'select','name'=>'_washerdryer'),
                                        array('label'=>'<b>Dishwasher</b>','type'=>'select','name'=>'_dishwasher'),
                                        array('label'=>'<b>Kitchen Sinks</b>','type'=>'text','name'=>'_kitchen_sinks'),
                                        array('label'=>'<b>Oven Type</b>','type'=>'select','name'=>'_oven_type'),
                                        array('label'=>'<b>Floor Type</b>','type'=>'select','name'=>'_floor_type'),
                                        */
                                    );
                                    $fields3=array(
                                        array('label'=>'<b>Bus Routes</b>','type'=>'textarea','name'=>'_bus_routes'),
                                        array('label'=>'<b>Driveway (Max Spaces)</b>','type'=>'select','name'=>'_driveway'),
                                        array('label'=>'<b>Parking (Permit - available from CCC)</b>','type'=>'select','name'=>'_parkingpermit'),
                                        array('label'=>'<b>Parking (On Road)</b>','type'=>'singlecheck','name'=>'_parking_road'),
                                        array('label'=>'<b>Parking (Road Bays)</b>','type'=>'singlecheck','name'=>'_parking_bay'),
                                        /* WAS KITCHIN 
                                        array('label'=>'<b>Bills package</b>','type'=>'select','name'=>'_bills_package'),
                                        array('label'=>'<b>Telephone Line (incoming only)</b>','type'=>'select','name'=>'_telephone_line_bill','options'=>$_yesno),
                                        array('label'=>'<b>Broadband Wireless</b>','type'=>'select','name'=>'_broadband_wireless_bill','options'=>$_yesno),
                                        array('label'=>'<b>TV License</b>','type'=>'select','name'=>'_tv_license_bill','options'=>$_yesno),
                                        array('label'=>'<b>TV</b>','type'=>'select','name'=>'_tv_bill','options'=>$_yesno),
                                        array('label'=>'<b>Gas</b>','type'=>'select','name'=>'_gas_bill','options'=>$_yesno),
                                        array('label'=>'<b>Electricity</b>','type'=>'select','name'=>'_electricity_bill','options'=>$_yesno),
                                        array('label'=>'<b>TV Coaxial - to all rooms</b>','type'=>'select','name'=>'_tv_coaxial_bill','options'=>$_yesno),
                                        array('label'=>'<b>Sewerage</b>','type'=>'select','name'=>'_sewerage_bill','options'=>$_yesno),
                                        array('label'=>'<b>Water</b>','type'=>'select','name'=>'_water_bill','options'=>$_yesno),
                                        */
                                    );
                                    $fields4=array(
                                            array('label'=>'<b>Thermostat</b>','type'=>'textarea','name'=>'_thermostat_location'),
                                            array('label'=>'<b>Hot Water and Heating Controls</b>','type'=>'textarea','name'=>'_hot_water_location'),
                                            array('label'=>'<b>Boiler</b>','type'=>'textarea','name'=>'_boiler_location'),
                                            array('label'=>'<b>Stop Cock</b>','type'=>'textarea','name'=>'_stop_cock_location'),
                                            array('label'=>'<b>Fuse Box</b>','type'=>'textarea','name'=>'_fuse_box_location'),
                                            array('label'=>'<b>Internet Box</b>','type'=>'textarea','name'=>'_internet_box_location'),
                                            array('label'=>'<b>Electric Meter</b>','type'=>'textarea','name'=>'_electric_meter_location'),
                                            array('label'=>'<b>Gas Meter</b>','type'=>'textarea','name'=>'_gas_meter_location'),
                                            array('label'=>'<b>Gas Detector</b>','type'=>'textarea','name'=>'_gas_detector_location')
                                    );
                                    $fields5=array(
                                        array('label'=>'<b>Boiler Type</b>','type'=>'text','name'=>'_boiler_type'),
                                        array('label'=>'<b>Cavity Insulation - Wall</b>','type'=>'select','name'=>'cavity_insulation_wall'),
                                        array('label'=>'<b>Cavity Insulation - Loft</b>','type'=>'select','name'=>'cavity_insulation_loft'),
                                        /* WAS BILLS 
                                        array('label'=>'<b>Burglar Alarm</b>','type'=>'select','name'=>'_balarm','options'=>$_yesno),
                                        array('label'=>'<b>Smoke Alarm</b>','type'=>'select','name'=>'_salarm','options'=>$_yesno),
                                        array('label'=>'<b>Heat Detector</b>','type'=>'select','name'=>'_heatdet','options'=>$_yesno),
                                        array('label'=>'<b>Carbon Monoxide Detector</b>','type'=>'select','name'=>'_cardet','options'=>$_yesno),
                                        array('label'=>'<b>Fire Alarm Unit</b>','type'=>'select','name'=>'_fam','options'=>$_yesno),
                                        */
                                    );
                                    echo '<div class="house-info ">';
                                    echo '<h4 class="title htitle">House</h4>';
                                    print_fields_front($fields2,get_the_ID());
                                    echo '</div>';

                                   
                                    echo '<div class="kitchen-info">';
                                    echo '<h4 class="title  htitle">Travel</h4>';
                                    print_fields_front($fields3,get_the_ID());
                                    echo '</div>';

                                    

                                    echo '<div class="bills-info ">';
                                    echo '<h4 class="title  htitle">Energy</h4>';
                                    print_fields_front($fields5,get_the_ID());
                                    echo '</div>'; 
                                ?>
                            </li>
                            <li style="display:none" id="housetab_3">
                                 <?php
                                $fields=array(
                                    array('label'=>'Rent','type'=>'text','name'=>'_rent'),
                                    array('label'=>'Size','type'=>'text','name'=>'_size'),
                                    array('label'=>'Bed Size','type'=>'text','name'=>'_bed_size'),
                                    array('label'=>'Location','type'=>'text','name'=>'_location'),
                                    array('label'=>'Availability this Academic year','type'=>'text','name'=>get_option('at_current_year')),
                                    array('label'=>'Availability next academic year','type'=>'text','name'=>get_option('at_next_year')),
                                    array('label'=>'Shower En-suite','type'=>'radio','name'=>'_shower_ensuite','options'=>array('Yes','No')),
                                    array('label'=>'Sink En-suite','type'=>'radio','name'=>'_sink_ensuite','options'=>array('Yes','No')),
                                    array('label'=>'Toilet En-suite','type'=>'radio','name'=>'_toilet_ensuite','options'=>array('Yes','No')),
                                    array('label'=>'Disabled Friendly','type'=>'radio','name'=>'_disabled_friendly','options'=>array('Yes','No')),
                                    array('label'=>'Bedroom lock','type'=>'radio','name'=>'_mortis_door_lock','options'=>array('Yes','No')),
                                    array('label'=>'Floor','type'=>'text','name'=>'_floor'),
                                    array('label'=>'Desk and Chair','type'=>'radio','name'=>'_desk_chair','options'=>array('Yes','No')),
                                    //array('label'=>'Mattress Type','type'=>'text','name'=>'_mattress_type'),
                                    array('label'=>'Wardrobe','type'=>'radio','name'=>'_wardrobe','options'=>array('Yes','No')),
                                    array('label'=>'Wardrobe inc Chest of Drawers','type'=>'radio','name'=>'_wardrobe_inc','options'=>array('Yes','No')),
                                    array('label'=>'Chest of Drawers','type'=>'radio','name'=>'_chest_drawers','options'=>array('Yes','No')),
                                    array('label'=>'Bedside Table','type'=>'radio','name'=>'_bedside_table','options'=>array('Yes','No')),
                                    array('label'=>'Hard-wired Broadband','type'=>'radio','name'=>'_hard_wired_broadband','options'=>array('Yes','No')),
                                    array('label'=>'Wireless Broadband','type'=>'radio','name'=>'_wireless_broadband','options'=>array('Yes','No')),
                                    array('label'=>'Telephone Line','type'=>'radio','name'=>'_telephone_line','options'=>array('Yes','No')),
                                    array('label'=>'TV Aerial','type'=>'radio','name'=>'_tv_aerial','options'=>array('Yes','No')),
                                );
                                echo '<div class="binfo ">';
                                $_rooms = get_post_meta(get_the_ID(), '_rooms', true);
                                if($_rooms){
                                    print_extra_fields_table_front($fields,get_the_ID());
                                }
                                echo '</div>';
                                ?>
                            </li>
                            <li style="display:none" id="housetab_4">
                                <?php
                                    $key='_floorplans';
                                    $_value = get_post_meta($id, $key, true);
                                    $attids=explode(',',$_value);
                                    ?>

                                    <?php

                                    if(count($attids)>=1)
                                    {
                                        foreach($attids as $attid){
                                            if(!empty($attid)){
                                                if(is_numeric($attid)){
                                                    $image = wp_get_attachment_image_src($attid, 'large');
                                                    $alt = get_post_meta($attid , '_wp_attachment_image_alt', true);

                                                    ?>
                                                    <a class="fancybox" rel="group" href="<?php echo $image[0] ?>"><img class="certimgs" src="<?php echo $image[0] ?>" alt="" /></a>

                                                <?php

                                                }

                                            }
                                        }
                                    }
                                    else
                                    {
                                        echo "<h4>Currently no floor plans available.</h4>";
                                    }
                                ?>
                            </li>
                            <li style="display:none" id="housetab_5">
                                <script type="text/javascript">
                                    jQuery(document).ready(function() {
                                        jQuery(".fancybox").fancybox();
                                    });

        
                                </script>

                                <?php
                                $key2='_certificates_images';
                                $_value2 = get_post_meta($id, $key2, true);
                                $attids2=explode(',',$_value2);
                                ?>
                                <?php
                                if(count($attids2)>=1)
                                {
                                    foreach($attids2 as $attid2){
                                        if(!empty($attid2)){
                                            if(is_numeric($attid2)){
                                                $image2 = wp_get_attachment_image_src($attid2, $size);
                                                if(!empty($image2))
                                                    ?>
                                                    <a class="fancybox" rel="group" href="<?php echo $image2[0] ?>"><img class="certimgs" src="<?php echo $image2[0] ?>" alt="" /></a>
                                            <?php
                                            }
                                            else
                                            {
                                                ?>
                                                <li><img src="<?php echo $attid2 ?>" /></li>
                                            <?php
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    echo "<h4>Currently no certificates available.</h4>";
                                }
                                ?>
                            </li>
                            <li style="display:none" id="housetab_6">
                                <?php
                                echo '<div class="emergency-info ">';
                                echo '<h4 class="title  htitle emergency-header">Emergency Locations</h4>';
                                print_fields_front($fields4,get_the_ID());
                                echo '</div>';
                                ?>
                                <p></p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="side_col col-xs-12 col-sm-12 col-md-3">
                    <div class="useful_links">
                        <h3> Useful Information </h3>
                        <a href="/emergencies/">Tenant Emergencies</a>
                        <a href="/fault-finding/">Fault Finding</a>
                        <a href="/glossary/">Letting Glossary</a>
                        <a href="/faq/">FAQ</a>
                    </div>
                    <div class="last_updated">
                        <p>Last updated: <?php the_modified_date('F j, Y'); ?> at <?php the_modified_date('G:i'); ?></p>
                        <p>Leydon Lettings Agency makes no guarantee as to the accuracy of these details. Room measurements are approximate and are provided for guidance purposes only and should not be used as the sole basis for making any decision. Details provided are true to the best of our knowledge.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
     jQuery('.house_tab_nav a').on('click',function(){
            var tab_id = jQuery(this).attr('data-housetab');
            jQuery('.house_tab_nav a.active').removeClass('active');
            jQuery(this).addClass('active');
            jQuery('.house_tab_cont .active').hide();
            jQuery('#housetab_'+tab_id).fadeIn('fast');
            jQuery('#housetab_'+tab_id).addClass('active');
           
        });
   
        jQuery('.bxslider').bxSlider({
            pagerCustom: '#bx-pager'
        });
      
        jQuery("#single").fancybox();
        jQuery("#single-1").fancybox();
        
        jQuery( "#tabs" ).tabs();
       
        jQuery('#housetab_3 .btn_enquire').on('click',function(){
            jQuery('#single-1').trigger('click');
        });
  
    var geocoder;
    var map;
    var map2;
    var loc;
    var latlng
    jQuery(function(){
        geocoder = new google.maps.Geocoder();
        latlng = new google.maps.LatLng(51.198427,1.082313);
        var mapOptions = {
            zoom: 14,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById('mapviews'), mapOptions);
        map2 = new google.maps.Map(document.getElementById('mapviewsxs'), mapOptions);
        var mapFirstClick = false;
        $("#maptab").click(function() {
            mapFirstClick || setTimeout(function() {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(loc);
                mapFirstClick = true;
            }, 250);
        });

    })
    google.maps.event.addListener(map, 'zoom_changed', function() {
        zoomLevel = map.getZoom();
        //this is where you will do your icon height and width change.     
      });
    function addmap(adddress,info)
    {

        //var address = document.getElementById('address').value;
        geocoder.geocode( { 'address': adddress}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                google.maps.event.trigger(map, 'resize');
                google.maps.event.trigger(map2, 'resize');
                loc=results[0].geometry.location;
                map.setCenter(results[0].geometry.location);
                map2.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
                var marker2 = new google.maps.Marker({
                    map: map2,
                    position: results[0].geometry.location
                });

                var infowindow = new google.maps.InfoWindow({});
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.setContent(info);
                    infowindow.open(map, this);
                });
                google.maps.event.addListener(marker2, 'click', function() {
                    infowindow.setContent(info);
                    infowindow.open(map2, this);
                });
            } else {
                console.log('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

</script>

<script type="text/javascript">
    jQuery("#enquiry-form").live('submit',function()
    {
        var str=jQuery("#enquiry-form").serialize();//alert(str);
        jQuery.post("<?php bloginfo('template_url'); ?>/doenquiry.php",str ,function(data)
        {
            if(data=='success') //if correct login detail
            {//alert('hi');
                jQuery("#msg").fadeTo(200,0.1,function()  //start fading the messagebox
                {
                    //add message and change the class of the box and start fading
                    jQuery(this).html('Thank you.').fadeTo(1200,1,
                        function()
                        {

                            jQuery.fancybox.close();
                        });
                });
            }
            else
            {
                jQuery("#msg").fadeTo(200,0.1,function() //start fading the messagebox
                {
                    //add message and change the class of the box and start fading
                    jQuery(this).html(data).fadeTo(1200,1);
                });
            }
            //$('#log').html(data);
        });
        return false; //not to post the  form physically
    });
</script>
<?php 
    $prop_type = get_terms( 'type');
    if($prop_type[0]) { 
        $prop_holder = $prop_type[0]->slug;
    } 
    if($prop_type[1]) {
        $prop_holder = $prop_holder.'/'.$prop_type[1]->slug; 
    }
    $title_string = $_room_count.'bedroom'.$prop_holder.'House,'.$_postal_code;


?>