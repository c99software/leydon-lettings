<?php
@session_start();
$surl=isset($_GET['s'])?$_SERVER['REQUEST_URI'].'&':$_SERVER['REQUEST_URI'].'?';
if(isset($_GET['table']))
{
	$br='table';
	//$_SESSION['view']='table';
}
if(isset($_GET['map']))
{
	$br='map';
	//$_SESSION['view']='map';
}
if(isset($_GET['results']))
{
	$br='results';
	//$_SESSION['view']='results';
}
if(isset($_GET['locations']))
{
	$br1='&locations='.$_GET['locations'];
}
$surl=str_replace('sort=price','',$surl);
$surl=str_replace('sort=beds','',$surl);
$surl=str_replace('&sort=price','',$surl);
$surl=str_replace('&sort=beds','',$surl);
$surl=str_replace('&dir=low','',$surl);
$surl=str_replace('&dir=high','',$surl);
$surl=str_replace('&dir=all','',$surl);
$surl=str_replace('dir=all','',$surl);
$surl=str_replace('dir=high','',$surl);
$surl=str_replace('dir=low','',$surl);
$surl=str_replace('?map','',$surl);
$surl=str_replace('?table','',$surl);
$surl=str_replace('?results','',$surl);
$surl=str_replace('&map','',$surl);
$surl=str_replace('&table','',$surl);
$surl=str_replace('&results','',$surl);
$surl=str_replace('&locations=_kent','',$surl);
$surl=str_replace('&locations=_canterbury','',$surl);
$surl=str_replace('&locations=_ucl','',$surl);
$surl=str_replace('?locations=_kent','',$surl);
$surl=str_replace('?locations=_canterbury','',$surl);
$surl=str_replace('?locations=_ucl','',$surl);
$term =	$wp_query->queried_object;
?>

<form action="/" method="get" id="desearch">
<input type="hidden" name="s" value="housing" />
<input type="hidden" name="cats" value="<?php echo $term->slug; ?>" />
<div class="cler">
<div id="pricelnk">
 
                <p><button <?php echo ($_GET['disability']=='yes')?'class="view6-3 active" value="0"':'class="view6-3" value="yes"' ?>    type="submit" name="disability" ></button>Disability Friendly</p>
                <p><button  <?php echo ($_GET['ensuites']=='yes')?'class=" view6-3 active" value="0"':'class="view6-3 " value="yes"' ?>   type="submit"   name="ensuites"   ></button>Ensuites</p>
                <p><button  <?php echo ($_GET['available']=='yes')?'class="view6-3 active" value="0"':'class="view6-3" value="yes"' ?>    name="available" type="submit"  ></button>Available This Year</p>
 <p id="pricelnk1">
<a id="sdf" class="mon" href="#" onclick="dopricechg()">Price Monthly</a></p></div>
</div>
<!--<input type="hidden" name="<?php echo $br ?>" value="1" />-->
<div class="viewhead">
                <div class="viewheaddiv1">
                <div class="view1">View as:</div>
               
                <button  class="view2" title="Table"  <?php if ( !is_home() ) {   ?>type="submit"  <?php } else { ?>type="button"  <?php }?>  name="table"></button>
                <button  class="view3" title="Pictures" <?php if ( !is_home() ) {   ?>type="submit"  <?php } else { ?>type="button"  <?php }?>  name="results"></button>
                <button  class="view4" title="Map"  <?php if ( !is_home() ) {   ?>type="submit"  <?php } else { ?>type="button"  <?php }?>  name="map"></button>
               
                </div><!--viewheaddiv1-->
                <div class="viewheaddiv2">
                <!-- <div  style="width:153px;" class="viewnew">
                <select id="ptypeid" onchange="dopricechg()" style="font-size: 12px;height: 30px;width:152px;padding: 5px;float:left;"  name="pricetype">
  <option value="0">Price Monthly</option>
  <option value="1">Price Weekly</option>
  	 
   	
</select>
               
               
                </div>--><!--viewnew-->
                <div class="viewnew1" style="width:173px;">
                <div id="tab1-div">
                <div id="tab1-up">▲</div>
                <div id="tab1-down">▼</div>
                <select <?php
if ( !is_home() ) {
   ?>onchange="dosearch()"  <?php
}
?>  name="locations">
  <option value="0">Which university/location?</option>
  <option <?php if($_GET['locations']=='_kent') { echo 'selected="selected"'; } ?> value="_kent">UKC</option>
  <option <?php if($_GET['locations']=='_ucl') { echo 'selected="selected"'; } ?> value="_ucl">CCCU</option>
  
  <option <?php if($_GET['locations']=='_uca') { echo 'selected="selected"'; } ?> value="_uca">UCA</option>
  <option <?php if($_GET['locations']=='_canterbury') { echo 'selected="selected"'; } ?> value="_canterbury">Canterbury Centre</option>
</select></div></div>
                <div  style="width:183px;" class="viewnew">
                 <div id="tab2-div">
                <div id="tab1-up">▲</div>
                <div id="tab1-down">▼</div>
                 <select <?php
if ( !is_home() ) {
   ?>onchange="dosearch()"  <?php
}
?> style="font-size: 12px;height: 30px;width: 152;padding: 5px;float:left;"  name="minbeds">
  <option value="0">How Many Bedrooms?</option>
  	 <option value="all" <?php if($_GET['minbeds']=='all') { echo 'selected="selected"'; } ?>>Show All</option>
   	<option value="1" <?php if($_GET['minbeds']=='1') { echo 'selected="selected"'; } ?>>1 Bedroom</option>
      <option value="2" <?php if($_GET['minbeds']=='2') { echo 'selected="selected"'; } ?>>2 Bedrooms</option>
      <option value="3" <?php if($_GET['minbeds']=='3') { echo 'selected="selected"'; } ?>>3 Bedrooms</option>
      <option value="4" <?php if($_GET['minbeds']=='4') { echo 'selected="selected"'; } ?>>4 Bedrooms</option>
      <option value="5" <?php if($_GET['minbeds']=='5') { echo 'selected="selected"'; } ?>>5 Bedrooms</option>
      <option value="6" <?php if($_GET['minbeds']=='6') { echo 'selected="selected"'; } ?>>6 Bedrooms</option>
</select></div>
                <a   class="view5" href="#"></a>
               
                </div><!--viewnew-->
                <div id="blabal" style="display:none;"></div>
                <div class="view6">
                
               
    <button <?php echo ($_GET['dir']=='low')?'class="active"':'' ?>  id="view6-2"    
		<?php if ( !is_home() ) {   ?> type="submit"  <?php } else { ?> type="button" onclick="jQuery('.view6 button').removeClass('active');jQuery(this).addClass('active');jQuery('#blabal').html('<input type=text name=dir value=low>');"  <?php }?> name="dir" value="low"></button>
                <p>Price Low</p>
                 <button <?php echo ($_GET['dir']=='high')?'class="active"':'' ?>  id="view6-3"  	<?php if ( !is_home() ) {   ?>type="submit"  <?php } else { ?>type="button"  onclick="jQuery('.view6 button').removeClass('active');jQuery(this).addClass('active');jQuery('#blabal').html('<input type=text name=dir value=high>');"  <?php }?>  name="dir" value="high"></button>
              
                
                <p>Price High</p>
                
                </div><!--view6-->
                <?php
if ( is_home() ) {
   ?>
    <button type="submit" class="view1" style=" line-height:1">Search</button>
    <?php
}
?>
               
           
                <?php /*?><div class="view7"><a <?php echo ($_GET['dir']=='all')?'class="active"':'' ?> href="<?php echo $surl ?>sort=<?php echo $_GET['sort']=='price'?'price':'beds' ?>&dir=all<?php echo $br; ?><?php echo $br1; ?>" id="view7-1"></a>All</div><?php */?>
                </div><!--viewheaddiv2-->
                
                </div>
                </form>
                <script type="text/javascript">
                function setLocation(val)
				{
					if(val!=0){
					document.location='<?php echo $surl?>sort=<?php echo $_GET['sort']=='price'?'price':'beds' ?>&dir=<?php echo $_GET['dir']=='high'?'high':'low' ?><?php echo $br?>&locations='+val;
					}
				}
				$( "#pricelnk a" ).click(function( event ) {
  event.preventDefault();
				}
				);
				function dosearch()
				{
					
					jQuery('#desearch').submit();
				}
				function dopricechg()
				{
					var ptype=jQuery('#ptypeid option:selected').val();
					var className = $('#sdf').attr('class');
					
					
					
								
							jQuery(".rmvalue" ).each(function(index, element  ) {
								var val1=jQuery(this).html();
								val1=val1.replace("£","");
								
								val1=val1*4.348;
								val1=val1.toFixed(0);
								val1=val1.replace(val1,"£"+val1);
								jQuery(this).html(val1);
								
								
								})
								jQuery(".priceval" ).each(function(index, element  ) {
								var val2=jQuery(this).html();
								val2=val2.replace("£","");
								val2=val2.replace("pcm","");
								val2=val2*4.348;
								val2=val2.toFixed(0);
								val2=val2.replace(val2,"£"+val2);
								jQuery(this).html(val2);
								
								
								})
								document.getElementById('pricelnk1').innerHTML='<a id="sdf" class="mon" href="#" onclick="dopricechg2()">Price Weekly</a>';
				}
				function dopricechg2()
				{
							jQuery(".rmvalue" ).each(function(index, element  ) {
								var val1=jQuery(this).html();
								val1=val1.replace("£","");
								val1=val1.replace("pcm","");
								val1=val1/4.348;
								val1=val1.toFixed(0);
								val1=val1.replace(val1,"£"+val1);
								jQuery(this).html(val1);
								
								
								})
								jQuery(".priceval" ).each(function(index, element  ) {
								var val2=jQuery(this).html();
								val2=val2.replace("£","");
								val2=val2.replace("pcm","");
								val2=val2/4.348;
								val2=val2.toFixed(0);
								val2=val2.replace(val2,"£"+val2);
								jQuery(this).html(val2);
								
								
								})
								document.getElementById('pricelnk1').innerHTML='<a id="sdf" class="mon" href="#" onclick="dopricechg()">Price Monthly</a>';
						}
				
                </script>