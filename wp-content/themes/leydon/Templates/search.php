

<?php

/*

Template Name: Template: Search

*/

get_header(); ?>

<div class="container page_style">
	<div class="row">
	<div id="search_message" class="col-xs-12">
	</div>
	<div class="col-xs-12"><div class="intro " style="display:none;"><?php echo the_content(); ?><a href="javascript:void(0);" class="close">X</a></div></div>
	
	<?php echo do_shortcode('[house_search]'); ?>

	</div>	
</div>	
<script>
jQuery('#grid_results').on('click','.more-info',function(){
	console.log('test');
    jQuery(this).closest('.house').addClass('hover');
});
jQuery('#grid_results').on('click','.less-info',function(){
	console.log('test');
    jQuery(this).closest('.house').removeClass('hover');
});
</script>




<?php get_footer(); ?>