<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Thirteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
@session_start();
get_header(); ?>

<script type="text/javascript">
var geocoder;
var map;
</script>
<?php
$term =	$wp_query->queried_object;
 $querystr = "
	SELECT * FROM $wpdb->posts
	LEFT JOIN $wpdb->postmeta ON($wpdb->posts.ID = $wpdb->postmeta.post_id)
	LEFT JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)
	LEFT JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
	LEFT JOIN $wpdb->terms ON($wpdb->term_taxonomy.term_id = $wpdb->terms.term_id)
	WHERE  $wpdb->terms.slug = '".$term->slug."'
	AND $wpdb->term_taxonomy.taxonomy = 'location'
	AND $wpdb->posts.post_status = 'publish'
	AND $wpdb->posts.post_type = 'housing'
";
 
 $querystr .= " 	GROUP BY $wpdb->posts.ID
	";
 	$querystr .= " ORDER BY $wpdb->posts.post_date DESC";
 $pageposts = $wpdb->get_results($querystr, ARRAY_A);

 ?>
 <?php foreach ($pageposts as $key=>$post): 
 $rooms=get_post_meta($post['ID'], '_rooms', true);
global $wpdb;
global $post;
$rentslowquery = "
    SELECT DISTINCT(CAST(meta_value AS UNSIGNED)) AS met 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '%_rent_%' AND post_id='".$post['ID']."'
	ORDER BY met ASC
	LIMIT 1
 ";
$rentslow = $wpdb->get_results($rentslowquery, ARRAY_N);
$rentshighquery = "
    SELECT DISTINCT(CAST(meta_value AS UNSIGNED)) AS met 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '%_rent_%' AND post_id='".$post['ID']."'
	ORDER BY met DESC
	LIMIT 1
 ";
$rentshigh = $wpdb->get_results($rentshighquery, ARRAY_N);
$availabilitythisquery = "
    SELECT count(*) 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '_availability_this_year_%' AND meta_value = 'Yes' AND post_id='".$post['ID']."'
 ";
$availabilitythis = $wpdb->get_results($availabilitythisquery, ARRAY_N);
$pageposts[$key]['rooms']=$rooms;
$pageposts[$key]['pricelow']=$rentslow[0][0];
$pageposts[$key]['pricehigh']=$rentshigh[0][0];
$pageposts[$key]['avail']=$availabilitythis[0][0];
  ?>
 <?php endforeach ?>
 <?php
 
 if($_GET['sort']=='price')
		{
			if($_GET['dir']=='low')
			{
				
				$pageposts=subval_sort($pageposts,'pricelow','asc'); 
			//	$querystr .= " ORDER BY  `wp_postmeta`.`meta_value` ASC ";
			}
			else if($_GET['dir']=='all')
			{
				//subval_sort($a,$subkey,$sr); 
			}
			else
			{
				$pageposts=subval_sort($pageposts,'pricelow','desc'); 
			}
		}
	else if($_GET['sort']=='beds')
		{
			if($_GET['dir']=='low')
			{
				$pageposts=subval_sort($pageposts,'rooms','asc'); 
			}
			else if($_GET['dir']=='all')
			{
				//subval_sort($a,$subkey,$sr); 
			}
			else
			{
				$pageposts=subval_sort($pageposts,'rooms','desc'); 
			}
		}
		else if($_GET['sort']=='avail')
		{
			
				$pageposts=subval_sort($pageposts,'avail','desc'); 
			
		}
 
 
// print_r($pageposts);
 ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
				<div class="container">
                <div class="innerpage" id="fullsize">
                <p class="innerpagehead">Leydon Lettings <?php echo $term->name ?></p>
                  <?php echo get_sidebar( 'search-bar' ); ?>              <!--viewhead-->
                
					<?php if ( $pageposts) : ?>
                    
                    <?php if(isset($_GET['table']) ){
						
						 ?>
                      <div class="list">
                        <table border="0px;" class="downloadtable">
                        <thead>
                        <tr>
                        <th width="162" colspan="2"  align="center" >Available This Year</th>
                        <th width="175" >House Address</th>
                        <th width="77">Postcode</th>
                        <th width="49">Rooms</th>
                       
                       
                        <th width="10" ></th>
                        
                    <?php /*?>    <th width="91">Price Range</th><?php */?>
                        <th width="85">Distance</th>
                        <th width="167"></th>
                          <th width="152" colspan="2" align="center" >Available Next Year</th>
                        </tr>
                        </thead>
                        <tbody>
                         <?php 
						 $x=0;
						 if ( $pageposts )
						{
						 foreach ($pageposts as $po): ?>
                           
                           <?php 
						   $postquery = "SELECT $wpdb->posts.*,$wpdb->postmeta.* FROM $wpdb->posts, $wpdb->postmeta     WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id AND $wpdb->posts.ID = '".$po['ID']."' AND $wpdb->posts.post_status = 'publish' AND $wpdb->posts.post_type = 'housing'";
$postquery .= "GROUP BY $wpdb->posts.ID";
$postquery .= " ORDER BY $wpdb->posts.post_date DESC";
$pq = $wpdb->get_results($postquery, OBJECT);	
foreach ($pq as $post):	
			   ?>
                     <?php setup_postdata($post); ?>
                         
                        	 <?php get_template_part( 'content', 'housing-list' ); ?>
                           <?php 
						   $x++;
						   endforeach; ?>  
                            <?php endforeach;
							
						}
						else
						{
							?>
                            <tr> <td colspan="8">Sorry there are no properties matching your search. Please adjust your criteria and search again.</td></tr>
                            <?php
						}?>   
                        </tbody>
                </table>
                </div>
                        <?php 
						
					} 
					else if(isset($_GET['map']) )
					{
						
						?>
                        <script>
						  // ======= Global variable to remind us what to do next
      var nextAddress = 0;
var geocoder;
var map;
var addresses=[];
var bounds;
var latlng
jQuery(function(){
  geocoder = new google.maps.Geocoder();
  latlng = new google.maps.LatLng(51.280277,1.079282);
  var mapOptions = {
    zoom: 14,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  map = new google.maps.Map(document.getElementById('mapview'), mapOptions);
    bounds = new google.maps.LatLngBounds();
	 theNext();
	 
	 
})


    // delay between geocode requests - at the time of writing, 100 miliseconds seems to work well
    var delay = 100;


      // ====== Create map objects ======
      var infowindow = new google.maps.InfoWindow();
     
    //  var geo = new google.maps.Geocoder(); 
    //  var map = new google.maps.Map(document.getElementById("mapview"), mapOptions);
     // var bounds = new google.maps.LatLngBounds();

      // ====== Geocoding ======
      function getAddress(searchi, next) {
		   nextAddress++;
        geocoder.geocode({address:searchi[0]}, function (results,status)
          { 
            // If that was successful
            if (status == google.maps.GeocoderStatus.OK) {
              // Lets assume that the first marker is the one we want
              var p = results[0].geometry.location;
              var lat=p.lat();
              var lng=p.lng();
              // Output the data
                var msg = 'address="' + searchi[0]+ '" lat=' +lat+ ' lng=' +lng+ '(delay='+delay+'ms)<br>';
            //   console.log(msg)
              // Create a mamsgrker
              createMarker(searchi[0],lat,lng,searchi[1],searchi[2]);
			
            }
            // ====== Decode the error status ======
            else {
              // === if we were sending the requests to fast, try this one again and increase the delay
              if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
				 var reason="Code "+status;
                var msg = 'address="' + searchi[0] + '" error=' +reason+ '(delay='+delay+'ms)<br>';
				//console.log(msg)
                nextAddress--;
                delay++;
              } else {
                var reason="Code "+status;
                var msg = 'address="' + searchi[0] + '" error=' +reason+ '(delay='+delay+'ms)<br>';
				//console.log(msg)
               // document.getElementById("messages").innerHTML += msg;
              }   
            }
            next();
          }
        );
      }

     // ======= Function to create a marker
     function createMarker(add,lat,lng,infoss,image) {
       var contentString = infoss;
       var marker = new google.maps.Marker({
         position: new google.maps.LatLng(lat,lng),
         map: map,
		  icon: image,
         zIndex: Math.round(latlng.lat()*-100000)<<5
       });

      google.maps.event.addListener(marker, 'click', function() {
         infowindow.setContent(contentString); 
         infowindow.open(map,marker);
       });

       bounds.extend(marker.position);

     }

     
    

      // ======= Function to call the next Geocode operation when the reply comes back

      function theNext() {
		 console.log(nextAddress);
        if (nextAddress < addresses.length) {
			
          setTimeout(getAddress(addresses[nextAddress],theNext), delay);
		    
         
		 // console.log(nextAddress)
        } else {
          // We're done. Show map bounds
       //   map.fitBounds(bounds);
        }
      }
	//	setTimeout(function() { theNext(); }, 1000);
      // ======= Call that function for the first time =======
     

    // This Javascript is based on code provided by the
    // Community Church Javascript Team
    // http://www.bisphamchurch.org.uk/   
    // http://econym.org.uk/gmap/

    //]]>

    </script>
                        <div class="list map" id="mapview">
                         </div>  
                         <?php foreach ($pageposts as $po): ?>
                           
                           <?php 
						   $postquery = "SELECT $wpdb->posts.*,$wpdb->postmeta.* FROM $wpdb->posts, $wpdb->postmeta     WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id AND $wpdb->posts.ID = '".$po['ID']."' AND $wpdb->posts.post_status = 'publish' AND $wpdb->posts.post_type = 'housing'";
$postquery .= "GROUP BY $wpdb->posts.ID";
$postquery .= " ORDER BY $wpdb->posts.post_date DESC";
$pq = $wpdb->get_results($postquery, OBJECT);	
foreach ($pq as $post):	


			   ?>
                     <?php setup_postdata($post); ?>
                         	 <?php get_template_part( 'content', 'housing-map' ); ?>   
                        <?php endforeach; ?>   
                         <?php endforeach; ?>                               
                        
                        <?php
					
					}
					 else {						
						
						 ?>
                    <div class="grid">
						<ul>
                           <?php foreach ($pageposts as $po): ?>
                           
                           <?php 
						   $postquery = "SELECT $wpdb->posts.*,$wpdb->postmeta.* FROM $wpdb->posts, $wpdb->postmeta     WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id AND $wpdb->posts.ID = '".$po['ID']."' AND $wpdb->posts.post_status = 'publish' AND $wpdb->posts.post_type = 'housing'";
$postquery .= "GROUP BY $wpdb->posts.ID";
$postquery .= " ORDER BY $wpdb->posts.post_date DESC";
$pq = $wpdb->get_results($postquery, OBJECT);	
foreach ($pq as $post):	
			   
						    ?>
                           
                     <?php setup_postdata($post); ?>
                         
                        	  <?php get_template_part( 'content', 'housing-grid' ); ?>
                           <?php endforeach;
						   
						   endforeach; ?>    
						</ul>
						</div>
                    <?php 
					} ?>
                        
            
                    <?php else : ?>
                        <?php get_template_part( 'content', 'none' ); ?>
                    <?php endif; ?>


</div><!--innerpage-->
</div>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>