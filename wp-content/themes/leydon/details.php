<?php
/*
 Template Name: Details
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
		<div class="container">
<div class="innerpage" id="fullsize">
<p class="innerpagehead">Student Houses To Let - Results</p>
<div class="viewhead">
<div class="viewheaddiv1">
<div class="view1">View as:</div>
<a class="view2" href="#"></a>
<a class="view3" href="#"></a>
<a class="view4" href="#"></a>
</div><!--viewheaddiv1-->
<div class="viewheaddiv2">
<a class="view5" href="#"><p>Bedrooms</p></a>
<div class="view6">
<a id="view6-1" href="#"></a><p>Price &nbsp;&nbsp; Low</p>
<a id="view6-2" href="#"></a>
<a id="view6-3" href="#"></a><p>High</p>
</div><!--view6-->
<div class="view7" href="#"><a href="#" id="view7-1"></a>All</div>
</div><!--viewheaddiv2-->

</div><!--viewhead-->
<div class="grid">
<ul>
<li>
<img src="images/grid-img.jpg" />
<div class="gridcontent">
<p id="gridhead">£650pcm</p>
<p id="gridsubhead">4 bedroom House<br />
The Street, The Town, CT1 1BT
</p>
<p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla musiol dignissim neque, nulla neque. Ultrices proin mi urna lorem iopsum ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit!
</p>
<div class="gridbtn">
<a href="#" class="gridb1">View full detals</a>
<a href="#" class="gridb2">Contact us</a>
<a href="#" class="gridb3">view similar properties</a>
</div><!--gridbtn-->
</div><!--gridcontent-->
</li>
<li>
<img src="images/grid-img.jpg" />
<div class="gridcontent">
<p id="gridhead">£650pcm</p>
<p id="gridsubhead">4 bedroom House<br />
The Street, The Town, CT1 1BT
</p>
<p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla musiol dignissim neque, nulla neque. Ultrices proin mi urna lorem iopsum ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit!
</p>
<div class="gridbtn">
<a href="#" class="gridb1">View full detals</a>
<a href="#" class="gridb2">Contact us</a>
<a href="#" class="gridb3">view similar properties</a>
</div><!--gridbtn-->
</div><!--gridcontent-->
</li>
<li>
<img src="images/grid-img.jpg" />
<div class="gridcontent">
<p id="gridhead">£650pcm</p>
<p id="gridsubhead">4 bedroom House<br />
The Street, The Town, CT1 1BT
</p>
<p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla musiol dignissim neque, nulla neque. Ultrices proin mi urna lorem iopsum ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit!
</p>
<div class="gridbtn">
<a href="#" class="gridb1">View full detals</a>
<a href="#" class="gridb2">Contact us</a>
<a href="#" class="gridb3">view similar properties</a>
</div><!--gridbtn-->
</div><!--gridcontent-->
</li>
<li>
<img src="images/grid-img.jpg" />
<div class="gridcontent">
<p id="gridhead">£650pcm</p>
<p id="gridsubhead">4 bedroom House<br />
The Street, The Town, CT1 1BT
</p>
<p>Lorem ipsum dolor sit amet, mauris suspendisse viverra eleifend tortor tellus suscipit, tortor aliquet at nulla musiol dignissim neque, nulla neque. Ultrices proin mi urna lorem iopsum ut, aenean sollicitudin etiam libero nisl, ultrices ridiculus in magna purus consequuntur, ipsum donec orci ad vitae pede, id odio. Turpis venenatis at laoreet. Etiam commodo fusce in diam feugiat, nullam suscipit tortor per velit!
</p>
<div class="gridbtn">
<a href="#" class="gridb1">View full detals</a>
<a href="#" class="gridb2">Contact us</a>
<a href="#" class="gridb3">view similar properties</a>
</div><!--gridbtn-->
</div><!--gridcontent-->
</li>

</ul>
</div><!--list-->
<div class="pagecontrol">
<div class="pageno">

<ul>
<li><a href="#">1</a></li>
<li><a href="#">2</a></li>
<li><a href="#">3</a></li>
<li><a href="#">4</a></li>
<li><a href="#">5</a></li>
<li><a href="#">6</a></li>
<li><a href="#">7</a></li>
</ul>
</div><!--pageno-->
<a href="#" class="ppage">Prev page</a>
<a href="#" class="npage">Next page</a>


</div><!--pagecontrol-->


</div><!--innerpage-->
</div>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>