<?php
function housing_enqueue($hook) {
   wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false');
   wp_register_script('map_script', get_bloginfo('template_directory') . '/housing/scripts/map.js', array('jquery') );
   wp_enqueue_script('map_script');
   wp_register_script('common_script', get_bloginfo('template_directory') . '/housing/scripts/common.js', array('jquery') );
   wp_enqueue_script('common_script');
   wp_register_style( 'housing_wp_admin_css', get_bloginfo('template_directory') . '/housing/scripts/admin.css', false, '1.0.0' );
   wp_enqueue_style( 'housing_wp_admin_css' );
}
function housing_enqueue_front($hook) {
   wp_enqueue_script('google-maps', 'http://maps.google.com/maps/api/js?sensor=false');
   wp_register_script('map_script', get_bloginfo('template_directory') . '/housing/scripts/map.js', array('jquery') );
   wp_enqueue_script('map_script');
   if (is_admin() ) {
    

   wp_register_script('common_script', get_bloginfo('template_directory') . '/housing/scripts/common.js', array('jquery') );
   wp_enqueue_script('common_script');
   }
}
add_action( 'admin_enqueue_scripts', 'housing_enqueue' );
add_action( 'wp_enqueue_scripts', 'housing_enqueue_front' );
add_action( 'init', 'wp_housing' );
function wp_housing() {
	register_post_type( 'housing',
		array(
			'labels' => array(
				'name' => __( 'Housing','language' ),
				'singular_name' => __( 'Housing' ,'language'),
				'add_new' => __( 'Add New Housing' ,'language'),
				'add_new_item' => __( 'Add New Housing' ,'language'),
				'add_new_item' => __( 'Add New Housing' ,'language'),
				'edit_item' => __( 'Edit Housing' ,'language'),
				'new_item' => __( 'Add New Housing','language' ),
				'view_item' => __( 'View Housing','language' ),
				'search_items' => __( 'Search Housing' ,'language'),
				'not_found' => __( 'No Housing found','language' ),
				'not_found_in_trash' => __( 'No Housing found in trash','language' )
			),
			'public' => true,
			'supports' => array( 'title','editor','thumbnail','category' ),
			'capability_type' => 'post',
			'has_archive' => true,
			'rewrite' => array('slug' => 'housing'),
			'menu_position' => '3',
			'taxonomies' => array('type','location')
		)
	);	
}
add_theme_support( 'post-thumbnails', array( 'housing' ) );
add_action('init' , 'add_housing_type' );
function add_housing_type()
  {
     $labels = array(
    'name' => _x( 'Property Types', 'taxonomy general name' ),
    'singular_name' => _x( 'Property Type', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Property Type' ),
    'all_items' => __( 'All Property Types' ),
    'parent_item' => __( 'Property Types' ),
    'parent_item_colon' => __( 'Property Type:' ),
    'edit_item' => __( 'Edit Property Type' ), 
    'update_item' => __( 'Update Property Type' ),
    'add_new_item' => __( 'Add New Property Type' ),
    'new_item_name' => __( 'New Property Type' ),
  );    

  register_taxonomy('type',array('housing'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'show_in_nav_menus' => true,
    'rewrite' => array( 'slug' => 'property-type' )
  ));   

}
add_action('init' , 'add_housing_location' );
function add_housing_location()
  {
     $labels = array(
    'name' => _x( 'Locations', 'taxonomy general name' ),
    'singular_name' => _x( 'Locations', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Locations' ),
    'all_items' => __( 'All Locations' ),
    'parent_item' => __( 'Locations' ),
    'parent_item_colon' => __( 'Locations:' ),
    'edit_item' => __( 'Edit Location' ), 
    'update_item' => __( 'Update Location' ),
    'add_new_item' => __( 'Add New Location' ),
    'new_item_name' => __( 'New Location' ),
  );    

  register_taxonomy('location',array('housing'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'query_var' => true,
    'show_in_nav_menus' => true,
    'rewrite' => array( 'slug' => 'location' )
  ));   
}
add_action( 'add_meta_boxes', 'add_housing_details_metaboxes' );
function add_housing_details_metaboxes() {
    add_meta_box('housing_details_metabox', 'Overview', 'housing_overview', 'housing', 'normal', 'high');
	add_meta_box('housing_main_details_metabox', 'Main Details', 'housing_details', 'housing', 'normal', 'high');
	add_meta_box('housing_room_details_metabox', 'Room Details', 'room_details', 'housing', 'normal', 'high');
	add_meta_box('housing_room_address_metabox', 'Address & Map', 'room_address', 'housing', 'normal', 'high');
	add_meta_box('housing_gallery_metabox', 'Gallery', 'housing_gallery', 'housing', 'normal', 'high');
	add_meta_box('housing_certificates_metabox', 'Certificates', 'housing_certificates', 'housing', 'normal', 'high');
	add_meta_box('housing_floorplans_metabox', 'Floorplans', 'housing_floorplans', 'housing', 'normal', 'high');
}
function housing_overview() {
    global $post;
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
	$_contracttypearray=array('Silver Contract','Gold Contract');
	$_featuredarray=array('Yes','No');
	$_varray=array('Monday 12 till 2','Monday 3 till 5','Tuesday 12 till 2','Tuesday 3 till 5','Wednesday 12 till 2','Wednesday 3 till 5','Thursday 12 till 2','Thursday 3 till 5','Friday 12 till 2','Friday 3 till 5');
	$_depositarray=array('Not Required','Required');
	$_billsarray=array('Not Included','Included');
	$_roomsarray=array('0','1','2','3','4','5','6','7','8','9','10');
	$_maxstuarray=array('0','1','2','3','4','5','6','7','8','9','10');
	$fields=array(
	array('label'=>'House Number','type'=>'text','name'=>'_housenumber'),	
	array('label'=>'UK Guarantor','type'=>'text','name'=>'_guarantor','value'=>'Required'),
	array('label'=>'Deposit','type'=>'select','name'=>'_deposit','options'=>$_depositarray),
	array('label'=>'Rent inclusive of bills','type'=>'select','name'=>'_billsinc','options'=>$_billsarray),
	array('label'=>'Non-refund admin fee','type'=>'text','name'=>'_nonrefundfee'),
	array('label'=>'Rooms','type'=>'select','name'=>'_rooms','options'=>$_roomsarray),
	array('label'=>'Max Students','type'=>'select','name'=>'_maxstudents','options'=>$_maxstuarray),
	array('label'=>'Contract Type','type'=>'select','name'=>'_contracttype','options'=>$_contracttypearray),
	array('label'=>'Weekly price.','type'=>'text','name'=>'_pcm'),
	array('label'=>'Wheelchair Friendly','type'=>'select','name'=>'_Wheelchair','options'=>$_featuredarray),
	array('label'=>'Featured','type'=>'select','name'=>'_featured','options'=>$_featuredarray),
	array('label'=>'Viewing Time1','type'=>'select','name'=>'_vtime1','options'=>$_varray),
	array('label'=>'Viewing Time2','type'=>'select','name'=>'_vtime2','options'=>$_varray),
	);
	echo '<h4 class="title  htitle">Housing Details</h4>';
    echo '<div class="hinfo allinfo">';	
	print_extra_fields($fields,$post->ID);
	echo '</div>';

	
}
function housing_details() {
    global $post;
	$_yesno=array('Yes','No');
	$_powerarray=array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14');
	$_noarray6=array('0','1','2','3','4','5','6');
	$_noarray10=array('0','1','2','3','4','5','6','7','8','9','10');
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
	$fields=array(
	array('label'=>'Power Sockets - (per room)','type'=>'select','name'=>'_power_sockets','options'=>$_powerarray),
	array('label'=>'TV License','type'=>'select','name'=>'_tv_license','options'=>$_yesno),
	array('label'=>'Outhouse (WC)','type'=>'select','name'=>'_outhouse_wc','options'=>$_yesno),
	array('label'=>'Outhouse (Bike Storage)','type'=>'select','name'=>'_outhouse_bike','options'=>$_yesno),
	array('label'=>'Utility Room','type'=>'select','name'=>'_utility_room','options'=>$_yesno),
	array('label'=>'En Suites','type'=>'select','name'=>'_en_suite','options'=>$_noarray6),
	array('label'=>'Shared Toilet','type'=>'select','name'=>'_shared_toilet','options'=>$_noarray6),
	array('label'=>'Shared Sink','type'=>'select','name'=>'_shared_sink','options'=>$_noarray6),
	array('label'=>'Shared Bath','type'=>'select','name'=>'_shared_bath','options'=>$_noarray6),
	array('label'=>'Shared Shower','type'=>'select','name'=>'_shared_shower','options'=>$_noarray6),
	array('label'=>'Boiler Type','type'=>'text','name'=>'_boiler_type'),
	array('label'=>'Cavity Insulation - Wall','type'=>'select','name'=>'cavity_insulation_wall','options'=>$_yesno),
	array('label'=>'Cavity Insulation - Loft','type'=>'select','name'=>'cavity_insulation_loft','options'=>$_yesno),
    array('label'=>'Driveway (Max Spaces)','type'=>'select','name'=>'_driveway','options'=>$_noarray10),
	array('label'=>'<b>Parking (Permit - available from CCC)</b>','type'=>'select','name'=>'_parkingpermit','options'=>$_yesno),
	array('label'=>'Parking (On Road)','type'=>'select','name'=>'_parking_road','options'=>$_yesno),
	array('label'=>'Parking (Road Bays)','type'=>'select','name'=>'_parking_bay','options'=>$_yesno),
	array('label'=>'Garden Shed','type'=>'select','name'=>'_garden_shed','options'=>$_yesno),
	array('label'=>'Front Garden','type'=>'select','name'=>'_front_garden','options'=>$_yesno),
	array('label'=>'Rear Garden','type'=>'select','name'=>'_rear_garden','options'=>$_yesno),
	array('label'=>'Bus Routes','type'=>'textarea','name'=>'_bus_routes'),
	);
	echo '<h4 class="title htitle">House Info</h4>';
	echo '<div class="hinfo allinfo">';	
	print_extra_fields($fields,$post->ID);
	echo '</div>';
	$_singdouble=array('Single','Double');
	$_floortype=array('Tile', 'Carpet', 'Laminate', 'Linoleum');
	$fields2=array(
	array('label'=>'Kitchen Type','type'=>'select','name'=>'_kitchen_type','options'=>$_singdouble),
	array('label'=>'Fridge','type'=>'select','name'=>'_fridge','options'=>$_noarray6),
	array('label'=>'Freezer','type'=>'select','name'=>'_freezer','options'=>$_noarray6),
	array('label'=>'Wash Machine','type'=>'select','name'=>'_wash_machine','options'=>$_noarray6),
	array('label'=>'Dryer','type'=>'select','name'=>'_dryer','options'=>$_noarray6),
	array('label'=>'Microwave','type'=>'select','name'=>'_microwave','options'=>$_noarray6),
	array('label'=>'Hobs','type'=>'select','name'=>'_hobs','options'=>$_noarray6),
	array('label'=>'Hood Extractor Fans','type'=>'select','name'=>'_hood_extractor_fans','options'=>$_noarray6),
	array('label'=>'Washer Dryer','type'=>'select','name'=>'_washerdryer','options'=>$_noarray6),
	array('label'=>'Dishwasher','type'=>'select','name'=>'_dishwasher','options'=>$_noarray6),
	array('label'=>'Kitchen Sinks','type'=>'text','name'=>'_kitchen_sinks','options'=>$_noarray6),
    array('label'=>'Oven Type','type'=>'select','name'=>'_oven_type','options'=>$_singdouble),
	array('label'=>'Floor Type','type'=>'select','name'=>'_floor_type','options'=>$_floortype),
	);
	echo '<h4 class="title  htitle">Kitchen Info </h4>';
	echo '<div class="kinfo allinfo">';	
	print_extra_fields($fields2,$post->ID);
	echo '</div>';
	$_yesno=array('Yes','No');
	$fields3=array(
	array('label'=>'Bills package','type'=>'select','name'=>'_bills_package','options'=>$_yesno),
	array('label'=>'Telephone Line (incoming only)','type'=>'select','name'=>'_telephone_line_bill','options'=>$_yesno),
	array('label'=>'Broadband Wireless','type'=>'select','name'=>'_broadband_wireless_bill','options'=>$_yesno),
	array('label'=>'TV License','type'=>'select','name'=>'_tv_license_bill','options'=>$_yesno),
	array('label'=>'TV','type'=>'select','name'=>'_tv_bill','options'=>$_yesno),
	array('label'=>'Gas','type'=>'select','name'=>'_gas_bill','options'=>$_yesno),
	array('label'=>'Electricity','type'=>'select','name'=>'_electricity_bill','options'=>$_yesno),
	array('label'=>'TV Coaxial - to all rooms','type'=>'select','name'=>'_tv_coaxial_bill','options'=>$_yesno),
	array('label'=>'Sewerage','type'=>'select','name'=>'_sewerage_bill','options'=>$_yesno),
	array('label'=>'Water','type'=>'select','name'=>'_water_bill','options'=>$_yesno),	
	);
	
	echo '<h4 class="title  htitle">Bills Package Info</h4>';
	echo '<div class="binfo allinfo">';	
	print_extra_fields($fields3,$post->ID);
	echo '</div>';
	
	$fields4=array(
	array('label'=>'Thermostat','type'=>'textarea','name'=>'_thermostat_location'),
	array('label'=>'Hot Water and Heating Controls','type'=>'textarea','name'=>'_hot_water_location'),
	array('label'=>'Boiler','type'=>'textarea','name'=>'_boiler_location'),
	array('label'=>'Stop Cock','type'=>'textarea','name'=>'_stop_cock_location'),
	array('label'=>'Fuse Box','type'=>'textarea','name'=>'_fuse_box_location'),
	array('label'=>'Internet Box','type'=>'textarea','name'=>'_internet_box_location'),
	array('label'=>'Electric Meter','type'=>'textarea','name'=>'_electric_meter_location'),
	array('label'=>'Gas Meter','type'=>'textarea','name'=>'_gas_meter_location'),
	array('label'=>'Gas detector','type'=>'textarea','name'=>'_gas_detector_location'),
	
	
	);
	
	echo '<h4 class="title  htitle">Emergency Locations</h4>';
	echo '<div class="binfo allinfo">';	
	print_extra_fields($fields4,$post->ID);
	echo '</div>';
	
	$fields5=array(
	array('label'=>'Burglar Alarm','type'=>'select','name'=>'_balarm','options'=>$_yesno),	
	array('label'=>'Smoke Alarm','type'=>'select','name'=>'_salarm','options'=>$_yesno),	
	array('label'=>'Heat Detector','type'=>'select','name'=>'_heatdet','options'=>$_yesno),	
	array('label'=>'Carbon Monoxide Detector','type'=>'select','name'=>'_cardet','options'=>$_yesno),	
	array('label'=>'Fire Alarm Unit','type'=>'select','name'=>'_fam','options'=>$_yesno),	
	
	
	
	);
	
	echo '<h4 class="title  htitle">Security</h4>';
	echo '<div class="binfo allinfo">';	
	print_extra_fields($fields5,$post->ID);
	echo '</div>';
	
}
function room_details() {
    global $post;
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
	$_rooms=array('Small', 'Medium', 'Large', 'XLarge','Virtual Studio');
	$_bedsize=array('Single', 'Double', 'King Size');
	$_floortype=array('Tile', 'Carpet', 'Laminate', 'Linoleum');
	$_locations=array('G/F Front', 'G/F Rear', 'F/F Front', 'F/F Rear', 'S/F Front', 'S/F Rear', 'T/F Front', 'T/F Rear', 'Basement Front', 'Basement Rear', 'Front', 'Rear', 'L Side', 'R Side');
	$fields=array(
	array('label'=>'Location','type'=>'select','name'=>'_location','options'=>$_locations),
	array('label'=>'Size','type'=>'select','name'=>'_size','options'=>$_rooms),
	array('label'=>'Rent','type'=>'text','name'=>'_rent'),
	array('label'=>'Availability Table 1 : '.(get_option('at_current_year')==_availability_this_year?(date(Y).' - '.(date(Y)+1)):"").(get_option('at_next_year')==_availability_this_year?((date(Y)+1).' - '.(date(Y)+2)):""),'type'=>'radio','name'=>'_availability_this_year'),
	array('label'=>'Availability Table 2 : '.(get_option('at_current_year')==_availability_next_year?(date(Y).' - '.(date(Y)+1)):"").(get_option('at_next_year')==_availability_next_year?((date(Y)+1).' - '.(date(Y)+2)):""),'type'=>'radio','name'=>'_availability_next_year'),
    array('label'=>'Availability Table 3 : '.(get_option('at_current_year')==_availability_next_next_year?(date(Y).' - '.(date(Y)+1)):"").(get_option('at_next_year')==_availability_next_next_year?((date(Y)+1).' - '.(date(Y)+2)):""),'type'=>'radio','name'=>'_availability_next_next_year'),
	array('label'=>'Mattress Type','type'=>'text','name'=>'_mattress_type'),
	array('label'=>'Bed Size','type'=>'select','name'=>'_bed_size','options'=>$_bedsize),
	array('label'=>'Desk and Chair','type'=>'radio','name'=>'_desk_chair','options'=>array('Yes','No')),
	array('label'=>'Wardrobe','type'=>'radio','name'=>'_wardrobe','options'=>array('Yes','No')),
	array('label'=>'Wardrobe inc Chest of Drawers','type'=>'radio','name'=>'_wardrobe_inc','options'=>array('Yes','No')),
	array('label'=>'Chest of Drawers','type'=>'radio','name'=>'_chest_drawers','options'=>array('Yes','No')),
	array('label'=>'Bedside Table','type'=>'radio','name'=>'_bedside_table','options'=>array('Yes','No')),
	array('label'=>'Bedroom lock','type'=>'radio','name'=>'_mortis_door_lock','options'=>array('Yes','No')),
	array('label'=>'Sink En-suite','type'=>'radio','name'=>'_sink_ensuite','options'=>array('Yes','No')),
	array('label'=>'Toilet En-suite','type'=>'radio','name'=>'_toilet_ensuite','options'=>array('Yes','No')),
	array('label'=>'Shower Ensuite','type'=>'radio','name'=>'_shower_ensuite','options'=>array('Yes','No')),
	array('label'=>'Hard-wired Broadband','type'=>'radio','name'=>'_hard_wired_broadband','options'=>array('Yes','No')),
	array('label'=>'Wireless Broadband','type'=>'radio','name'=>'_wireless_broadband','options'=>array('Yes','No')),
   array('label'=>'Telephone Line','type'=>'radio','name'=>'_telephone_line','options'=>array('Yes','No')),
   array('label'=>'TV Aerial','type'=>'radio','name'=>'_tv_aerial','options'=>array('Yes','No')),
	array('label'=>'Floor','type'=>'select','name'=>'_floor','options'=>$_floortype),
	 array('label'=>'Disabled Friendly','type'=>'radio','name'=>'_disabled_friendly','options'=>array('Yes','No')),
	);
	echo '<h4 class="title  htitle">Room Details/Prices</h4>';
		echo '<div class="binfo allinfo">';	
		$_rooms = get_post_meta($post->ID, '_rooms', true);
		if($_rooms){
		print_extra_fields_table($fields,$post->ID);
		}
		else
		{
			echo '<h4>Please add number of rooms in Housing Details and save your housing to add room details.</h4>';
		}
	echo '</div>';
}
function room_address() {
    global $post;
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
	$_address = get_post_meta($post->ID, '_address', true);
	$_postal_code = get_post_meta($post->ID, '_postal_code', true);
	$_lat = get_post_meta($post->ID, '_lat', true);
	$_lng = get_post_meta($post->ID, '_lng', true);
	
	$addre=$_address.','.$_postal_code;
	if($_address){
		
		$start = getLatLong($addre);
		//$finish1 = getLatLong('Christ Church University CT1 1QU');
		//$distance1 = Haversine($start, $finish1);
		//$finish2 = getLatLong('The University of Kent, Canterbury, Kent, CT2 7NZ');
		//$distance2 = Haversine($start, $finish2);
		//$finish3 = getLatLong('Canterbury, Kent CT1 2RY');
		//$distance3 = Haversine($start, $finish3);
		//$finish4 = getLatLong('UCA Caterbury, CT1 3AN');
		//$distance4 = Haversine($start, $finish4);
	}
	$ltd=$_lat.','.$_lng;
	$re=getDistance($ltd);
	$fields=array(
	array('label'=>'House Address','type'=>'text','name'=>'_address'),
	array('label'=>'Postal Code','type'=>'text','name'=>'_postal_code'),
	array('label'=>'Latitude','type'=>'text','name'=>'_lat','value'=>$start[0]),
	array('label'=>'Longitude','type'=>'text','name'=>'_lng','value'=>$start[1])
	);
	$fieldswe=array(
	array('label'=>'Distance from Christ Church University','type'=>'text','name'=>'_ucl','value'=>$re[0]['distance']['text']),
	array('label'=>'Time from Christ Church University','type'=>'text','name'=>'_uclt','value'=>$re[0]['duration']['text']),
	array('label'=>'Distance from The University of Kent ','type'=>'text','name'=>'_kent','value'=>$re[1]['distance']['text']),
	array('label'=>'Time from The University of Kent ','type'=>'text','name'=>'_kentt','value'=>$re[1]['duration']['text']),
	array('label'=>'Distance from Canterbury Center','type'=>'text','name'=>'_canterbury','value'=>$re[2]['distance']['text']),
	array('label'=>'Time from Canterbury Center','type'=>'text','name'=>'_canterburyt','value'=>$re[2]['duration']['text']),
	array('label'=>'Distance from UCA Caterbury','type'=>'text','name'=>'_uca','value'=>$re[3]['distance']['text']),
	array('label'=>'Time from UCA Caterbury','type'=>'text','name'=>'_ucat','value'=>$re[3]['duration']['text']),
	);
	echo '<h4 class="title  htitle">Address & Map</h4>';
		echo '<div class="binfo allinfo">';	
	print_extra_fields($fields,$post->ID);
	echo '</div>';
	
	echo '<h4 class="title  htitle">Distances</h4>';
		echo '<div class="binfo allinfo">';	
	print_extra_fields($fieldswe,$post->ID);
	echo '</div>';
	
	echo '<h4 class="title">Please add address,postal code and save your housing in order to see map below</h4>';
	if($_address){
		
			
		
	echo ' <div style="height:300px; width:100%;" id="map-canvas"></div>';
	echo '<script type="text/javascript">jQuery(function(){
			initialize("'.$_address.', '.$_postal_code.'");
	})</script>';
	}
	
}
function housing_gallery(){
	  global $post;
	print_gallery($post->ID);
}
function housing_certificates(){
	  global $post;
	print_certificates($post->ID);
}
function housing_floorplans(){
	  global $post;
	print_floorplans($post->ID);
}
function save_housing_meta($post_id, $post) {
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
    return $post->ID;
    }
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ))
        return $post->ID;
    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though.
	$datas = $_POST['data'];
    // Add values of $events_meta as custom fields
    foreach ($datas as $key => $value) { // Cycle through the $events_meta array!
	
//	echo $key.'=>'.$value.'<br>';
	
        if( $post->post_type == 'revision' ) return; // Don't store custom data twice
        $value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
        if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
            update_post_meta($post->ID, $key, $value);
        } else { // If the custom field doesn't have a value
            add_post_meta($post->ID, $key, $value);
        }
        if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
    }
}
add_action('save_post', 'save_housing_meta', 2, 2); // save the custom fields
add_action('save_post','housing_save_gallery_images', 2, 2);
add_action('save_post','housing_save_certificates_images', 2, 2);
add_action('save_post','housing_save_floorplans', 2, 2);
require_once get_template_directory() . '/housing/meta-box.php';
require_once get_template_directory() . '/housing/columns.php';
function housing_save_gallery_images($post_id, $post)
{
	
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ))
        return $post->ID;
			$datas = $_POST['add_img'];
			
			$imagescount=count($datas);
			
		if(is_array($datas))
		{
			if(count($datas)>0)
			{
			
				$key='_gallery_images';
				$galleryids=implode(',',$datas);
				 if(get_post_meta($post_id, $key, FALSE)) { // If the custom field already has a value
				update_post_meta($post_id, $key, $galleryids);
			} else { // If the custom field doesn't have a value
				add_post_meta($post_id, $key, $galleryids);
			}
			
				
			}

		}
		else
		{
			$key='_gallery_images';
				$galleryids='';
				 if(get_post_meta($post_id, $key, FALSE)) { // If the custom field already has a value
				update_post_meta($post_id, $key, $galleryids);
			} else { // If the custom field doesn't have a value
				add_post_meta($post_id, $key, $galleryids);
			}
		}
	
}
function housing_save_certificates_images($post_id, $post)
{
	
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ))
        return $post->ID;
			$datas2 = $_POST['add_img2'];
			$imagescount=count($datas2);
		if(is_array($datas2))
		{
			if(count($datas2)>0)
			{
			
				$key2='_certificates_images';
				$certificatesids=implode(',',$datas2);
				 if(get_post_meta($post_id, $key2, FALSE)) { // If the custom field already has a value
				update_post_meta($post_id, $key2, $certificatesids);
			} else { // If the custom field doesn't have a value
				add_post_meta($post_id, $key2, $certificatesids);
			}
			
				
			}

		}
		else
		{
			$key='_certificates_images';
				$certificatesids='';
				 if(get_post_meta($post_id, $key, FALSE)) { // If the custom field already has a value
				update_post_meta($post_id, $key, $certificatesids);
			} else { // If the custom field doesn't have a value
				add_post_meta($post_id, $key, $galleryids);
			}
		}
	
}
function housing_save_floorplans($post_id, $post)
{
	
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ))
        return $post->ID;
			$datas2 = $_POST['add_img3'];
			$imagescount=count($datas2);
		if(is_array($datas2))
		{
			if(count($datas2)>0)
			{
			
				$key2='_floorplans';
				$certificatesids=implode(',',$datas2);
				 if(get_post_meta($post_id, $key2, FALSE)) { // If the custom field already has a value
				update_post_meta($post_id, $key2, $certificatesids);
			} else { // If the custom field doesn't have a value
				add_post_meta($post_id, $key2, $certificatesids);
			}
			
				
			}

		}
		else
		{
			$key='_floorplans';
				$certificatesids='';
				 if(get_post_meta($post_id, $key, FALSE)) { // If the custom field already has a value
				update_post_meta($post_id, $key, $certificatesids);
			} else { // If the custom field doesn't have a value
				add_post_meta($post_id, $key, $galleryids);
			}
		}
	
}
function getLatLong($address) {
 
    $address = str_replace(' ', '+', $address);
    $url = 'http://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&sensor=false';
 
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $geoloc = curl_exec($ch);
 
    $json = json_decode($geoloc);
    return array($json->results[0]->geometry->location->lat, $json->results[0]->geometry->location->lng);
 
}
function getDistance($ltd) {
 
    $address = str_replace(' ', '+', $address);	
 	$url = 'https://maps.googleapis.com/maps/api/distancematrix/json?destinations=Canterbury+Christ+Church+University,+North+Homes+Road,+Canterbury,+Kent+CT1+1QU|The+University+of+Kent+Canterbury+Kent+CT2+7NZ|Canterbury+Kent+CT1+2RY|UCA+Caterbury+CT1+3AN&origins='.$ltd.'&mode=walking&sensor=false&units=imperial&key=AIzaSyBwvThgB5SQC3lDcj6W8qcHjpadGL7hrPI';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  	 $geoloc = curl_exec($ch); 	
    $json = json_decode($geoloc,true);	
// Will dump a beauty json :3
    return $json['rows'][0]['elements'];
 
}
function Haversine($start, $finish) {
 
    $theta = $start[1] - $finish[1]; 
    $distance = (sin(deg2rad($start[0])) * sin(deg2rad($finish[0]))) + (cos(deg2rad($start[0])) * cos(deg2rad($finish[0])) * cos(deg2rad($theta))); 
    $distance = acos($distance); 
    $distance = rad2deg($distance); 
    $distance = $distance * 60 * 1.1515; 
 
    return round($distance, 2);
 
}
?>