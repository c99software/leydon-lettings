<?php
/*
 Template Name: Search Results
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

 get_header(); ?>

 <div id="primary" class="content-area">
 	<div id="content" class="site-content" role="main">
 		<div class="container page_style">
	<div class="row">
        <?php RethinkBreadcrumb();?>
    </div>
	<div class="row">
 				<p class="innerpagehead">Student Houses To Let - Results</p>
 				<div class="viewhead">
 					<div class="viewheaddiv1">
 						<div class="view1">View as:</div>
 						<a class="view2" href="#"></a>
 						<a class="view3" href="#"></a>
 						<a class="view4" href="#"></a>
 					</div><!--viewheaddiv1-->
 					<div class="viewheaddiv2">
 						<a class="view5" href="#"><p>Bedrooms</p></a>
 						<div class="view6">
 							<a id="view6-1" href="#"></a><p>Price &nbsp;&nbsp; Low</p>
 							<a id="view6-2" href="#"></a>
 							<a id="view6-3" href="#"></a><p>High</p>
 						</div><!--view6-->
 						<div class="view7" href="#"><a href="#" id="view7-1"></a>All</div>
 					</div><!--viewheaddiv2-->

 				</div><!--viewhead-->
 				<div class="list">
 					<table border="0px;" class="downloadtable">
 						<thead>
 							<tr>
 								<th width="152" colspan="2">Available Next Year</th>
 								<th width="165" >House Address</th>
 								<th width="77">Postcode</th>
 								<th width="69">Rooms</th>
 								<th width="162" colspan="2">Available This Year</th>
 								<th width="10" ></th>

 								<th width="81">Price Low</th>
 								<th width="72">Price High</th>
 								<th width="167"></th>
 							</tr>
 						</thead>
 						<tbody>
 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center" >reserver </td> <td width="40" bgcolor="#d3ffcc" align="center"  >4</td>
 								<td bgcolor="#FFFFFF" ></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td bgcolor="#FFFFFF" align="center" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF"><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center" >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7">-</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF"><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center"  >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7"> -</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF" ><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center" >reserver </td> <td width="40" bgcolor="#d3ffcc" align="center"  >4</td>
 								<td bgcolor="#FFFFFF" ></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td bgcolor="#FFFFFF" align="center" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF"><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center" >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7">-</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF"><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center"  >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7"> -</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF" ><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>
 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center" >reserver </td> <td width="40" bgcolor="#d3ffcc" align="center"  >4</td>
 								<td bgcolor="#FFFFFF" ></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td bgcolor="#FFFFFF" align="center" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF"><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center" >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7">-</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF"><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center"  >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7"> -</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF" ><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>
 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center" >reserver </td> <td width="40" bgcolor="#d3ffcc" align="center"  >4</td>
 								<td bgcolor="#FFFFFF" ></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td bgcolor="#FFFFFF" align="center" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF"><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center" >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7">-</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF"><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center"  >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7"> -</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF" ><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>
 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center" >reserver </td> <td width="40" bgcolor="#d3ffcc" align="center"  >4</td>
 								<td bgcolor="#FFFFFF" ></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td bgcolor="#FFFFFF" align="center" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF"><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center" >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7">-</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF"><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center"  >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7"> -</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF" ><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>
 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center"  >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7"> -</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF" ><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>
 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center" >reserver </td> <td width="40" bgcolor="#d3ffcc" align="center"  >4</td>
 								<td bgcolor="#FFFFFF" ></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td bgcolor="#FFFFFF" align="center" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF"><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center" >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7">-</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF"><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center"  >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7"> -</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF" ><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>
 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center" >reserver </td> <td width="40" bgcolor="#d3ffcc" align="center"  >4</td>
 								<td bgcolor="#FFFFFF" ></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td bgcolor="#FFFFFF" align="center" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF"><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center" >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7">-</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF"><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center"  >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7"> -</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF" ><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>
 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center" >reserver </td> <td width="40" bgcolor="#d3ffcc" align="center"  >4</td>
 								<td bgcolor="#FFFFFF" ></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td bgcolor="#FFFFFF" align="center" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF"><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center" >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7">-</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF"><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>

 							<tr>
 								<td bgcolor="#ebf5fd">104 Old Park Avenue</td>
 								<td align="center" bgcolor="#FFFFFF">CT11DN</td>
 								<td align="center" bgcolor="#ebf5fd">4</td>
 								<td bgcolor="#ffa7a7" align="center"  >reserver </td> <td width="40" align="center"  bgcolor="#ffa7a7"> -</td>
 								<td bgcolor="#FFFFFF"></td>
 								<td align="center"  bgcolor="#d3ffcd">available </td> <td align="left"  bgcolor="#d3ffcd"> 4</td>
 								<td align="center" bgcolor="#FFFFFF" >$5</td>
 								<td align="center" bgcolor="#ebf5fd" >$123</td>
 								<td bgcolor="#FFFFFF" ><div class="tablebtn"><a href="#">View property</a></div></td>
 							</tr>
 						</tbody>
 					</table>
 				</div><!--list-->
 				<div class="pagecontrol">
 					<div class="pageno">

 						<ul>
 							<li><a href="#">1</a></li>
 							<li><a href="#">2</a></li>
 							<li><a href="#">3</a></li>
 							<li><a href="#">4</a></li>
 							<li><a href="#">5</a></li>
 							<li><a href="#">6</a></li>
 							<li><a href="#">7</a></li>
 						</ul>
 					</div><!--pageno-->
 					<a href="#" class="ppage">Prev page</a>
 					<a href="#" class="npage">Next page</a>


 				</div><!--pagecontrol-->


 			</div><!--innerpage-->
 		</div>
 	</div><!-- #content -->
 </div><!-- #primary -->

 <?php get_sidebar(); ?>
 <?php get_footer(); ?>