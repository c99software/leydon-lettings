<?php
/**
 * Twenty Thirteen functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * see http://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

/**
 * Sets up the content width value based on the theme's design.
 * @see twentythirteen_content_width() for template-specific adjustments.
 */
if ( ! isset( $content_width ) )
    $content_width = 604;

/**
 * Adds support for a custom header image.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 *
Adds support fro hostel management
 */
require_once get_template_directory() . '/housing/init.php';
/**
 * Twenty Thirteen only works in WordPress 3.6 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '3.6-alpha', '<' ) )
    require get_template_directory() . '/inc/back-compat.php';

/**
 * Sets up theme defaults and registers the various WordPress features that
 * Twenty Thirteen supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add Visual Editor stylesheets.
 * @uses add_theme_support() To add support for automatic feed links, postmeta
 * formats, and post thumbnails.
 * @uses register_nav_menu() To add support for a navigation menu.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return void
 */
function twentythirteen_setup() {
    /*
     * Makes Twenty Thirteen available for translation.
     *
     * Translations can be added to the /languages/ directory.
     * If you're building a theme based on Twenty Thirteen, use a find and
     * replace to change 'twentythirteen' to the name of your theme in all
     * template files.
     */
    load_theme_textdomain( 'twentythirteen', get_template_directory() . '/languages' );

    /*
     * This theme styles the visual editor to resemble the theme style,
     * specifically font, colors, icons, and column width.
     */
    add_editor_style( array( 'css/editor-style.css', 'fonts/genericons.css', twentythirteen_fonts_url() ) );

    // Adds RSS feed links to <head> for posts and comments.
    add_theme_support( 'automatic-feed-links' );

    // Switches default core markup for search form, comment form, and comments
    // to output valid HTML5.
    add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

    /*
     * This theme supports all available post formats by default.
     * See http://codex.wordpress.org/Post_Formats
     */
    add_theme_support( 'post-formats', array(
        'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
    ) );

    // This theme uses wp_nav_menu() in one location.
    register_nav_menu( 'primary', __( 'Navigation Menu', 'twentythirteen' ) );

    /*
     * This theme uses a custom image size for featured images, displayed on
     * "standard" posts and pages.
     */
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 604, 270, true );

    // This theme uses its own gallery styles.
    add_filter( 'use_default_gallery_style', '__return_false' );
}
add_action( 'after_setup_theme', 'twentythirteen_setup' );

/**
 * Returns the Google font stylesheet URL, if available.
 *
 * The use of Source Sans Pro and Bitter by default is localized. For languages
 * that use characters not supported by the font, the font can be disabled.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return string Font stylesheet or empty string if disabled.
 */
function twentythirteen_fonts_url() {
    $fonts_url = '';

    /* Translators: If there are characters in your language that are not
     * supported by Source Sans Pro, translate this to 'off'. Do not translate
     * into your own language.
     */
    $source_sans_pro = _x( 'on', 'Source Sans Pro font: on or off', 'twentythirteen' );

    /* Translators: If there are characters in your language that are not
     * supported by Bitter, translate this to 'off'. Do not translate into your
     * own language.
     */
    $bitter = _x( 'on', 'Bitter font: on or off', 'twentythirteen' );

    if ( 'off' !== $source_sans_pro || 'off' !== $bitter ) {
        $font_families = array();

        if ( 'off' !== $source_sans_pro )
            $font_families[] = 'Source Sans Pro:300,400,700,300italic,400italic,700italic';

        if ( 'off' !== $bitter )
            $font_families[] = 'Bitter:400,700';

        $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext' ),
        );
        $fonts_url = add_query_arg( $query_args, "//fonts.googleapis.com/css" );
    }

    return $fonts_url;
}

/**
 * Enqueues scripts and styles for front end.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return void
 */
function twentythirteen_scripts_styles() {
    // Adds JavaScript to pages with the comment form to support sites with
    // threaded comments (when in use).
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
        wp_enqueue_script( 'comment-reply' );

    // Adds Masonry to handle vertical alignment of footer widgets.
    if ( is_active_sidebar( 'sidebar-1' ) )
        wp_enqueue_script( 'jquery-masonry' );

    // Loads JavaScript file with functionality specific to Twenty Thirteen.
    wp_enqueue_script( 'twentythirteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '2013-07-18', true );

    // Add Open Sans and Bitter fonts, used in the main stylesheet.
    wp_enqueue_style( 'twentythirteen-fonts', twentythirteen_fonts_url(), array(), null );

    // Add Genericons font, used in the main stylesheet.
    wp_enqueue_style( 'genericons', get_template_directory_uri() . '/fonts/genericons.css', array(), '2.09' );

    // Loads our main stylesheet.
    wp_enqueue_style( 'twentythirteen-style', get_stylesheet_uri(), array(), '2013-07-18' );

    // Loads the Internet Explorer specific stylesheet.
    wp_enqueue_style( 'twentythirteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentythirteen-style' ), '2013-07-18' );
    wp_style_add_data( 'twentythirteen-ie', 'conditional', 'lt IE 9' );
}
add_action( 'wp_enqueue_scripts', 'twentythirteen_scripts_styles' );

/**
 * Creates a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function twentythirteen_wp_title( $title, $sep ) {
    global $paged, $page;

    if ( is_feed() )
        return $title;

    // Add the site name.
    $title .= get_bloginfo( 'name' );

    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        $title = "$site_description $sep $title ";

    // Add a page number if necessary.
    if ( $paged >= 2 || $page >= 2 )
        $title = "$title $sep " . sprintf( __( 'Page %s', 'twentythirteen' ), max( $paged, $page ) )."$sep $title  ";

    return $title;
}
add_filter( 'wp_title', 'twentythirteen_wp_title', 10, 2 );

/**
 * Registers two widget areas.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return void
 */
function twentythirteen_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Main Widget Area', 'twentythirteen' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Appears in the footer section of the site.', 'twentythirteen' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="none">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Secondary Widget Area', 'twentythirteen' ),
        'id'            => 'sidebar-2',
        'description'   => __( 'Appears on posts and pages in the sidebar.', 'twentythirteen' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
}
add_action( 'widgets_init', 'twentythirteen_widgets_init' );

if ( ! function_exists( 'twentythirteen_paging_nav' ) ) :
    /**
     * Displays navigation to next/previous set of posts when applicable.
     *
     * @since Twenty Thirteen 1.0
     *
     * @return void
     */
    function twentythirteen_paging_nav() {
        global $wp_query;

        // Don't print empty markup if there's only one page.
        if ( $wp_query->max_num_pages < 2 )
            return;
        ?>
        <nav class="navigation paging-navigation" role="navigation">
            <h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'twentythirteen' ); ?></h1>
            <div class="nav-links">

                <?php if ( get_next_posts_link() ) : ?>
                    <div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'twentythirteen' ) ); ?></div>
                <?php endif; ?>

                <?php if ( get_previous_posts_link() ) : ?>
                    <div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?></div>
                <?php endif; ?>

            </div><!-- .nav-links -->
        </nav><!-- .navigation -->
    <?php
    }
endif;

if ( ! function_exists( 'twentythirteen_post_nav' ) ) :
    /**
     * Displays navigation to next/previous post when applicable.
     *
     * @since Twenty Thirteen 1.0
     *
     * @return void
     */
    function twentythirteen_post_nav() {
        global $post;

        // Don't print empty markup if there's nowhere to navigate.
        $previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
        $next     = get_adjacent_post( false, '', false );

        if ( ! $next && ! $previous )
            return;
        ?>
        <nav class="navigation post-navigation" role="navigation">
            <h1 class="screen-reader-text"><?php _e( 'Post navigation', 'twentythirteen' ); ?></h1>
            <div class="nav-links">

                <?php previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'twentythirteen' ) ); ?>
                <?php next_post_link( '%link', _x( '%title <span class="meta-nav">&rarr;</span>', 'Next post link', 'twentythirteen' ) ); ?>

            </div><!-- .nav-links -->
        </nav><!-- .navigation -->
    <?php
    }
endif;

if ( ! function_exists( 'twentythirteen_entry_meta' ) ) :
    /**
     * Prints HTML with meta information for current post: categories, tags, permalink, author, and date.
     *
     * Create your own twentythirteen_entry_meta() to override in a child theme.
     *
     * @since Twenty Thirteen 1.0
     *
     * @return void
     */
    function twentythirteen_entry_meta() {
        if ( is_sticky() && is_home() && ! is_paged() )
            echo '<span class="featured-post">' . __( 'Sticky', 'twentythirteen' ) . '</span>';

        if ( ! has_post_format( 'link' ) && 'post' == get_post_type() )
            twentythirteen_entry_date();

        // Translators: used between list items, there is a space after the comma.
        $categories_list = get_the_category_list( __( ', ', 'twentythirteen' ) );
        if ( $categories_list ) {
            echo '<span class="categories-links">' . $categories_list . '</span>';
        }

        // Translators: used between list items, there is a space after the comma.
        $tag_list = get_the_tag_list( '', __( ', ', 'twentythirteen' ) );
        if ( $tag_list ) {
            echo '<span class="tags-links">' . $tag_list . '</span>';
        }

        // Post author
        if ( 'post' == get_post_type() ) {
            printf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
                esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
                esc_attr( sprintf( __( 'View all posts by %s', 'twentythirteen' ), get_the_author() ) ),
                get_the_author()
            );
        }
    }
endif;

function leydon_entry_meta() {
        if ( 'post' == get_post_type() ) {
            printf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
                esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
                esc_attr( sprintf( __( 'View all posts by %s', 'twentythirteen' ), get_the_author() ) ),
                get_the_author()
            );
            echo '<span class="post-sep">&#124; </span>';
        }
        if ( ! has_post_format( 'link' ) && 'post' == get_post_type() )
            twentythirteen_entry_date();echo '<span class="post-sep">&#124; </span>';

        // Translators: used between list items, there is a space after the comma.
        $categories_list = get_the_category_list( __( ', ', 'twentythirteen' ) );
        if ( $categories_list ) {
            echo '<span class="categories-links">' . $categories_list . '</span>';
        }

        // Post author
       
}
if ( ! function_exists( 'twentythirteen_entry_date' ) ) :
    /**
     * Prints HTML with date information for current post.
     *
     * Create your own twentythirteen_entry_date() to override in a child theme.
     *
     * @since Twenty Thirteen 1.0
     *
     * @param boolean $echo Whether to echo the date. Default true.
     * @return string The HTML-formatted post date.
     */
    function twentythirteen_entry_date( $echo = true ) {
        if ( has_post_format( array( 'chat', 'status' ) ) )
            $format_prefix = _x( '%1$s on %2$s', '1: post format name. 2: date', 'twentythirteen' );
        else
            $format_prefix = '%2$s';

        $date = sprintf( '<span class="date"><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
            esc_url( get_permalink() ),
            esc_attr( sprintf( __( 'Permalink to %s', 'twentythirteen' ), the_title_attribute( 'echo=0' ) ) ),
            esc_attr( get_the_date( 'c' ) ),
            esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format() ), get_the_date() ) )
        );

        if ( $echo )
            echo $date;

        return $date;
    }
endif;

if ( ! function_exists( 'twentythirteen_the_attached_image' ) ) :
    /**
     * Prints the attached image with a link to the next attached image.
     *
     * @since Twenty Thirteen 1.0
     *
     * @return void
     */
    function twentythirteen_the_attached_image() {
        $post                = get_post();
        $attachment_size     = apply_filters( 'twentythirteen_attachment_size', array( 724, 724 ) );
        $next_attachment_url = wp_get_attachment_url();

        /**
         * Grab the IDs of all the image attachments in a gallery so we can get the URL
         * of the next adjacent image in a gallery, or the first image (if we're
         * looking at the last image in a gallery), or, in a gallery of one, just the
         * link to that image file.
         */
        $attachment_ids = get_posts( array(
            'post_parent'    => $post->post_parent,
            'fields'         => 'ids',
            'numberposts'    => -1,
            'post_status'    => 'inherit',
            'post_type'      => 'attachment',
            'post_mime_type' => 'image',
            'order'          => 'ASC',
            'orderby'        => 'menu_order ID'
        ) );

        // If there is more than 1 attachment in a gallery...
        if ( count( $attachment_ids ) > 1 ) {
            foreach ( $attachment_ids as $attachment_id ) {
                if ( $attachment_id == $post->ID ) {
                    $next_id = current( $attachment_ids );
                    break;
                }
            }

            // get the URL of the next image attachment...
            if ( $next_id )
                $next_attachment_url = get_attachment_link( $next_id );

            // or get the URL of the first image attachment.
            else
                $next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );
        }

        printf( '<a href="%1$s" title="%2$s" rel="attachment">%3$s</a>',
            esc_url( $next_attachment_url ),
            the_title_attribute( array( 'echo' => false ) ),
            wp_get_attachment_image( $post->ID, $attachment_size )
        );
    }
endif;

/**
 * Returns the URL from the post.
 *
 * @uses get_url_in_content() to get the URL in the post meta (if it exists) or
 * the first link found in the post content.
 *
 * Falls back to the post permalink if no URL is found in the post.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return string The Link format URL.
 */
function twentythirteen_get_link_url() {
    $content = get_the_content();
    $has_url = get_url_in_content( $content );

    return ( $has_url ) ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}

/**
 * Extends the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Active widgets in the sidebar to change the layout and spacing.
 * 3. When avatars are disabled in discussion settings.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function twentythirteen_body_class( $classes ) {
    if ( ! is_multi_author() )
        $classes[] = 'single-author';

    if ( is_active_sidebar( 'sidebar-2' ) && ! is_attachment() && ! is_404() )
        $classes[] = 'sidebar';

    if ( ! get_option( 'show_avatars' ) )
        $classes[] = 'no-avatars';

    return $classes;
}
add_filter( 'body_class', 'twentythirteen_body_class' );

/**
 * Adjusts content_width value for video post formats and attachment templates.
 *
 * @since Twenty Thirteen 1.0
 *
 * @return void
 */
function twentythirteen_content_width() {
    global $content_width;

    if ( is_attachment() )
        $content_width = 724;
    elseif ( has_post_format( 'audio' ) )
        $content_width = 484;
}
add_action( 'template_redirect', 'twentythirteen_content_width' );

/**
 * Add postMessage support for site title and description for the Customizer.
 *
 * @since Twenty Thirteen 1.0
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 * @return void
 */
function twentythirteen_customize_register( $wp_customize ) {
    $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
    $wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'twentythirteen_customize_register' );

/**
 * Binds JavaScript handlers to make Customizer preview reload changes
 * asynchronously.
 *
 * @since Twenty Thirteen 1.0
 */
function twentythirteen_customize_preview_js() {
    wp_enqueue_script( 'twentythirteen-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), '20130226', true );
}
add_action( 'customize_preview_init', 'twentythirteen_customize_preview_js' );
function subval_sort($a,$subkey,$sr)
{
    $b=array();
    foreach($a as $k=>$v) {
        $b[$k] = strtolower($v[$subkey]);
    }
    if($sr=='asc'){
        asort($b);}
    else{arsort($b);}
    foreach($b as $key=>$val) {
        $c[] = $a[$key];
    }
    return $c;
}

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
    } else {
        $excerpt = implode(" ",$excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
    return $excerpt;
}

function content($limit) {
    $content = explode(' ', get_the_content(), $limit);
    if (count($content)>=$limit) {
        array_pop($content);
        $content = implode(" ",$content).'...';
    } else {
        $content = implode(" ",$content);
    }
    $content = preg_replace('/\[.+\]/','', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}

function RethinkBreadcrumb() {
    if (!is_home()) {

        if (is_category() || is_single()) {
            echo '<ol class="breadcrumb"><li class="first">You are here:</li><li><a href="';
            echo get_option('home');
            echo '">';
            echo 'Home';
            echo "</a></li>";
            echo "<li>";
            the_category('-');
            echo "</li>";
            if (is_single()) {
                echo "<li class='current'>";
                the_title();
                echo "</li>";
            }
            echo "</ol>";
        } elseif (is_page()) {
            
            echo '<ol class="breadcrumb"><li class="first">You are here:</li><li><a href="';
            echo get_option('home');
            echo '">';
            echo 'Home';
            echo "</a></li>";
            echo "<li class='current'>";
            the_title();
            echo "</li>";
            echo "</ol>";
        }

    }
}

if (function_exists('st_makeEntries')) :
    add_shortcode('sharethis', 'st_makeEntries');
endif;

function new_excerpt_more( $more ) {
    return '...<br /><a class="read-more" href="'. get_permalink( get_the_ID() ) . '">Continue reading <span class="meta-nav">&rarr;</span></a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

add_action('admin_menu', 'add_available_table_options');
function add_available_table_options()
{
    add_options_page('Available Table Options', 'Available Table Options', 'manage_options', 'functions','available_table_options');
}

function available_table_options()
{
    ?>
    <div class="wrap">
        <h2>Available Table Options</h2>
        <form method="post" action="options.php">
            <?php wp_nonce_field('update-options') ?>
            <p><strong>Current Year <?php echo date('Y').' - '.(date('Y')+1);?></strong><br />
                <select name="at_current_year">
                    <option value="_availability_this_year" <?php if(get_option('at_current_year')=='_availability_this_year'){echo 'selected="selected"';};?>>Availability Table 1</option>
                    <option value="_availability_next_year" <?php if(get_option('at_current_year')=='_availability_next_year'){echo 'selected="selected"';};?>">Availability Table 2</option>
                    <option value="_availability_next_next_year" <?php if(get_option('at_current_year')=='_availability_next_next_year'){echo 'selected="selected"';};?>">Availability Table 3</option>
                </select>
            </p>
            <p><strong>Next Year <?php echo (date('Y')+1).' - '.(date('Y')+2);?></strong><br />
                <select name="at_next_year">
                    <option value="_availability_this_year" <?php if(get_option('at_next_year')=='_availability_this_year'){echo 'selected="selected"';};?>>Availability Table 1</option>
                    <option value="_availability_next_year" <?php if(get_option('at_next_year')=='_availability_next_year'){echo 'selected="selected"';};?>">Availability Table 2</option>
                    <option value="_availability_next_next_year" <?php if(get_option('at_next_year')=='_availability_next_next_year'){echo 'selected="selected"';};?>">Availability Table 3</option>
                </select>
            </p>
            <p><input type="submit" name="Submit" value="Save Setting" /></p>
            <input type="hidden" name="action" value="update" />
            <input type="hidden" name="page_options" value="at_current_year,at_next_year" />
        </form>
        <hr/>
        <form method="post">
            <select name="action_reset_table">
                <option value="" selected="selected">Select Table to reset</option>
                <!-- <option value="_availability_this_year">Availability Table 1</option>
                <option value="_availability_next_year">Availability Table 2</option>
                -->
                <option value="_availability_next_next_year">Availability Table 3</option>
            </select>
            <input type="submit" name="Submit" value="Reset Table" />
        </form>
        <?php
        if (isset($_POST['action_reset_table'])){
            if($_POST['action_reset_table']==""){
                echo "** Reset Availability Table ";
            } else {
                echo reset_availability_table($_POST['action_reset_table']);
            }
        }
        ?>
    </div>
<?php
}
function reset_availability_table($availability_table_name){
    if ($availability_table_name=="_availability_this_year" || $availability_table_name=="_availability_next_year" || $availability_table_name=="_availability_next_next_year"){
    global $wpdb;
    $result=$wpdb->query("
DELETE
FROM $wpdb->postmeta
WHERE meta_key LIKE '".$availability_table_name."_%'
");
    } else {
        $result=false;
    }
    if ($result){
        echo $availability_table_name." has been reset";
    } else {
        echo "Error or No record was found!";
        if ($availability_table_name==""){
            echo "** Please select table to reset";
        }
    }   
}
function house_search_scripts(){
    wp_enqueue_script('house_search',get_stylesheet_directory_uri(). '/js/house_search.js',array(),'1.0.0',true);
    wp_localize_script('house_search','ajax_url',admin_url('admin-ajax.php'));

}
function house_search(){
    house_search_scripts();
    ob_start();?>
    <div class="house_search_inner col-lg-3 col-md-4 col-sm-12 col-xs-12" >

    <div id="house_search"  class="" >
        
            <h3> Property Criteria </h3>
            <form action="" method="get">
                <input id="view_input" type="hidden" name="view" value="2" />
                <input id="sort" type="hidden" name="sort" value=" 1" />
                <input id="perpage" type="hidden" name="perpage" value="10" />
                <select id="location" name="location">
                    <option value="0"> Which university or location? </option>
                    <option value="1"> UKC</option>
                    <option value="2"> CCCU</option>
                    <option value="3"> UCA</option>
                    <option value="4"> Canterbury City Centre</option>
                </select>
                <select id="room_count" name="rooms">
                    <option value="0" > How many bedrooms? </option>
                    <option value="0">All Bedrooms</option>
                    <option value="1">1 Bedroom</option>
                    <option value="2">2 Bedroom</option>
                    <option value="3">3 Bedroom</option>
                    <option value="4">4 Bedroom</option>
                    <option value="5">5 Bedroom</option>
                    <option value="6">6 Bedroom</option>
                </select>
                
                <select id="let_type" name="let_type">
                    <option value="0"> Are you a student or professional? </option>
                    <option value="1"> Student</option>
                    <option value="2"> Professional</option>
                </select>
                <?php /*?>
                <select id="avail" name="avail">
                    <option value="0"> Select Available Years </option>
                    <option value="1" selected> <?php echo date("Y"); ?> to <?php echo date('Y', strtotime('+1 year')); ?></option>
                    <option value="2"> <?php echo date('Y', strtotime('+1 year')); ?> to <?php echo date('Y', strtotime('+2 year')); ?></option>
                </select>
                <?php */?>
                <div class="col-lg-12 col-md-12 col-sm-4 col-xs-6 check ensuite">
                    <label> En Suite </label>
                    <input id="has_ensuite" type="checkbox" name="ensuite"/>
                    <span class="icon"> </span>
                </div>
               
                <div class="col-lg-12 col-md-12 col-sm-4 col-xs-6 check parking">
                    <label> Parking </label>
                    <input id="has_parking" type="checkbox" name="parking"/>
                    <span class="icon"> </span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-4 col-xs-6 check garden">
                    <label> Garden </label>
                    <input id="has_garden" type="checkbox" name="garden"/>
                    <span class="icon"> </span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-4 col-xs-6 check bathroom">
                    <label> Bath </label>
                    <input id="has_bathroom" type="checkbox" name="bathroom"/>
                    <span class="icon"> </span>
                </div>
                <!--
                <div class="col-lg-12 col-md-12 col-sm-4 col-xs-6 check toilet">
                    <label> Shared Toilet </label>
                    <input id="has_toilet" type="checkbox" name="toilet"/>
                    <span class="icon view10_r "> </span>
                </div>
                -->

                 
                <?php if(get_page_uri(get_the_ID()) == "disabled"):?>
                    <div class="col-lg-12 col-md-12 col-sm-4 col-xs-6 check active wheel">
                        <label> Wheelchair Friendly </label>
                        <input id="has_wheel" type="checkbox" name="wheel" checked/>
                        <span class="icon"> </span>
                    </div>
                <?php else: ?>
                <div class="col-lg-12 col-md-12 col-sm-4 col-xs-6 check wheel">
                    <label> Wheelchair Friendly </label>
                    <input id="has_wheel" type="checkbox" name="wheel"/>
                    <span class="icon"> </span>
                </div>
                <?php endif;?>

                <div class="col-lg-12 col-md-12 col-sm-4 col-xs-6 check availthis  ">
                    <label> Available <?php echo date("Y", strtotime('-1 year')); ?> to <?php echo date('y'); ?></label>
                    <input id="avail_this" type="checkbox" name="avail_this"  />
              
                </div>
                <div class="col-lg-12 col-md-12 col-sm-4 col-xs-6 check availnext active">
                    <label> Available <?php echo date('Y'); ?> to <?php echo date('y', strtotime('+1 year')); ?></label>
                    <input id="avail_next" type="checkbox" name="avail_next" checked />
     
                </div>
                 <button class="button" type="submit"> Update Search </button>
            </form>
        </div>
    </div>
    
    <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
        <div id="search_top">
            <div class="type">
                <a href="javascript:void(0)" class="type_grid"  data-view="2"> By Picture <span> </span> </a>
                <a href="javascript:void(0)" class="type_list" data-view="1"> List <span> </span></a>
                <a href="javascript:void(0)" class="type_map" data-view="3" > Map<span> </span> </a>
            </div>
             <div class="sort ">
                <select id="sort_select">
                    <option value="1"> Sort: Price </option>
                    <option value="2"> Sort: Distance </option>
                </select>
            </div>
            <div class="perpage ">
                <label> Results: </label>
                <select id="per_page_select">
                    <option value="-1"> All </option>
                    <option value="10"> 10 </option>
                    <option value="15"> 15 </option>
                    <option value="20"> 20 </option>
                    <option value="30"> 30 </option>
                    
                </select>
            </div>
           
        </div>
        <div class=""  id="house_results" >
            <div class="results_overlay">
                <h2> Loading.. <span class="loading"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/loading_ajax.gif" alt="test"> </span> </h2>
            </div>
            <div id="list_results">
                <div class="flipTable_header">
                    <div class="col-lg-2 col-md-3 col-sm-4 col-xs-4">
                        House Address
                    </div>
                     <div class="col-lg-4 col-md-3 hidden-xs hidden-sm">
                         House Includes
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
                        Postcode
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 avail_head ">
                        Rooms Available <?php echo date("Y"); ?>-<?php echo date('Y', strtotime('+1 year')); ?>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 avail_head ">
                        Rooms Available <?php echo date('Y', strtotime('+1 year')); ?>-<?php echo date('Y', strtotime('+2 year')); ?>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
                        Distance from <span id="distance_from"> </span>
                    </div>
                </div>
                <div id="house_results_container" >
                </div>
            </div>
            <div id="grid_results" class="grid housing_carousel">
                <ul> </ul>
            </div>

            <div class="list map" id="mapview">

            </div>
        </div>
    </div>
    <?php 
    return ob_get_clean();
}

add_shortcode ('house_search','house_search');
add_action('wp_ajax_house_search','house_search_callback');
add_action('wp_ajax_nopriv_house_search','house_search_callback');

function house_search_callback(){

    header("Content-Type: application/json");

    $with_ensuite = 0;
    if(isset($_GET['ensuite']))
        $with_ensuite = intval(sanitize_text_field($_GET['ensuite']));

    $room_count = 0;
    if(isset($_GET['rooms']))
        $room_count = intval(sanitize_text_field($_GET['rooms']));

    $let_type = 0;
    if(isset($_GET['lettype']))
        $let_type = intval(sanitize_text_field($_GET['lettype']));

    $with_parking = 0;
    if(isset($_GET['ensuite']))
        $with_parking = intval(sanitize_text_field($_GET['parking']));

    $with_wheel = 0;
    if(isset($_GET['wheel']))
        $with_wheel = intval(sanitize_text_field($_GET['wheel']));

    $with_garden= 0;
    if(isset($_GET['garden']))
        $with_garden = intval(sanitize_text_field($_GET['garden']));
    $avail_next = 0;
    if(isset($_GET['availnext']))
        $avail_next = intval(sanitize_text_field($_GET['availnext']));
    $avail_this = 0;
    if(isset($_GET['availthis']))
        $avail_this = intval(sanitize_text_field($_GET['availthis']));

    $tag_filter = 0;
    if(isset($_GET['tag']))
        $tag_filter = strval(sanitize_text_field($_GET['tag']));
    
  
    $orderby= 0;
    if(isset($_GET['sort']))
        $orderby = intval(sanitize_text_field($_GET['sort']));
        
     $perpage= 0;
    if(isset($_GET['perpage']))
        $perpage = intval(sanitize_text_field($_GET['perpage']));
     $location= 0;
    if(isset($_GET['location']))
        $location = intval(sanitize_text_field($_GET['location']));        

    $args =array(
        "post_type" =>"housing",
        "posts_per_page" => $perpage,
        
    );  

    if ($let_type == "1"){
          $args['tax_query'][] = array(
        array(
            'taxonomy' => 'type',
            'field'    => 'slug',
            'terms'    => 'student-only',
        ));
    }
    if ($let_type == "2"){
          $args['tax_query'][] = array(
        array(
            'taxonomy' => 'type',
            'field'    => 'slug',
            'terms'    => 'professional',
        ));
    }
  
    if ($room_count != "0"){
        $args['meta_query'][] = array(
            'key' => '_rooms',
            'value' => $room_count,
            'compare' => "="
        );
    }
    
    if ($with_ensuite != "0"){
        $args['meta_query'][] = array(
            'key' => '_en_suite',
        'value' => $with_ensuite,
        'compare' => ">="
        );
    }
    if ($with_parking != "0"){
        $args['meta_query'][] = array(
            'relation' => 'OR',
            array(
                'key' => '_parking_bay',
                'value' => $with_parking,
                'compare' => ">="
            ),
            array(
                'key' => '_parking_road',
                'value' => 'Yes',
                'compare' => "="
            ),
             array(
                'key' => '_parking_bay',
                'value' => 'Yes',
                'compare' => "="
            )
        );         
    }
    if ($with_wheel != "0"){
        $args['meta_query'][] = array(
            'key' => '_Wheelchair',
        'value' => $with_wheel,
        'compare' => ">="
        );
    }
    if ($with_garden != "0"){
        $args['meta_query'][] = array(
            'key' => '_rear_garden',
        'value' => $with_garden,
        'compare' => ">="
        );
    }
     if($tag_filter != "0"){
            $args['tax_query'][] = array(
                array(
                    'taxonomy' => 'location',
                    'field'    => 'slug',
                    'terms'    => $tag_filter,
                ));
    }
    if ($avail_this == "1"){
        $args['meta_query'][] = array(
            'relation' => 'OR',
            array(
                'key' => '_availability_this_year_1',
                'value' => 'Yes',
                'compare' => "="
            ),
            array(
                'key' => '_availability_this_year_2',
                'value' => 'Yes',
                'compare' => "="
            ),
             array(
                'key' => '_availability_this_year_3',
                'value' => 'Yes',
                'compare' => "="
            ),
              array(
                'key' =>'_availability_this_year_4',
                'value' => 'Yes',
                'compare' => "="
            ),
               array(
                'key' => '_availability_this_year_5',
                'value' => 'Yes',
                'compare' => "="
            ),
                array(
                'key' => '_availability_this_year_6',
                'value' => 'Yes',
                'compare' => "="
            ),
                 array(
                'key' => '_availability_this_year_7',
                'value' => 'Yes',
                'compare' => "="
            )
        );
    }
    if ($avail_next == "1"){
        $args['meta_query'][] = array(
            'relation' => 'OR',
            array(
                'key' => '_availability_next_year_1',
                'value' => 'Yes',
                'compare' => "="
            ),
            array(
                'key' => '_availability_next_year_2',
                'value' => 'Yes',
                'compare' => "="
            ),
             array(
                'key' => '_availability_next_year_3',
                'value' => 'Yes',
                'compare' => "="
            ),
              array(
                'key' =>'_availability_next_year_4',
                'value' => 'Yes',
                'compare' => "="
            ),
               array(
                'key' => '_availability_next_year_5',
                'value' => 'Yes',
                'compare' => "="
            ),
                array(
                'key' => '_availability_next_year_6',
                'value' => 'Yes',
                'compare' => "="
            ),
                 array(
                'key' => '_availability_next_year_7',
                'value' => 'Yes',
                'compare' => "="
            )
        );
    }

   
    if ($orderby != "0"){
        if($orderby == "1"){
            $args['orderby'] = 'meta_value_num';
            $args['meta_key'] = '_pcm';
            $args['order'] = "ASC";
        }
        if($orderby == "2"){
            $args['orderby'] = 'meta_value_num';
            $args['meta_key'] = '_kent';
            $args['order'] = "ASC";
        }
        
    }


    $house_query =new WP_Query($args);  

    while($house_query->have_posts()){
        $house_query->the_post();
        $thumb_id = get_post_thumbnail_id();
        $url = wp_get_attachment_image_src($thumb_id,'large', true);
        $rooms = get_post_meta(get_the_ID(),'_rooms' , true);
        $athis = 0;
        for($g = 1; $g <= $rooms; $g++){
            $room_check = get_post_meta(get_the_ID(), get_option('at_next_year').'_'.$g, true);
            if ($room_check == 'Yes'){
                $athis++;            
            }
        }

        $marker_url = get_bloginfo('template_url').'/markers/R'.$rooms.'-'.$athis.'A.png';

        $toilet_total = 0;
        $toilet_total = $toilet_total + get_post_meta(get_the_ID(), '_shared_toilet', true);
        
        if (get_post_meta(get_the_ID(), '_toilet_ensuite_1', true) == "Yes")
            $toilet_total++;
        if (get_post_meta(get_the_ID(), '_toilet_ensuite_2', true) == "Yes")
            $toilet_total++;
        if (get_post_meta(get_the_ID(), '_toilet_ensuite_3', true) == "Yes")
            $toilet_total++;
        if (get_post_meta(get_the_ID(), '_toilet_ensuite_4', true) == "Yes")
            $toilet_total++;
        if (get_post_meta(get_the_ID(), '_toilet_ensuite_5', true) == "Yes")
            $toilet_total++;
        if (get_post_meta(get_the_ID(), '_toilet_ensuite_6', true) == "Yes")
            $toilet_total++;
        if (get_post_meta(get_the_ID(), '_toilet_ensuite_7', true) == "Yes")
            $toilet_total++;
        if (get_post_meta(get_the_ID(), '_toilet_ensuite_8', true) == "Yes")
            $toilet_total++;


        $result[] = array(
            "id" => get_the_ID(),
            "title" => get_the_title(),
            "permalink" => get_permalink(),
            "tax" => wp_get_post_terms(get_the_ID(),'type',array( 'fields' => 'slugs')),
            "ppm" => get_post_meta(get_the_ID(), '_pcm', true),
            "wheel" => get_post_meta(get_the_ID(), '_Wheelchair', true),
            "ensuite" => get_post_meta(get_the_ID(), '_shared_shower', true),
            "parking" => get_post_meta(get_the_ID(), '_parking_bay', true),
            "reargarden" => get_post_meta(get_the_ID(), '_rear_garden', true),
            "shared_bath" => get_post_meta(get_the_ID(), '_shared_bath', true),
            "shared_toilet" => get_post_meta(get_the_ID(), '_shared_toilet', true),
            "postcode" => get_post_meta(get_the_ID(), '_postal_code', true),
            "address" => get_post_meta(get_the_ID(), '_address', true),
            "ukct" =>get_post_meta(get_the_ID(),'_kentt' , true),
            "cccut" =>get_post_meta(get_the_ID(),'_uclt' , true),
            "ucat" =>get_post_meta(get_the_ID(),'_ucat' , true),
            "citycentret" =>get_post_meta(get_the_ID(),'_canterburyt' , true),
            "ukc" =>get_post_meta(get_the_ID(),'_kent' , true),
            "cccu" =>get_post_meta(get_the_ID(),'_ucl' , true),
            "uca" =>get_post_meta(get_the_ID(),'_uca' , true),
            "citycentre" =>get_post_meta(get_the_ID(),'_canterbury' , true),
            "rooms" => $rooms,
            "driveway_space" => get_post_meta(get_the_ID(),'_driveway' , true),
            "pcm" =>get_post_meta(get_the_ID(),'_pcm' , true),
            "pcm_n" =>get_post_meta(get_the_ID(),'_pcm_n' , true),
            "marker" => $marker_url,
            "excerpt" => excerpt(40),
            "thumb" => $url,
            "toilet_tot" =>  $toilet_total,
            "terms" => get_the_terms(get_the_ID(), 'location', '', '-' ),
            "avail_this" => array (
                1 => get_post_meta(get_the_ID(), get_option('at_current_year').'_'.'1', true),
                2 => get_post_meta(get_the_ID(), get_option('at_current_year').'_'.'2', true),
                3 => get_post_meta(get_the_ID(), get_option('at_current_year').'_'.'3', true),
                4 => get_post_meta(get_the_ID(), get_option('at_current_year').'_'.'4', true),
                5 => get_post_meta(get_the_ID(), get_option('at_current_year').'_'.'5', true),
                6 => get_post_meta(get_the_ID(), get_option('at_current_year').'_'.'6', true),
                7 => get_post_meta(get_the_ID(), get_option('at_current_year').'_'.'7', true),
                ),
            "avail_next" => array (
                1 => get_post_meta(get_the_ID(), get_option('at_next_year').'_'.'1', true),
                2 => get_post_meta(get_the_ID(), get_option('at_next_year').'_'.'2', true),
                3 => get_post_meta(get_the_ID(), get_option('at_next_year').'_'.'3', true),
                4 => get_post_meta(get_the_ID(), get_option('at_next_year').'_'.'4', true),
                5 => get_post_meta(get_the_ID(), get_option('at_next_year').'_'.'5', true),
                6 => get_post_meta(get_the_ID(), get_option('at_next_year').'_'.'6', true),
                7 => get_post_meta(get_the_ID(), get_option('at_next_year').'_'.'7', true),
                ),
            "room_ppw" => array (
                1 => get_post_meta(get_the_ID(), '_rent_'.'1', true),
                2 => get_post_meta(get_the_ID(), '_rent_'.'2', true),
                3 => get_post_meta(get_the_ID(), '_rent_'.'3', true),
                4 => get_post_meta(get_the_ID(), '_rent_'.'4', true),
                5 => get_post_meta(get_the_ID(), '_rent_'.'5', true),
                6 => get_post_meta(get_the_ID(), '_rent_'.'6', true),
                7 => get_post_meta(get_the_ID(), '_rent_'.'7', true),
                )

            );
    }
    echo json_encode($result);
    wp_die();
}


add_action('wp_head','hook_facebook_meta');

function hook_facebook_meta() {

        global $post;
        $key='_gallery_images';
        $_value = get_post_meta($post->ID, $key, true);
        $attids=explode(',',$_value);
        $text = get_excerpt_by_id($post->ID); //$post_id is the post id of the desired post
        $image = wp_get_attachment_image_src($attids[0], 'large');
        $output='
        <meta property="og:description" content="'.$text.'" />
        <meta property="og:url" content="'.get_post_permalink($post->ID).'" />
        <meta name="twitter:card " content="'.$text.'" />
        <meta name="twitter:description " content="'.$text.'" />
        <meta name="twitter:url"content="'.get_post_permalink($post->ID).'" />';
        $post_type = get_post_type($post);
        if($post_type == "housing"){
            $output=$output.'<meta name="twitter:title " content="Check out this property for rent from Leydon Lettings!" />
            <meta property="og:title" content="Check out this property for rent from Leydon Lettings!" />
            <meta name="twitter:image" content="'.$image[0].'" />
            <meta property="og:image" content="'.$image[0].'" />';

        }
        else
        {
            $output=$output.'<meta name="twitter:title " content="'. get_the_title( $post->ID ).'" />
            <meta property="og:title" content="'. get_the_title( $post->ID ).'" />';
        }
        echo $output;
    
   

}

function get_excerpt_by_id($post_id){
    $the_post = get_post($post_id); //Gets post ID
    $the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
    $excerpt_length =25; //Sets excerpt length by word count
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);
    if(count($words) > $excerpt_length) :
    array_pop($words);
    array_push($words, '…');
    $the_excerpt = implode(' ', $words);
    endif;  
    return $the_excerpt;
}

add_image_size( 'custom-size', 130, 80 );
add_image_size( 'house-full', 821, 500 );

function posttype_search_filter($query) {
    $filter =  $_GET['post_type']; 
    if ($query->is_search) {
        $query->set('post_type', array($filter));
    }
    return $query;
}
add_filter( 'pre_get_posts', 'posttype_search_filter' );