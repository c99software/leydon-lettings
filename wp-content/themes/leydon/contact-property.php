<?php /* Template Name: Contact property Template  */ 
get_header(); ?>


<div class="container page_style">
	<div class="row">

	<script>

		jQuery("#enquiry-form").live('submit',function()
		{
		var str=jQuery("#enquiry-form").serialize();//alert(str);
		jQuery.post("<?php bloginfo('template_url'); ?>/docon.php",str ,function(data)
	        {
				console.log(data);
			if(data=='success') //if correct login detail
			  {//alert('hi');
			  	jQuery("#msg").fadeTo(200,0.1,function()  //start fading the messagebox
				{ 
				  //add message and change the class of the box and start fading
				  jQuery(this).html('Your enquiry has been submitted.').fadeTo(1200,1,
	              function()
				  {
						document.location='/tenants-prospective-tenants-contact-form/';
						
				  });
				});
			  }
			  else 
				{
		  	jQuery("#msg").fadeTo(200,0.1,function() //start fading the messagebox
				{ 
				  //add message and change the class of the box and start fading
				  jQuery(this).html(data).fadeTo(1200,1);
				});		
	          }
				//$('#log').html(data);
	        });
			return false; //not to post the  form physically
			});


	</script>
	<script type="text/javascript">
	jQuery(function(){
		jQuery('#property1').change(function(){
			jQuery.post('<?php bloginfo('template_url') ?>/ajax.php',{data:jQuery(this).val()},function(response){
				jQuery('#time1').html(response);
				})
			})
			jQuery('#property2').change(function(){
			jQuery.post('<?php bloginfo('template_url') ?>/ajax.php',{data:jQuery(this).val()},function(response){
				jQuery('#time2').html(response);
				})
			})
			jQuery('#property3').change(function(){
			jQuery.post('<?php bloginfo('template_url') ?>/ajax.php',{data:jQuery(this).val()},function(response){
				jQuery('#time3').html(response);
				})
			})
		})
	</script>
	<div class="col-xs-12 col-sm-9">

	<?php 
	$_varray=array('Monday 12 till 2','Monday 3 till 5','Tuesday 12 till 2','Tuesday 3 till 5','Wednesday 12 till 2','Wednesday 3 till 5','Thursday 12 till 2','Thursday 3 till 5','Friday 12 till 2','Friday 3 till 5');
	 ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<h4 class="innerpagehead"><?php the_title(); ?></h4>
						<div class="entry-content">
							<div class="wpcf7" id="wpcf7-f407-p401-o1">
		<form action="/tenants-prospective-tenants-contact-form" method="post" id="enquiry-form" class="wpcf7-form" novalidate="novalidate">
			
			<p>
				Name<span class="required">*</span><br>
				<span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true"></span>
			</p>
			<p>
				Email<span class="required">*</span><br>
				<span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true"></span>
			</p>
	        <p>
				How many are in your group?<span class="required">*</span><br>
				<span class="wpcf7-form-control-wrap your-email">
	            <select  style="height: 30px;width: 270px;"  name="howmany" class="wpcf7-form-control wpcf7-select">
					<option value="Any">Any</option>
	                <?php for($i=0;$i<12;$i++) { ?>
	                   <option value="<?php echo $i ?>"><?php echo $i ?></option>
	                <?php } ?>
	              
	                  
				</select>
	          
	            </span>
			</p>
	        <p>
				How many rooms do you require<br>
				<span class="wpcf7-form-control-wrap your-email">
	            <select  style="height: 30px;width: 270px;"  name="roomsrequire" class="wpcf7-form-control wpcf7-select">
					<option value="Any">Any</option>
	                <?php for($i=0;$i<12;$i++) { ?>
	                   <option value="<?php echo $i ?>"><?php echo $i ?></option>
	                <?php } ?>
	              
	                  
				</select>
	          
	            </span>
			</p>
	         <p>
				Where your group number is different from your <br>room number please explain? E.g room share*<br>
				<span class="wpcf7-form-control-wrap your-email">
	  
	          <textarea name="why" cols="40" rows="3" class="wpcf7-form-control wpcf7-textarea"></textarea>
	            </span>
			</p>
			<p>
				Which University do you attend?<span class="required">*</span><br>
				<span class="wpcf7-form-control-wrap Location">
				<select  style="height: 30px;width: 270px;"  name="Location" id="location" class="wpcf7-form-control wpcf7-select">
					<option value="Any">Any</option>
	                 <option value="Canterbury Christ Church">Canterbury Christ Church (CCCU)</option>
	                  <option value="Kent University">Kent University (UKC)</option>
	              	  <option value="UCA Caterbury">UCA Caterbury (UCA)</option>    
				      <option value="Canterbury City Center">Canterbury City Center</option>
				</select>
				</span>
			</p>
	        <p>
			  Where did you find us?<br>
				<span class="wpcf7-form-control-wrap Status">
					<span class="wpcf7-form-control wpcf7-checkbox">
	            		<span class="wpcf7-list-item">
	            			<input type="checkbox" name="find[]" value="Social Networking">&nbsp;<span class="wpcf7-list-item-label">Social Networking</span>
					    </span>
	            		
					  	<span class="wpcf7-list-item">
	            			<input type="checkbox" name="find[]" value="Word of Mouth">&nbsp;<span class="wpcf7-list-item-label">Word of Mouth</span>
					    </span>
	            		
					  	<span class="wpcf7-list-item">
	           				<input type="checkbox" name="find[]" value="Search Engine">&nbsp;<span class="wpcf7-list-item-label">Search Engine</span>
					  	</span>
						
					  <br>
					  
					  <span class="wpcf7-list-item">
	           				<input type="checkbox" name="find[]" value="Property Sites">&nbsp;<span class="wpcf7-list-item-label">Property Sites</span>
					  	</span>
					  
	   				  	<span class="wpcf7-list-item">
	           				<input type="checkbox" name="find[]" value="Office Window">&nbsp;<span class="wpcf7-list-item-label">Office Window</span>
					  	</span>
						
					  	<span class="wpcf7-list-item">
	           				<input type="checkbox" name="find[]" value="Other">&nbsp;<span class="wpcf7-list-item-label">Other</span>
					  	</span>
					  
				  </span>
	            </span>
			</p>
			 <p>
				If you ticked 'other' please say why...<br>
				<span class="wpcf7-form-control-wrap your-email">
	  
	          <textarea name="why1" cols="40" rows="3" class="wpcf7-form-control wpcf7-textarea"></textarea>
	            </span>
			</p>
	        <p>
				Preferred Property 1<span class="required">*</span><br>
				<span class="wpcf7-form-control-wrap Location">
				<select style="height: 30px;width: 270px;" id="property1" name="Property1" class="wpcf7-form-control wpcf7-select">
					<option value="Any">Any</option>
	                <?php
					// Get the 'Profiles' post type
					$args = array(
						'post_type' => 'housing',
						'post_status '=>'publish'
					);
					$loop = new WP_Query($args);
					
					while($loop->have_posts()): $loop->the_post();
					?>
	                 <option value="<?php echo the_ID(); ?>"><?php echo the_title(); ?></option> 
	                <?php
					
					
					endwhile;
					wp_reset_query();
					?>
	               
					
				</select>
				</span>
			</p>
	        <p>
				Select Viewing Time<span class="required">*</span><br>
				<span class="wpcf7-form-control-wrap Location">
				<select style="height: 30px;width: 270px;" id="time1" name="Time1" class="wpcf7-form-control wpcf7-select">
					<option value="Any">Any</option>
	                 <?php for($k=0;$k<count($_varray);$k++) { ?>
	                   <option value="<?php echo $_varray[$k] ?>"><?php echo $_varray[$k] ?></option>
	                <?php } ?>
				</select>
				</span>
			</p>
	          <p>
				Preferred Property 2<br>
				<span class="wpcf7-form-control-wrap Location">
				<select style="height: 30px;width: 270px;" id="property2" name="Property2" class="wpcf7-form-control wpcf7-select">
					<option value="Any">Any</option>
	                <?php
					// Get the 'Profiles' post type
					$args = array(
						'post_type' => 'housing',
						'post_status '=>'publish'
					);
					$loop = new WP_Query($args);
					
					while($loop->have_posts()): $loop->the_post();
					?>
	                 <option value="<?php echo the_ID(); ?>"><?php echo the_title(); ?></option> 
	                <?php
					
					
					endwhile;
					wp_reset_query();
					?>
	               
					
				</select>
				</span>
			</p>
	        <p>
				Select Viewing Time<br>
				<span class="wpcf7-form-control-wrap Location">
				<select style="height: 30px;width: 270px;" id="time2" name="Time2" class="wpcf7-form-control wpcf7-select">
					<option value="Any">Any</option>
	                 <?php for($k=0;$k<count($_varray);$k++) { ?>
	                   <option value="<?php echo $_varray[$k] ?>"><?php echo $_varray[$k] ?></option>
	                <?php } ?>
				</select>
				</span>
			</p>
	          <p>
				Preferred Property 3<br>
				<span class="wpcf7-form-control-wrap Location">
				<select style="height: 30px;width: 270px;" id="property3" name="Property3" class="wpcf7-form-control wpcf7-select">
					<option value="Any">Any</option>
	                <?php
					// Get the 'Profiles' post type
					$args = array(
						'post_type' => 'housing',
						'post_status '=>'publish'
					);
					$loop = new WP_Query($args);
					
					while($loop->have_posts()): $loop->the_post();
					?>
	                 <option value="<?php echo the_ID(); ?>"><?php echo the_title(); ?></option> 
	                <?php
					
					
					endwhile;
					wp_reset_query();
					?>
	               
					
				</select>
				</span>
			</p>
	        <p>
				Select Viewing Time<br>
				<span class="wpcf7-form-control-wrap Location">
				<select style="height: 30px;width: 270px;" id="time3" name="Time3" class="wpcf7-form-control wpcf7-select">
					<option value="Any">Any</option>
	                 <?php for($k=0;$k<count($_varray);$k++) { ?>
	                   <option value="<?php echo $_varray[$k] ?>"><?php echo $_varray[$k] ?></option>
	                <?php } ?>
				</select>
				</span>
			</p>
			
			
			<p>
				<input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit">
			</p>
			<div class="" id="msg">
			</div>
		</form>
	</div>
							<div class="contentinner2">
	<div class="advhead"></div><!--advhead-->
	<div class="advbg">
	<img src="<?php echo get_template_directory_uri(); ?>/images/advimg.png">
	<p class="adv24">Thank You</p>
	<p class="adv16" style="padding: 10px;">For contacting Leydon Lettings, Canterburys leading student lettings agency
	</p>
	</div><!--advbg-->
	</div>
						</div><!-- .entry-content -->

						
					</article><!-- #post -->

					
				<?php endwhile; ?>

	</div><!--innerpage-->

	</div><!--contentallign-->
</div><!--contentallign-->


<?php get_footer();?></div><!--content-->