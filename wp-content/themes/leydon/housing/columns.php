<?php
function add_housing_columns($columns) {
	 unset($columns['date']);
    return array_merge($columns,
              array('_postal_code' => __('Postal Code'),
			  		'_rooms' => __('Rooms')
			  ));
}
add_filter('manage_housing_posts_columns' , 'add_housing_columns');
function custom_housing_column( $column, $post_id ) {
	global $post;
	$status=get_post_status( $post_id );	
    switch ( $column ) {
      case '_postal_code':
	  if(get_post_meta($post_id, '_postal_code', true)){ echo get_post_meta($post_id, '_postal_code', true);}
        break;
		 case '_rooms':
	  if(get_post_meta($post_id, '_rooms', true)){ echo get_post_meta($post_id, '_rooms', true);}
        break;
			 case '_availability_this_year':
	  if(get_post_meta($post_id, '_availability_this_year', true)){ echo get_post_meta($post_id, '_availability_this_year', true);}
        break;
			 case '_availability_next_year':
	  if(get_post_meta($post_id, '_availability_next_year', true)){ echo get_post_meta($post_id, '_availability_next_year', true);}
        break;
    }
}
add_action( 'manage_housing_posts_custom_column' , 'custom_housing_column' , 10, 2  );
/*add_action( 'admin_menu', 'myprefix_remove_meta_box');
function myprefix_remove_meta_box(){
   remove_meta_box('locationdiv', 'housing', 'side');
}
add_action( 'add_meta_boxes', 'myprefix_add_meta_box');  
 function myprefix_add_meta_box() {  
     add_meta_box( 'location_id', 'Location','myprefix_mytaxonomy_metabox','housing' ,'side','core');  
 } 

function myprefix_mytaxonomy_metabox( $post ) {
    //Get taxonomy and terms
    $taxonomy = 'location';

    //Set up the taxonomy object and get terms
    $tax = get_taxonomy($taxonomy);
    $terms = get_terms($taxonomy,array('hide_empty' => 0));

    //Name of the form
    $name = 'tax_input[' . $taxonomy . ']';

    //Get current and popular terms
    $popular = get_terms( $taxonomy, array( 'orderby' => 'count', 'order' => 'DESC', 'number' => 10, 'hierarchical' => false ) );
    $postterms = get_the_terms( $post->ID,$taxonomy );
    $current = ($postterms ? array_pop($postterms) : false);
    $current = ($current ? $current->term_id : 0);
    ?>

    <div id="taxonomy-<?php echo $taxonomy; ?>" class="categorydiv">

        <!-- Display tabs-->
        <ul id="<?php echo $taxonomy; ?>-tabs" class="category-tabs">
            <li class="tabs"><a href="#<?php echo $taxonomy; ?>-all" tabindex="3"><?php echo $tax->labels->all_items; ?></a></li>
            <li class="hide-if-no-js"><a href="#<?php echo $taxonomy; ?>-pop" tabindex="3"><?php _e( 'Most Used' ); ?></a></li>
        </ul>

        <!-- Display taxonomy terms -->
        <div id="<?php echo $taxonomy; ?>-all" class="tabs-panel">
            <ul id="<?php echo $taxonomy; ?>checklist" class="list:<?php echo $taxonomy?> categorychecklist form-no-clear">
                <?php   foreach($terms as $term){
                    $id = $taxonomy.'-'.$term->term_id;
                    echo "<li id='$id'><label class='selectit'>";
                    echo "<input type='radio' id='in-$id' name='{$name}'".checked($current,$term->term_id,false)."value='$term->term_id' />$term->name<br />";
                   echo "</label></li>";
                }?>
           </ul>
        </div>

        <!-- Display popular taxonomy terms -->
        <div id="<?php echo $taxonomy; ?>-pop" class="tabs-panel" style="display: none;">
            <ul id="<?php echo $taxonomy; ?>checklist-pop" class="categorychecklist form-no-clear" >
                <?php   foreach($popular as $term){
                    $id = 'popular-'.$taxonomy.'-'.$term->term_id;
                    echo "<li id='$id'><label class='selectit'>";
                    echo "<input type='radio' id='in-$id'".checked($current,$term->term_id,false)."value='$term->term_id' />$term->name<br />";
                    echo "</label></li>";
                }?>
           </ul>
       </div>

    </div>
    <?php
}
*/
?>