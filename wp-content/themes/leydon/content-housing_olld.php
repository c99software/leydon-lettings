 <?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */


?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/housing/scripts/jquery.bxslider.css" type="text/css" />
<script src="<?php bloginfo('template_url') ?>/housing/scripts/jquery.bxslider.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.bxslider').bxSlider({
            pagerCustom: '#bx-pager'
        });

        jQuery( "#tabs" ).tabs();
    });
    var geocoder;
    var map;
    var map2;
    var loc;
    var latlng
    jQuery(function(){
        geocoder = new google.maps.Geocoder();
        latlng = new google.maps.LatLng(51.198427,1.082313);
        var mapOptions = {
            zoom: 14,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById('mapviews'), mapOptions);
        map2 = new google.maps.Map(document.getElementById('mapviewsxs'), mapOptions);
        var mapFirstClick = false;
        $("#maptab").click(function() {
            mapFirstClick || setTimeout(function() {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(loc);
                mapFirstClick = true;
            }, 250);
        });

    })
    google.maps.event.addListener(map, 'zoom_changed', function() {
        zoomLevel = map.getZoom();
        //this is where you will do your icon height and width change.     
      });
    function addmap(adddress,info)
    {

        //var address = document.getElementById('address').value;
        geocoder.geocode( { 'address': adddress}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                google.maps.event.trigger(map, 'resize');
                google.maps.event.trigger(map2, 'resize');
                loc=results[0].geometry.location;
                map.setCenter(results[0].geometry.location);
                map2.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
                var marker2 = new google.maps.Marker({
                    map: map2,
                    position: results[0].geometry.location
                });

                var infowindow = new google.maps.InfoWindow({});
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.setContent(info);
                    infowindow.open(map, this);
                });
                google.maps.event.addListener(marker2, 'click', function() {
                    infowindow.setContent(info);
                    infowindow.open(map2, this);
                });
            } else {
                console.log('Geocode was not successful for the following reason: ' + status);
            }
        });
    }
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery("#single").fancybox();
        jQuery("#single-1").fancybox();
    });
</script>

<script type="text/javascript">
    jQuery("#enquiry-form").live('submit',function()
    {
        var str=jQuery("#enquiry-form").serialize();//alert(str);
        jQuery.post("<?php bloginfo('template_url'); ?>/doenquiry.php",str ,function(data)
        {
            if(data=='success') //if correct login detail
            {//alert('hi');
                jQuery("#msg").fadeTo(200,0.1,function()  //start fading the messagebox
                {
                    //add message and change the class of the box and start fading
                    jQuery(this).html('Thank you.').fadeTo(1200,1,
                        function()
                        {

                            jQuery.fancybox.close();
                        });
                });
            }
            else
            {
                jQuery("#msg").fadeTo(200,0.1,function() //start fading the messagebox
                {
                    //add message and change the class of the box and start fading
                    jQuery(this).html(data).fadeTo(1200,1);
                });
            }
            //$('#log').html(data);
        });
        return false; //not to post the  form physically
    });
</script>

<div class="subpage-housing hidden-xs hidden-sm">

    <div id="addthis-wrapper"><!-- AddThis Button BEGIN -->
        <h5>Sharing is caring</h5>
        <p>
            <span class='st_facebook'></span>
            <span class='st_twitter' st_via='leydonlettings' st_username='leydonlettings'></span>
            <span class='st_linkedin'></span>
            <span class='st_googleplus'></span>
            <span class='st_pinterest'></span>
            <span class='st_reddit'></span>
            <span class='st_stumbleupon'></span>
            <span class='st_email'></span>
            <span class='st_sharethis'></span>
        </p>
    </div><!-- AddThis Button END -->

    <div class="housing-details">
        <?php
        echo '<h5>House Details</h5>';

        // do something with $variable

            $parent_guaran = get_post_meta(get_the_ID(),'_guarantor' , true); 
            $deposit = get_post_meta(get_the_ID(),'_deposit' , true); 
            $deposit_amount = get_post_meta(get_the_ID(),'_deposit_amount' , true); 
            $non_ref_admin = get_post_meta(get_the_ID(),'_nonrefundfee' , true); 
            $rooms = get_post_meta(get_the_ID(),'_rooms' , true); 
            $max_students = get_post_meta(get_the_ID(),'_maxstudents' , true); 
            $contract_type = get_post_meta(get_the_ID(),'_contracttype' , true); 

            if($deposit == 'Required' && $deposit_amount != null){
                $deposit_print = $deposit."(&pound;".$deposit_amount.")";
            }
            else
            {
                $deposit_print = $deposit;
            }
            echo "<p class='guarantor'>Parent Guarantor: ".$parent_guaran."</p>";
            echo "<p class='deposit'>Deposit: ". $deposit_print ."</p>";
            echo "<p class='admin-fee'>Non-refund admin fee: ".$non_ref_admin."</p>";
            echo "<p class='rooms'>Rooms: ".$rooms."</p>";
            echo "<p class='max-students'>Max Students: ".$max_students."</p>";
            echo "<p class='contract-type'>Contract Type: ".$contract_type."</p>";
         

        echo '<h5>When can I arrange a viewing?</h5>';
        $fieldswer=array(
            array('label'=>'Viewing Times','type'=>'select','name'=>'_vtime1','options'=>$_varray),
            array('label'=>'Viewing Times','type'=>'select','name'=>'_vtime2','options'=>$_varray),
        );

        print_fields_front($fieldswer,get_the_ID());

        /* echo '<h5>Where is it?</h5>';
           $fieldswe=array(
           array('label'=>'CCCU - 10mins','type'=>'text','name'=>'_ucl','after'=>''),
           array('label'=>'UKC - 10mins','type'=>'text','name'=>'_kent','after'=>''),
           array('label'=>'UCA - 10mins','type'=>'text','name'=>'_uca','after'=>''),
           array('label'=>'City Centre - 10mins','type'=>'text','name'=>'_canterbury','after'=>''),
           );

           print_fields_front($fieldswe,get_the_ID());
         */
        ?>
        <?php
        if(!empty($_GET['locations']))
        {
            $loc=$_GET['locations'];
        }
        else
        {
            $loc='_canterbury';
        }

        if($loc=='_kent'){ $loce='UKC';}
        elseif($loc=='_ucl'){ $loce='CCCU';}
        elseif($loc=='_uca'){ $loce='UCA';}
        elseif($loc=='_canterbury'){ $loce='Canterbury';}


        //$distance= get_post_meta(get_the_ID(),'_kent' , true);
        //$distance= get_post_meta(get_the_ID(),$loc , true);
        echo "<h5 class=\"distanceheader\">Where is it?</h5>";
        ?>
        <p><?php  ?>UKC  -  <?php  echo get_post_meta(get_the_ID(),'_kentt' , true) ?> <span class="distancemiles">(<?php  echo get_post_meta(get_the_ID(),'_kent' , true) ?>)</span></p>
        <p><?php  ?>CCCU  -  <?php echo get_post_meta(get_the_ID(),'_uclt' , true) ?> <span class="distancemiles">(<?php echo  get_post_meta(get_the_ID(),'_ucl' , true) ?>)</span></p>
        <p><?php  ?>UCA Canterbury  -  <?php  echo get_post_meta(get_the_ID(),'_ucat' , true) ?> <span class="distancemiles">(<?php  echo get_post_meta(get_the_ID(),'_uca' , true) ?>)</span></p>
        <p><?php  ?>Canterbury Center  -  <?php  echo get_post_meta(get_the_ID(),'_canterburyt' , true) ?> <span class="distancemiles">(<?php  echo get_post_meta(get_the_ID(),'_canterbury' , true) ?>)</span></p>
    </div><!-- #HousingDetials END -->

    <div class="griddistance">


    </div><!-- #GridDistance END-->

    <?php
    $_address = get_post_meta(get_the_ID(), '_address', true);
    $_postal_code = get_post_meta(get_the_ID(), '_postal_code', true);
    if($_address){
        echo ' <div id="mapviews" class="mapfront" ></div>';
        echo '<script type="text/javascript">jQuery(function(){
			addmap("'.$_address.', '.$_postal_code.'","'.$_address.', '.$_postal_code.'");
		})</script>';
    }
    ?>

    <div class="usefullinfo">
        <h4>Useful Information</h4>

        <?php wp_nav_menu( array('menu' => 'usefulinfo','menu_id'=> 'usefulinfo' )); ?>
    </div><!-- #UsefullInfo END -->

</div><!-- #SubPage Housing END -->
<?php $_room_count = get_post_meta(get_the_ID(), '_rooms', true); ?>
<div class="innerpage room_count<?php echo $_room_count;?> ">
<div id="housing-features">
    <?php
    $_wheel = get_post_meta(get_the_ID(), '_Wheelchair', true);
    $_shower = get_post_meta(get_the_ID(), '_en_suite', true);
    $_bath = get_post_meta(get_the_ID(), '_shared_bath', true);
    $_toilet = get_post_meta(get_the_ID(), '_shared_toilet', true);
    $_parking = get_post_meta(get_the_ID(), '_parking_bay', true);
    $_garden = get_post_meta(get_the_ID(), '_rear_garden', true);
    ?>
    <?php if($_bath!='' && $_bath != '0') { ?>
        <div title="Bath" class="wrapper-housing-icon icon view9_r active"><div class="ccount"><?php echo $_bath; ?></div></div>
    <?php } ?>
    <?php if($_toilet!='' && $_toilet != '0') { ?>
        <div title="Toilet" class="wrapper-housing-icon icon view10_r active"><div class="ccount"><?php echo $_toilet; ?></div></div>
    <?php } ?>
    <?php if($_shower!='') { ?>
        <div title="En Suite" class="wrapper-housing-icon icon icon view6_r active">
        <div class="ccount"><?php echo $_shower; ?></div>
        </div><?php } ?>
    <?php if($_wheel!='' && $_wheel != 'No' ) { ?>
        <div title="Wheelchair Friendly" class="wrapper-housing-icon icon view5_r active">
    </div><?php } ?>
    
    <?php if($_parking!='') { ?>
        <div title="Parking" class="wrapper-housing-icon icon view7_r active">

        </div><?php } ?>
    <?php if($_garden!=''&& $_garden!='No') { ?>
        <div title="Garden" class="wrapper-housing-icon icon view8_r active">

        </div><?php } ?>
    
     
</div>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
<header class="entry-header">
    <h1 class="innerpagehead-housing"><?php  the_title(); ?></h1>
    <div class="entry-meta">    
        <?php $terms = wp_get_post_terms( $post->ID, 'location' ); ?>
        <?php foreach($terms as $key => $term): ?>
            <?php $term_link = $term->slug; ?>
            <?php if($key != 0): ?>
               &#8226
            <?php endif; ?>
            <a rel="tag" href="/find-properties/?loc=<?php echo $term_link ?>"><?php echo $term->name; ?> </a>
        <?php endforeach; ?>
    </div><!-- #entry meta END -->
</header>

<script type="text/javascript">
    var addthis_config = addthis_config||{};
    addthis_config.data_track_addressbar = false;
    addthis_config.data_track_clickback = false;
</script>

<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51d4476837e39e46"></script>

<div id="tabs">
<ul>
    <li><a href="#tabs-1">Main Details</a></li>
    <li><a href="#tabs-2">House Information</a></li>
    <li><a href="#tabs-3">Room Details</a></li>
    <li><a href="#tabs-4">Floor Plan</a></li>
    <li><a href="#tabs-5">Certificates</a></li>
    <li id="emergency-tab"><a href="#tabs-6">Emergency</a></li>
</ul>
<div id="tabs-1">

<div class="sliderhousing">
    <?php
    $key='_gallery_images';
    $_value = get_post_meta($id, $key, true);
    $attids=explode(',',$_value);
    ?>
    <!--
    <ul class="bxslider">
        <?php

        if(count($attids)>0)
        {
            foreach($attids as $attid){
                if(!empty($attid)){
                    if(is_numeric($attid)){
                        $image = wp_get_attachment_image_src($attid, 'large');
                        $alt = get_post_meta($attid , '_wp_attachment_image_alt', true);

                        if(!empty($image))
                            ?>
                            <li><img src="<?php echo $image[0] ?>" alt="<?php echo $alt; ?>" title="<?php echo $alt; ?>"/></li>
                    <?php

                    }
                    else
                    {
                        ?>
                        <li><img src="<?php echo $attid ?>" alt="<?php echo $alt; ?>" title="<?php echo $alt; ?>"/></li>
                    <?php
                    }
                }
            }
        }
        ?>
    </ul>
    -->
    <?php $main_image = ""; ?>
    <div id="carousel-house" class="carousel" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <?php

            if(count($attids)>0)
            {
                $k=0;
                foreach($attids as $attid){
                    if(!empty($attid)){
                        if(is_numeric($attid)){
                            $image = wp_get_attachment_image_src($attid, 'large');
                            $alt = get_post_meta($attid , '_wp_attachment_image_alt', true);

                            if(!empty($image))
                                ?>
            <div class="item <?php echo ($k==0)?'active':'';?>"><img src="<?php echo $image[0] ?>" alt="<?php echo $alt; ?>" title="<?php echo $alt; ?>"/></div>

                        <?php
                            $main_image = $image[0];
                        }
                        else
                        {
                            ?>
            <div class="item <?php echo ($k==0)?'active':'';?>"><img src="<?php echo $attid ?>" alt="<?php echo $alt; ?>" title="<?php echo $alt; ?>"/></div>
                        <?php
                        }
                    }
                    $k++;
                }
            }
            ?>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-house" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#carousel-house" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>


    <div class="text-center">
        <div class="contactdetails">
            <a class="nbtn" id="single" style="float: left; margin: 0px 20px 0px 0px; border-radius: 0px 10px 0px 10px;" href="<?php bloginfo('template_url'); ?>/enquiry.php?pr=<?php the_title(); ?>&amp;prid=<?php the_ID(); ?>&amp;name=Arrange a Viewing">Arrange a Viewing</a>
            <a class="nbtn" id="single-1" style="float: left; border-radius: 0px 10px 0px 10px;" href="<?php bloginfo('template_url'); ?>/enquiry.php?pr=<?php the_title(); ?>&amp;name=Request More Details">Request More Details</a>
        </div><!-- #ContactDetails END -->
    </div>

    <div class="c-pager">
        <ul class="">
            <?php
            if(count($attids)>0)
            {
                $k=0;
                foreach($attids as $attid){
                    if(!empty($attid)){
                        if(is_numeric($attid)){
                            $image = wp_get_attachment_image_src($attid, 'thumbnail');
                            $alt = get_post_meta($attid , '_wp_attachment_image_alt', true);
                            //$immeta = wp_get_attachment_metadata( $attid);
                            //print_r($immeta);
                            if(!empty($image))
                                ?>
                                <li data-target="#carousel-house" data-slide-to="<?php echo $k ?>" <?php echo ($k==0)?'class="active"':'';?>>
                            <img src="<?php echo $image[0] ?>" width="<?php echo($image[1]); ?>" height="<?php echo($image[2]); ?>" alt="<?php echo $alt; ?>" title="<?php echo $alt ?>" />
                            </li>
                        <?php

                        }
                        else
                        {
                            ?>
                            <li data-target="#carousel-house" data-slide-to="<?php echo $k ?>" <?php echo ($k==0)?'class="active"':'';?>>
                                <img src="<?php echo $attid ?>" width="<?php echo($image[1]); ?>" height="<?php echo($image[2]); ?>" alt="<?php echo $alt; ?>" title="<?php echo $alt; ?>" />
                            </li>

                        <?php
                        }
                    }

                    $k++;
                }

            }
            ?>
        </ul>
    </div>
    <!--
    <div id="bx-pager">
        <?php
        if(count($attids)>0)
        {
            $k=0;
            foreach($attids as $attid){
                if(!empty($attid)){
                    if(is_numeric($attid)){
                        $image = wp_get_attachment_image_src($attid, 'thumbnail');
                        $alt = get_post_meta($attid , '_wp_attachment_image_alt', true);
                        //$immeta = wp_get_attachment_metadata( $attid);
                        //print_r($immeta);
                        if(!empty($image))
                            ?>
                            <a data-slide-index="<?php echo $k ?>" href="">
                        <img src="<?php echo $image[0] ?>" width="<?php echo($image[1]); ?>" height="<?php echo($image[2]); ?>" alt="<?php echo $alt; ?>" title="<?php echo $alt ?>" />
                        </a>
                    <?php

                    }
                    else
                    {
                        ?>
                        <a data-slide-index="<?php echo $k ?>" href="">
                            <img src="<?php echo $attid ?>" width="<?php echo($image[1]); ?>" height="<?php echo($image[2]); ?>" alt="<?php echo $alt; ?>" title="<?php echo $alt; ?>" />
                        </a>

                    <?php
                    }
                }

                $k++;
            }

        }
        ?>
    </div>--><!-- #BxPager END -->
</div><!-- #SliderHousing END -->


<div class="visible-xs visible-sm">
    <div class="clearBoth"></div>
    <div style="width: 50%;float: left">
        <?php
        $_address = get_post_meta(get_the_ID(), '_address', true);
        $_postal_code = get_post_meta(get_the_ID(), '_postal_code', true);
        if($_address){
            echo ' <div id="mapviewsxs" class="mapfront" ></div>';
            echo '<script type="text/javascript">jQuery(function(){
			addmap("'.$_address.', '.$_postal_code.'","'.$_address.', '.$_postal_code.'");
		})</script>';
        } ?>
        <div id="addthis-wrapper" class="visible-xs" style="padding: 0;"><!-- AddThis Button BEGIN -->
            <h5>Sharing is caring</h5>
            <p>
                <span class='st_facebook'></span>
                <span class='st_twitter' st_via='leydonlettings' st_username='leydonlettings'></span>
                <span class='st_linkedin'></span>
                <span class='st_googleplus'></span>
                <span class='st_pinterest'></span>
                <span class='st_reddit'></span>
                <span class='st_stumbleupon'></span>
                <span class='st_email'></span>
                <span class='st_sharethis'></span>
            </p>
        </div><!-- AddThis Button END -->
    </div>
    <div style="width: 50%;float: left">
        <div class="housing-details" style="margin-top: 0; padding-top: 0;">
            <?php
            echo '<h5 style="margin-top: 0;">House Details</h5>';

            $parent_guaran = get_post_meta(get_the_ID(),'_guarantor' , true); 
            $deposit = get_post_meta(get_the_ID(),'_deposit' , true); 
            $deposit_amount = get_post_meta(get_the_ID(),'_deposit_amount' , true); 
            $non_ref_admin = get_post_meta(get_the_ID(),'_nonrefundfee' , true); 
            $rooms = get_post_meta(get_the_ID(),'_rooms' , true); 
            $max_students = get_post_meta(get_the_ID(),'_maxstudents' , true); 
            $contract_type = get_post_meta(get_the_ID(),'_contracttype' , true); 

            if($deposit == 'Required' && $deposit_amount != null){
                $deposit_print = $deposit."(&pound;".$deposit_amount.")";
            }
            else
            {
                $deposit_print = $deposit;
            }
            echo "<p class='guarantor'>Parent Guarantor: ".$parent_guaran."</p>";
            echo "<p class='deposit'>Deposit: ". $deposit_print ."</p>";
            echo "<p class='admin-fee'>Non-refund admin fee: ".$non_ref_admin."</p>";
            echo "<p class='rooms'>Rooms: ".$rooms."</p>";
            echo "<p class='max-students'>Max Students: ".$max_students."</p>";
            echo "<p class='contract-type'>Contract Type: ".$contract_type."</p>";


            echo '<h5>When can I arrange a viewing?</h5>';
            $fieldswer=array(
                array('label'=>'Viewing Times','type'=>'select','name'=>'_vtime1','options'=>$_varray),
                array('label'=>'Viewing Times','type'=>'select','name'=>'_vtime2','options'=>$_varray),
            );

            print_fields_front($fieldswer,get_the_ID());

            /* echo '<h5>Where is it?</h5>';
               $fieldswe=array(
               array('label'=>'CCCU - 10mins','type'=>'text','name'=>'_ucl','after'=>''),
               array('label'=>'UKC - 10mins','type'=>'text','name'=>'_kent','after'=>''),
               array('label'=>'UCA - 10mins','type'=>'text','name'=>'_uca','after'=>''),
               array('label'=>'City Centre - 10mins','type'=>'text','name'=>'_canterbury','after'=>''),
               );

               print_fields_front($fieldswe,get_the_ID());
             */
            ?>
            <?php
            if(!empty($_GET['locations']))
            {
                $loc=$_GET['locations'];
            }
            else
            {
                $loc='_canterbury';
            }

            if($loc=='_kent'){ $loce='UKC';}
            elseif($loc=='_ucl'){ $loce='CCCU';}
            elseif($loc=='_uca'){ $loce='UCA';}
            elseif($loc=='_canterbury'){ $loce='Canterbury';}


            //$distance= get_post_meta(get_the_ID(),'_kent' , true);
            //$distance= get_post_meta(get_the_ID(),$loc , true);
            echo "<h5 class=\"distanceheader\">Where is it?</h5>";
            ?>
            <p><?php  ?>UKC  -  <?php  echo get_post_meta(get_the_ID(),'_kentt' , true) ?> <span class="distancemiles">(<?php  echo get_post_meta(get_the_ID(),'_kent' , true) ?>)</span></p>
            <p><?php  ?>CCCU  -  <?php echo get_post_meta(get_the_ID(),'_uclt' , true) ?> <span class="distancemiles">(<?php echo  get_post_meta(get_the_ID(),'_ucl' , true) ?>)</span></p>
            <p><?php  ?>UCA Canterbury  -  <?php  echo get_post_meta(get_the_ID(),'_ucat' , true) ?> <span class="distancemiles">(<?php  echo get_post_meta(get_the_ID(),'_uca' , true) ?>)</span></p>
            <p><?php  ?>Canterbury Center  -  <?php  echo get_post_meta(get_the_ID(),'_canterburyt' , true) ?> <span class="distancemiles">(<?php  echo get_post_meta(get_the_ID(),'_canterbury' , true) ?>)</span></p>
        </div><!-- #HousingDetials END -->

    </div>
    <div class="clearBoth"></div>
    <hr/>
</div>
<?php
echo '<h4 class="title htitle">Description</h4>';
echo '<div class="binfo ">';
the_content();
echo '</div>';
?>
<div class="quote_slider">
    <ul class="bxslider2"> 
        <li>
            <p class="quote">Dear Bob, Many thanks for the very comprehensive guidance. <br />

The advice in your email as well as the advice on your website were both very helpful in making the final decision. <br />

Indeed, the reason why I contacted your letting agency before any other...</p>
            <p class="author"> W. N. (20/06/2013) </p>
        </li>

         <li>
            <p class="quote">Thanks for this Bob.

Being proactive, and ensuring tenants know how they can help reduce any risk, is something that we are also aiming to do, rather than just being reactive, and finding any issues once they are established.</p>
            <p class="author"> Phil (22/12/13)</p>
        </li>

         <li>
            <p class="quote">Hello, I was just e-mailing to say thanks for the prompt replacement and maintenance of our heating, myself and everyone else at 23 College Road really appreciate it.</p>
            <p class="author"> Richard (13/02/2013)</p>
        </li>

         <li>
            <p class="quote">Hello Bob We want to thank you SO much for going to our property at xxxxxxxx Road and assessing its suitability as a student rental. You gave us such an incredibly detailed report, we can see you are passionate about property and have lots of ideas on how to improve it.

<br /><br />Your assessment has certainly helped us make a decision about what we should do we really appreciated your input so much, and thank you for it.</p>
            <p class="author">John and Reena (23/12/2013)</p>
        </li>
         <li>
            <p class="quote">Just wanted to let you know the lenders agreed to giving us a loan on xxxxx xxxxx Road and have accepted the tenancy contract as it is.<br /><br />

So thank you and Bob for your advice when we needed it.<br /><br />

Regards.</p>
            <p class="author"> Cristina (Landlady) (15/112013) </p>
        </li>

         <li>
            <p class="quote">You made a difficult and stressful situation a lot easier and I thank you for that.<br /><br />

Perhaps if/when I need new students I call upon your services once again.<br /><br />

Keep well and good luck with the 'rush' now on! With very best regards.</p>
            <p class="author"> H. G. (Landlady) (16/8/2013)</p>
        </li>
         <li>
            <p class="quote">I am really impressed.<br /><br />

Yours is a really efficient and slick organisation with professional helpful staff.</p>
            <p class="author">Martin Minhall (Landlord) (22/10/2013)</p>
        </li>
        <li>
            <p class="quote">I just wanted to thank you all as well, by far the best, friendliest letting agency I've met so far and a million times better than any of the ones I had dealings with today.</p>
            <p class="author">Josh Gardner (30/07/2013)</p>
        </li>


    </ul>
</div>
<script>
jQuery('.bxslider2').bxSlider({
  mode: 'vertical',
  randomStart: 'true',
  slideMargin: 5,
  pager: false,
  auto: true,
});
</script>

<p></p>
<div class="usefullinfo visible-xs visible-sm">
    <h4>Useful Information</h4>

    <?php wp_nav_menu( array('menu' => 'usefulinfo','menu_id'=> 'usefulinfo' )); ?>
</div><!-- #UsefullInfo END -->


</div><!-- #tabs-1 END -->

<div id="tabs-2">
    <?php
    $fields=array(
        array('label'=>'<b>Power Sockets - (per room)</b>','type'=>'select','name'=>'_power_sockets'),
        array('label'=>'<b>TV License</b>','type'=>'select','name'=>'_tv_license','options'=>$_yesno),
        array('label'=>'<b>Outhouse (WC)</b>','type'=>'select','name'=>'_outhouse_wc','options'=>$_yesno),
        array('label'=>'<b>Outhouse (Bike Storage)</b>','type'=>'select','name'=>'_outhouse_bike','options'=>$_yesno),
        array('label'=>'<b>Utility Room</b>','type'=>'select','name'=>'_utility_room'),
        array('label'=>'<b>Shared Shower</b>','type'=>'select','name'=>'_shared_shower'),
        array('label'=>'<b>Ensuite</b>','type'=>'select','name'=>'_en_suite'),
        array('label'=>'<b>Shared Toilet</b>','type'=>'select','name'=>'_shared_toilet'),
        array('label'=>'<b>Shared Sink</b>','type'=>'select','name'=>'_shared_sink'),
        array('label'=>'<b>Shared Bath</b>','type'=>'select','name'=>'_shared_bath'),
        array('label'=>'<b>Boiler Type</b>','type'=>'text','name'=>'_boiler_type'),
        array('label'=>'<b>Cavity Insulation - Wall</b>','type'=>'select','name'=>'cavity_insulation_wall'),
        array('label'=>'<b>Cavity Insulation - Loft</b>','type'=>'select','name'=>'cavity_insulation_loft'),
        array('label'=>'<b>Driveway (Max Spaces)</b>','type'=>'select','name'=>'_driveway'),
        array('label'=>'<b>Parking (Permit - available from CCC)</b>','type'=>'select','name'=>'_parkingpermit'),
        array('label'=>'<b>Parking (On Road)</b>','type'=>'singlecheck','name'=>'_parking_road'),
        array('label'=>'<b>Parking (Road Bays)</b>','type'=>'singlecheck','name'=>'_parking_bay'),
        array('label'=>'<b>Garden Shed</b>','type'=>'select','name'=>'_garden_shed','options'=>$_yesno),
        array('label'=>'<b>Front Garden</b>','type'=>'select','name'=>'_front_garden','options'=>$_yesno),
        array('label'=>'<b>Rear Garden</b>','type'=>'select','name'=>'_rear_garden','options'=>$_yesno),
        array('label'=>'<b>Bus Routes</b>','type'=>'textarea','name'=>'_bus_routes'),
    );
    $fields2=array(
        array('label'=>'<b>Shared Shower</b>','type'=>'select','name'=>'_shared_shower'),
        array('label'=>'<b>Shared Toilet</b>','type'=>'select','name'=>'_shared_toilet'),
        array('label'=>'<b>Shared Sink</b>','type'=>'select','name'=>'_shared_sink'),
        array('label'=>'<b>Ensuite</b>','type'=>'select','name'=>'_en_suite'),
        array('label'=>'<b>Utility Room</b>','type'=>'select','name'=>'_utility_room'),
        array('label'=>'<b>Outhouse (WC)</b>','type'=>'select','name'=>'_outhouse_wc','options'=>$_yesno),
        array('label'=>'<b>Outhouse (Bike Storage)</b>','type'=>'select','name'=>'_outhouse_bike','options'=>$_yesno),
        array('label'=>'<b>Garden Shed</b>','type'=>'select','name'=>'_garden_shed','options'=>$_yesno),
        array('label'=>'<b>Front Garden</b>','type'=>'select','name'=>'_front_garden','options'=>$_yesno),
        array('label'=>'<b>Rear Garden</b>','type'=>'select','name'=>'_rear_garden','options'=>$_yesno),
        array('label'=>'<b>Power Sockets - (per room)</b>','type'=>'select','name'=>'_power_sockets'),
        array('label'=>'<b>TV License</b>','type'=>'select','name'=>'_tv_license','options'=>$_yesno),
        /* 
        array('label'=>'<b>Fridge</b>','type'=>'select','name'=>'_fridge'),
        array('label'=>'<b>Kitchen Type</b>','type'=>'select','name'=>'_kitchen_type'),
        array('label'=>'<b>Fridge</b>','type'=>'select','name'=>'_fridge'),
        array('label'=>'<b>Freezer</b>','type'=>'select','name'=>'_freezer'),
        array('label'=>'<b>Wash Machine</b>','type'=>'select','name'=>'_wash_machine'),
        array('label'=>'<b>Dryer</b>','type'=>'select','name'=>'_dryer'),
        array('label'=>'<b>Microwave</b>','type'=>'select','name'=>'_microwave'),
        array('label'=>'<b>Hobs</b>','type'=>'select','name'=>'_hobs'),
        array('label'=>'<b>Hood Extractor Fans</b>','type'=>'select','name'=>'_hood_extractor_fans'),
        array('label'=>'<b>Washer Dryer</b>','type'=>'select','name'=>'_washerdryer'),
        array('label'=>'<b>Dishwasher</b>','type'=>'select','name'=>'_dishwasher'),
        array('label'=>'<b>Kitchen Sinks</b>','type'=>'text','name'=>'_kitchen_sinks'),
        array('label'=>'<b>Oven Type</b>','type'=>'select','name'=>'_oven_type'),
        array('label'=>'<b>Floor Type</b>','type'=>'select','name'=>'_floor_type'),
        */
    );
    $fields3=array(
        array('label'=>'<b>Bus Routes</b>','type'=>'textarea','name'=>'_bus_routes'),
        array('label'=>'<b>Parking (Permit - available from CCC)</b>','type'=>'select','name'=>'_parkingpermit'),
        array('label'=>'<b>Parking (On Road)</b>','type'=>'singlecheck','name'=>'_parking_road'),
        /* WAS KITCHIN 
        array('label'=>'<b>Bills package</b>','type'=>'select','name'=>'_bills_package'),
        array('label'=>'<b>Telephone Line (incoming only)</b>','type'=>'select','name'=>'_telephone_line_bill','options'=>$_yesno),
        array('label'=>'<b>Broadband Wireless</b>','type'=>'select','name'=>'_broadband_wireless_bill','options'=>$_yesno),
        array('label'=>'<b>TV License</b>','type'=>'select','name'=>'_tv_license_bill','options'=>$_yesno),
        array('label'=>'<b>TV</b>','type'=>'select','name'=>'_tv_bill','options'=>$_yesno),
        array('label'=>'<b>Gas</b>','type'=>'select','name'=>'_gas_bill','options'=>$_yesno),
        array('label'=>'<b>Electricity</b>','type'=>'select','name'=>'_electricity_bill','options'=>$_yesno),
        array('label'=>'<b>TV Coaxial - to all rooms</b>','type'=>'select','name'=>'_tv_coaxial_bill','options'=>$_yesno),
        array('label'=>'<b>Sewerage</b>','type'=>'select','name'=>'_sewerage_bill','options'=>$_yesno),
        array('label'=>'<b>Water</b>','type'=>'select','name'=>'_water_bill','options'=>$_yesno),
        */
    );
    $fields5=array(
        array('label'=>'<b>Boiler Type</b>','type'=>'text','name'=>'_boiler_type'),
        array('label'=>'<b>Cavity Insulation - Wall</b>','type'=>'select','name'=>'cavity_insulation_wall'),
        array('label'=>'<b>Cavity Insulation - Loft</b>','type'=>'select','name'=>'cavity_insulation_loft'),
        /* WAS BILLS 
        array('label'=>'<b>Burglar Alarm</b>','type'=>'select','name'=>'_balarm','options'=>$_yesno),
        array('label'=>'<b>Smoke Alarm</b>','type'=>'select','name'=>'_salarm','options'=>$_yesno),
        array('label'=>'<b>Heat Detector</b>','type'=>'select','name'=>'_heatdet','options'=>$_yesno),
        array('label'=>'<b>Carbon Monoxide Detector</b>','type'=>'select','name'=>'_cardet','options'=>$_yesno),
        array('label'=>'<b>Fire Alarm Unit</b>','type'=>'select','name'=>'_fam','options'=>$_yesno),
        */
    );
    echo '<div class="house-info ">';
    echo '<h4 class="title htitle">House</h4>';
    print_fields_front($fields2,get_the_ID());
    echo '</div>';

   
    echo '<div class="kitchen-info">';
    echo '<h4 class="title  htitle">Travel</h4>';
    print_fields_front($fields3,get_the_ID());
    echo '</div>';

    

    echo '<div class="bills-info ">';
    echo '<h4 class="title  htitle">Energy</h4>';
    print_fields_front($fields5,get_the_ID());
    echo '</div>';

   

    /*
    echo '<div class="security-info">';

    echo '<h4 class="title  htitle">Security</h4>';
    print_fields_front($fields5,get_the_ID());
    echo '</div>';
 */
    $fields4=array(
        array('label'=>'<b>Thermostat</b>','type'=>'textarea','name'=>'_thermostat_location'),
        array('label'=>'<b>Hot Water and Heating Controls</b>','type'=>'textarea','name'=>'_hot_water_location'),
        array('label'=>'<b>Boiler</b>','type'=>'textarea','name'=>'_boiler_location'),
        array('label'=>'<b>Stop Cock</b>','type'=>'textarea','name'=>'_stop_cock_location'),
        array('label'=>'<b>Fuse Box</b>','type'=>'textarea','name'=>'_fuse_box_location'),
        array('label'=>'<b>Internet Box</b>','type'=>'textarea','name'=>'_internet_box_location'),
        array('label'=>'<b>Electric Meter</b>','type'=>'textarea','name'=>'_electric_meter_location'),
        array('label'=>'<b>Gas Meter</b>','type'=>'textarea','name'=>'_gas_meter_location'),
        array('label'=>'<b>Gas Detector</b>','type'=>'textarea','name'=>'_gas_detector_location')
    );
   
    ?>
</div><!-- #tabs-2 END -->

<div id="tabs-3">
    <?php
    $fields=array(
        array('label'=>'Location','type'=>'text','name'=>'_location'),
        array('label'=>'Size','type'=>'text','name'=>'_size'),
        array('label'=>'Rent','type'=>'text','name'=>'_rent'),
        array('label'=>'Availability this Academic year','type'=>'text','name'=>get_option('at_current_year')),
        array('label'=>'Availability next academic year','type'=>'text','name'=>get_option('at_next_year')),
        //array('label'=>'Mattress Type','type'=>'text','name'=>'_mattress_type'),
        array('label'=>'Bed Size','type'=>'text','name'=>'_bed_size'),
        array('label'=>'Desk and Chair','type'=>'radio','name'=>'_desk_chair','options'=>array('Yes','No')),
        array('label'=>'Wardrobe','type'=>'radio','name'=>'_wardrobe','options'=>array('Yes','No')),
        array('label'=>'Wardrobe inc Chest of Drawers','type'=>'radio','name'=>'_wardrobe_inc','options'=>array('Yes','No')),
        array('label'=>'Chest of Drawers','type'=>'radio','name'=>'_chest_drawers','options'=>array('Yes','No')),
        array('label'=>'Bedside Table','type'=>'radio','name'=>'_bedside_table','options'=>array('Yes','No')),
        array('label'=>'Bedroom lock','type'=>'radio','name'=>'_mortis_door_lock','options'=>array('Yes','No')),
        array('label'=>'Sink En-suite','type'=>'radio','name'=>'_sink_ensuite','options'=>array('Yes','No')),
        array('label'=>'Toilet En-suite','type'=>'radio','name'=>'_toilet_ensuite','options'=>array('Yes','No')),
        array('label'=>'Shower En-suite','type'=>'radio','name'=>'_shower_ensuite','options'=>array('Yes','No')),
        array('label'=>'Hard-wired Broadband','type'=>'radio','name'=>'_hard_wired_broadband','options'=>array('Yes','No')),
        array('label'=>'Wireless Broadband','type'=>'radio','name'=>'_wireless_broadband','options'=>array('Yes','No')),
        array('label'=>'Telephone Line','type'=>'radio','name'=>'_telephone_line','options'=>array('Yes','No')),
        array('label'=>'TV Aerial','type'=>'radio','name'=>'_tv_aerial','options'=>array('Yes','No')),
        array('label'=>'Floor','type'=>'text','name'=>'_floor'),
        array('label'=>'Disabled Friendly','type'=>'radio','name'=>'_disabled_friendly','options'=>array('Yes','No')),
    );
    echo '<h4 class="title  htitle">Room Details/Prices</h4>';
    echo '<div class="binfo ">';
    $_rooms = get_post_meta(get_the_ID(), '_rooms', true);
    if($_rooms){
        print_extra_fields_table_front($fields,get_the_ID());
    }
    echo '</div>';
    ?>
</div><!-- #tabs-3 END -->

<div id="tabs-4">
    <?php
    $key='_floorplans';
    $_value = get_post_meta($id, $key, true);
    $attids=explode(',',$_value);
    ?>

    <?php

    if(count($attids)>0)
    {
        foreach($attids as $attid){
            if(!empty($attid)){
                if(is_numeric($attid)){
                    $image = wp_get_attachment_image_src($attid, 'large');
                    $alt = get_post_meta($attid , '_wp_attachment_image_alt', true);

                    ?>
                    <a class="fancybox" rel="group" href="<?php echo $image[0] ?>"><img class="certimgs" src="<?php echo $image[0] ?>" alt="" /></a>

                <?php

                }

            }
        }
    }
    ?>

</div><!-- #tabs-4 END -->

<div id="tabs-5">
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery(".fancybox").fancybox();
        });
    </script>

    <?php
    $key2='_certificates_images';
    $_value2 = get_post_meta($id, $key2, true);
    $attids2=explode(',',$_value2);
    ?>

    <?php
    if(count($attids2)>0)
    {
        foreach($attids2 as $attid2){
            if(!empty($attid2)){
                if(is_numeric($attid2)){
                    $image2 = wp_get_attachment_image_src($attid2, $size);
                    if(!empty($image2))
                        ?>
                        <a class="fancybox" rel="group" href="<?php echo $image2[0] ?>"><img class="certimgs" src="<?php echo $image2[0] ?>" alt="" /></a>


                <?php
                }
                else
                {
                    ?>
                    <li><img src="<?php echo $attid2 ?>" /></li>
                <?php
                }
            }
        }
    }
    ?>
</div><!-- #tabs-5 END -->

<div id="tabs-6">
    <?php

    echo '<div class="emergency-info ">';
    echo '<h4 class="title  htitle emergency-header">Emergency Locations</h4>';
    print_fields_front($fields4,get_the_ID());
    echo '</div>';
    ?>
    <p></p>
</div><!-- #tabs-6 END -->
</div><!-- #tabs END -->

<footer class="disclaimer">
    <p>Leydon Letting Agents makes no guarantee as to the accuracy of these details.  Room measurements are approximate and are provided for guidance purposes only and should not be used as the sole basis for making any decision. Details provided are true to the best of our knowledge.</p>
    <p>Last updated: <?php the_modified_date('F j, Y'); ?> at <?php the_modified_date('G:i'); ?></p>
</footer>
</article><!-- #post -->
</div><!-- #innerpage -->
<?php 
    $prop_type = get_terms( 'type');
    if($prop_type[0]) { 
        $prop_holder = $prop_type[0]->slug;
    } 
    if($prop_type[1]) {
        $prop_holder = $prop_holder.'/'.$prop_type[1]->slug; 
    }
    $title_string = $_room_count.'bedroom'.$prop_holder.'House,'.$_postal_code;


?>