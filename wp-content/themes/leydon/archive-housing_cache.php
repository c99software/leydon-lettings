<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Thirteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
@session_start();
get_header(); ?>
    <script>
        $(document).ready(function(){

            function scroll () {
                if ($(window).scrollTop() >= 245) {
                    $('.tableheadbar').addClass('fixedlocktb');

                } else {
                    $('.tableheadbar').removeClass('fixedlocktb');

                }


            }

            document.onscroll = scroll;
           
        });

    </script>
    <script type="text/javascript">
        var geocoder;
        var map;
    </script>
<?php
$querystr = "
    SELECT $wpdb->posts.*,$wpdb->postmeta.* 
    FROM $wpdb->posts, $wpdb->postmeta
    WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id
    AND $wpdb->posts.post_status = 'publish' 
    AND $wpdb->posts.post_type = 'housing'
 ";

$querystr .= " 	GROUP BY $wpdb->posts.ID
	";
$querystr .= " ORDER BY $wpdb->posts.post_date DESC";

$pageposts = $wpdb->get_results($querystr, ARRAY_A);

?>
<?php foreach ($pageposts as $key=>$post):
    $rooms=get_post_meta($post['ID'], '_rooms', true);
    global $wpdb;
    global $post;
    $rentslowquery = "
    SELECT DISTINCT(CAST(meta_value AS UNSIGNED)) AS met 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '%_rent_%' AND post_id='".$post['ID']."'
	ORDER BY met ASC
	LIMIT 1
 ";
    $rentslow = $wpdb->get_results($rentslowquery, ARRAY_N);
    $rentshighquery = "
    SELECT DISTINCT(CAST(meta_value AS UNSIGNED)) AS met 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '%_rent_%' AND post_id='".$post['ID']."'
	ORDER BY met DESC
	LIMIT 1
 ";
    $rentshigh = $wpdb->get_results($rentshighquery, ARRAY_N);
    $availabilitythisquery = "
    SELECT count(*) 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '_availability_this_year_%' AND meta_value = 'Yes' AND post_id='".$post['ID']."'
 ";
    $availabilitythis = $wpdb->get_results($availabilitythisquery, ARRAY_N);
    $pageposts[$key]['rooms']=$rooms;
    $pageposts[$key]['pricelow']=$rentslow[0][0];
    $pageposts[$key]['pricehigh']=$rentshigh[0][0];
    $pageposts[$key]['avail']=$availabilitythis[0][0];
    ?>
<?php endforeach ?>
<?php

if($_GET['sort']=='price')
{
    if($_GET['dir']=='low')
    {

        $pageposts=subval_sort($pageposts,'pricelow','asc');
        //	$querystr .= " ORDER BY  `wp_postmeta`.`meta_value` ASC ";
    }
    else if($_GET['dir']=='all')
    {
        //subval_sort($a,$subkey,$sr);
    }
    else
    {
        $pageposts=subval_sort($pageposts,'pricelow','desc');
    }
}
else if($_GET['sort']=='beds')
{
    if($_GET['dir']=='low')
    {
        $pageposts=subval_sort($pageposts,'rooms','asc');
    }
    else if($_GET['dir']=='all')
    {
        //subval_sort($a,$subkey,$sr);
    }
    else
    {
        $pageposts=subval_sort($pageposts,'rooms','desc');
    }
}
else if($_GET['sort']=='avail')
{

    $pageposts=subval_sort($pageposts,'avail','desc');

}


// print_r($pageposts);
?>
    <div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">
    <div class="container">
    <div class="innerpage" id="fullsize">
    <p class="innerpagehead">Houses To Let - Results</p>
    <?php echo get_sidebar( 'search-bar' ); ?>              <!--viewhead-->

    <?php if ( $pageposts) : ?>

        <?php if(isset($_GET['results']) ){

            ?>
            <div class="grid">
                <ul>
                    <?php foreach ($pageposts as $po): ?>

                        <?php
                        $postquery = "SELECT $wpdb->posts.*,$wpdb->postmeta.* FROM $wpdb->posts, $wpdb->postmeta     WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id AND $wpdb->posts.ID = '".$po['ID']."' AND $wpdb->posts.post_status = 'publish' AND $wpdb->posts.post_type = 'housing'";
                        $postquery .= "GROUP BY $wpdb->posts.ID";
                        $postquery .= " ORDER BY $wpdb->posts.post_date DESC";
                        $pq = $wpdb->get_results($postquery, OBJECT);
                        foreach ($pq as $post):

                            ?>

                            <?php setup_postdata($post); ?>

                            <?php get_template_part( 'content', 'housing-grid' ); ?>
                        <?php endforeach;

                    endforeach; ?>
                </ul>

            </div>
        <?php
        }
        else if(isset($_GET['map']) )
        {

            ?>
            <script>
                // ======= Global variable to remind us what to do next
                var nextAddress = 0;
                var geocoder;
                var map;
                var addresses=[];
                var bounds;
                var latlng
                jQuery(function(){
                    geocoder = new google.maps.Geocoder();
                    latlng = new google.maps.LatLng(51.280277,1.079282);
                    var mapOptions = {
                        zoom: 14,
                        center: latlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }
                    map = new google.maps.Map(document.getElementById('mapview'), mapOptions);
                    bounds = new google.maps.LatLngBounds();
                    theNext();


                })

                // delay between geocode requests - at the time of writing, 100 miliseconds seems to work well
                var delay = 100;


                // ====== Create map objects ======
                var infowindow = new google.maps.InfoWindow();

                //  var geo = new google.maps.Geocoder();
                //  var map = new google.maps.Map(document.getElementById("mapview"), mapOptions);
                // var bounds = new google.maps.LatLngBounds();

                // ====== Geocoding ======
                function getAddress(searchi, next) {
                    nextAddress++;
                    var keyname_lat = searchi[0]+"key_lat";
                    var keyname_lng = searchi[0]+"key_lng";
                    var stored_lat = localStorage[keyname_lat];
                    var stored_lng = localStorage[keyname_lng];
                    if (stored_lat){

                        var lat = JSON.parse(stored_lat);
                        var lng = JSON.parse(stored_lng);
                        createMarker(searchi[0],lat,lng,searchi[1],searchi[2]);
                        next();
                    } 
                    else
                    {
                    geocoder.geocode({address:searchi[0]}, function (results,status)
                        {

                            //console.log(searchi[0]);
                            // If that was successful
                            if (status == google.maps.GeocoderStatus.OK) {
                                // Lets assume that the first marker is the one we want
                                var p = results[0].geometry.location;
                                var lat=p.lat();
                                var lng=p.lng();
                                // Output the data
                                var msg = 'address="' + searchi[0]+ '" lat=' +lat+ ' lng=' +lng+ '(delay='+delay+'ms)<br>';
                                //   console.log(msg)
                                // Create a mamsgrker
                                createMarker(searchi[0],lat,lng,searchi[1],searchi[2]);


                                var cache_data = [];
                                cache_data[0] = searchi[0]+"key_lat";
                                cache_data[1] = searchi[0]+"key_lng";
                                cache_data[2] = lat=p.lat();
                                cache_data[3] = lng=p.lng();
                                
                                localStorage[cache_data[0]] = cache_data[2];
                                localStorage[cache_data[1]] = cache_data[3];
                            }
                            // ====== Decode the error status ======
                            else {
                                // === if we were sending the requests to fast, try this one again and increase the delay
                                if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                                    var reason="Code "+status;
                                    var msg = 'address="' + searchi[0] + '" error=' +reason+ '(delay='+delay+'ms)<br>';
                                    //console.log(msg)
                                    nextAddress--;
                                    delay++;
                                } else {
                                    var reason="Code "+status;
                                    var msg = 'address="' + searchi[0] + '" error=' +reason+ '(delay='+delay+'ms)<br>';
                                    //console.log(msg)
                                    // document.getElementById("messages").innerHTML += msg;
                                }
                            }
                            next();
                        }

                    );
                    }
                }

                // ======= Function to create a marker
                function createMarker(add,lat,lng,infoss,image) {
                    var contentString = infoss;
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(lat,lng),
                        map: map,
                        icon: image,
                        zIndex: Math.round(latlng.lat()*-100000)<<5
                    });

                    //when the map zoom changes, resize the icon based on the zoom level so the marker covers the same geographic area
                    google.maps.event.addListener(map, 'zoom_changed', function() {

                        var pixelSizeAtZoom0 = 2; //the size of the icon at zoom level 0
                        var maxPixelSize = 100; //restricts the maximum size of the icon, otherwise the browser will choke at higher zoom levels trying to scale an image to millions of pixels

                        var zoom = map.getZoom();
                        var relativePixelSize = zoom * 10; // use 2 to the power of current zoom to calculate relative pixel size.  Base of exponent is 2 because relative size should double every time you zoom in

                        if(relativePixelSize > maxPixelSize) //restrict the maximum size of the icon
                            relativePixelSize = maxPixelSize;

                        //change the size of the icon
                        marker.setIcon(
                            new google.maps.MarkerImage(
                                marker.getIcon().url, //marker's same icon graphic
                                null,//size
                                null,//origin
                                null, //anchor
                                new google.maps.Size(relativePixelSize, relativePixelSize) //changes the scale
                            )
                        );        
                    });


                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.setContent(contentString);
                        infowindow.open(map,marker);
                    });

                    bounds.extend(marker.position);

                }




                // ======= Function to call the next Geocode operation when the reply comes back

                function theNext() {
                    console.log(nextAddress);
                    if (nextAddress < addresses.length) {

                        setTimeout(getAddress(addresses[nextAddress],theNext), delay);


                        // console.log(nextAddress)
                    } else {
                        // We're done. Show map bounds
                        //   map.fitBounds(bounds);
                    }
                }
                //	setTimeout(function() { theNext(); }, 1000);
                // ======= Call that function for the first time =======


                // This Javascript is based on code provided by the
                // Community Church Javascript Team
                // http://www.bisphamchurch.org.uk/
                // http://econym.org.uk/gmap/

                //]]>
                var add=['CT2 7NZ','','<?php echo get_bloginfo('template_url') ?>/images/kent-logo-map.png']
                addresses.push(add);
                var add=['CT1 1QU','','<?php echo get_bloginfo('template_url') ?>/images/cccu-logo-map.png']
                addresses.push(add);
                var add=['CT1 3AN','','<?php echo get_bloginfo('template_url') ?>/images/uca-logo-map.png']
                addresses.push(add);
            </script>
            <div class="list map" id="mapview">
            </div>
            <?php foreach ($pageposts as $po): ?>

            <?php
            $postquery = "SELECT $wpdb->posts.*,$wpdb->postmeta.* FROM $wpdb->posts, $wpdb->postmeta     WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id AND $wpdb->posts.ID = '".$po['ID']."' AND $wpdb->posts.post_status = 'publish' AND $wpdb->posts.post_type = 'housing'";
            $postquery .= "GROUP BY $wpdb->posts.ID";
            $postquery .= " ORDER BY $wpdb->posts.post_date DESC";
            $pq = $wpdb->get_results($postquery, OBJECT);
            foreach ($pq as $post):


                ?>
                <?php setup_postdata($post); ?>
                <?php get_template_part( 'content', 'housing-map' ); ?>
            <?php endforeach; ?>
        <?php endforeach; ?>

        <?php

        }
        else {
            ?><?php $current2year=date("y"); //14
            $current4year=date("Y"); //2014
            $curstrtime=strtotime("15 May ".$current2year);
            if(strtotime("now")>=$curstrtime)
            {
                $newyear2=$current2year+1;
                $newyear4=$current4year+1;
            }
            else {
                $newyear2=$current2year;
                $newyear4=$current4year;
            }

            ?>
         <div class="flipTable_header">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                House Address
            </div>
             <div class="col-lg-3 col-md-3 hidden-xs hidden-sm">
                 House Includes
            </div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
                Postcode
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                Rooms Available 2014-2015
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                Rooms Available 2015-2016
            </div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
                Distance from <?php 
                if ($loc == "_kent"){ echo "UKC";}
                else if ($loc == "_ucl"){ echo "UCL";}
                else if ($loc == "_uca"){ echo "UCA";}
                else if ($loc == "_canterbury"){ echo "Canterbury City Centre";}
                else { echo "UKC"; }               ?>
            </div>
        </div>
              
              
                <div class="flipTable">
                    <?php
                    $x=0;
                    if ( $pageposts )
                    {
                        foreach ($pageposts as $po): ?>

                            <?php
                            $postquery = "SELECT $wpdb->posts.*,$wpdb->postmeta.* FROM $wpdb->posts, $wpdb->postmeta     WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id AND $wpdb->posts.ID = '".$po['ID']."' AND $wpdb->posts.post_status = 'publish' AND $wpdb->posts.post_type = 'housing'";
                            $postquery .= "GROUP BY $wpdb->posts.ID";
                            $postquery .= " ORDER BY $wpdb->posts.post_date DESC";
                            $pq = $wpdb->get_results($postquery, OBJECT);
                            foreach ($pq as $post):
                                ?>
                                <?php setup_postdata($post); ?>

                                <?php get_template_part( 'content', 'housing-list' ); ?>
                                <?php
                                $x++;
                            endforeach; ?>
                        <?php endforeach;

                    }
                    else
                    {
                        ?>
                        <tr> <td colspan="8">Sorry there are no properties matching your search. Please adjust your criteria and search again.</td></tr>
                    <?php
                    }?>
                   
                </div>

        <?php
        } ?>


    <?php else : ?>
        <?php get_template_part( 'content', 'none' ); ?>
    <?php endif; ?>


    </div><!--innerpage-->
    </div>
    </div><!-- #content -->
    </div><!-- #primary -->
   <script>
        jQuery(document).ready(function(){
            jQuery('body').append('<div style="position:fixed; top:500px; padding:6px 15px;right:0px; z-index:999;" id="flip_all"> Show Rooms </div>')
            $('.flipTable_item').mouseenter(function() {
                jQuery(this).find('.front').fadeOut();
                jQuery(this).find('.back').fadeIn();
            }).mouseleave(function() {
                if(!jQuery('#flip_all').hasClass('active')){
                jQuery(this).find('.back').fadeOut();
                jQuery(this).find('.front').fadeIn();
                }
            });
            jQuery('#flip_all').click(function() {
                if(jQuery(this).hasClass('active')){
                    jQuery(this).text('Show Rooms');
                    jQuery(this).removeClass('active');
                    jQuery(".flipTable_item").flip(false);
                    jQuery('.back').fadeOut();
                    jQuery('.front').fadeIn();
                }
                else{
                    jQuery(this).text('Hide Rooms');
                    jQuery(this).addClass('active');
                    jQuery(".flipTable_item").flip(true);
                    jQuery('.back').fadeIn();
                    jQuery('.front').fadeOut();
                }
                
            });
         

           
        });
</script>

<?php get_footer(); ?>