<?php /* Template Name: Viewing Property Template  */
get_header(); ?>


<div class="container">
    <?php get_sidebar('contact');

    ?>

    <div class="innerpage">

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <h5 class="innerpagehead"><?php the_title(); ?></h5>

            <div class="entry-content">
                <div class="tabel-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr style="border-bottom: 1px solid #ddd;height: 25px;background: rgba(3, 66, 126, 0.96);">
                            <th style="font-weight:bold;text-transform: uppercase;">viewing times</th>
                            <th style="font-weight:bold;text-transform: uppercase;">property address</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        /*$_varray=array('Monday 12 till 2','Monday 3 till 5','Tuesday 12 till 2','Tuesday 3 till 5','Wednesday 12 till 2','Wednesday 3 till 5','Thursday 12 till 2','Thursday 3 till 5','Friday 12 till 2','Friday 3 till 5');*/
                        $_varray=array('Monday 12 till 2','Monday 3 till 5','Tuesday 12 till 2','Tuesday 3 till 5','Wednesday 12 till 2','Wednesday 3 till 5','Thursday 12 till 2','Thursday 3 till 5','Friday 12 till 2','Friday 3 till 5');
                        foreach($_varray as $times)
                        {
                            ?>
                            <tr>


                                <td id="<?php echo str_replace(' ','',$times); ?>" class="text-left">
                                    <?php echo strtoupper($times); ?>
                                </td>
                                <td class="text-left" rel="<?php echo str_replace(' ','',$times); ?>">                         <?php global $wpdb;
                                    $old_query = " SELECT * FROM {$wpdb->prefix}posts
INNER JOIN {$wpdb->prefix}postmeta m1
  ON ( {$wpdb->prefix}posts.ID = m1.post_id )
INNER JOIN {$wpdb->prefix}postmeta m2
  ON ( {$wpdb->prefix}posts.ID = m2.post_id )
WHERE
{$wpdb->prefix}posts.post_type = 'housing'
AND {$wpdb->prefix}posts.post_status = 'publish' AND (
( m1.meta_key = '_vtime1' AND m1.meta_value = '".$times."' )
OR ( m2.meta_key = '_vtime2' AND m2.meta_value = '".$times."' )) GROUP BY {$wpdb->prefix}posts.ID
ORDER BY {$wpdb->prefix}posts.post_date
DESC;
";                                  $pref=$wpdb->prefix;
                                    $query="SELECT * from {$pref}posts p left join {$pref}postmeta pm on (p.ID=pm.post_id) where p.post_type='housing' AND p.post_status='publish' and ((pm.meta_key='_vtime1' and pm.meta_value='".$times."') or (pm.meta_key='_vtime2' and pm.meta_value='".$times."')) order by p.post_date DESC; ";

                                    $pageposts = $wpdb->get_results($query, OBJECT);
                                    ?>
                                    <?php if ($pageposts): ?>
                                        <?php global $post; ?>
                                        <?php foreach ($pageposts as $post): ?>

                                            <?php
                                            setup_postdata($post);
                                            //var_dump($post);
                                            ?>
                                            <div class="property-detail-item">
                                            <a href="<?php the_permalink() ?>"> <?php the_title() ?> , <?php echo get_post_meta(get_the_ID(), '_postal_code', true); ?> <span class="pull-right glyphicon glyphicon-search"></span></a>
                                            </div>
                                        <?php endforeach; ?>

                                    <?php else : ?>
                                        Sorry, but you are looking for something that isn't here.

                                    <?php endif;   wp_reset_query(); ?>

                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div><!-- .entry-content -->
        </article><!-- #post -->

    </div><!--innerpage-->

</div><!--contentallign-->

<script type='text/javascript' src='/wp-content/plugins/wordpress-faq-manager/inc/js/faq.init.js?ver=1.331'></script>
<?php get_footer();?></div><!--content-->