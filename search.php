<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
@session_start();
error_reporting(E_ERROR);
if(isset($_GET['table']))
{
    $br='table';
    $_SESSION['view']='table';
}
if(isset($_GET['map']))
{
    $br='map';
    $_SESSION['view']='map';
}
if(isset($_GET['results']))
{
    $br='results';
    $_SESSION['view']='results';
}
get_header(); ?>

<?php

if($_GET['s']=='housing'){

    
    $property_type=trim(mysql_real_escape_string($_GET['property-type']));
    $querystr = "SELECT * FROM $wpdb->posts LEFT JOIN $wpdb->postmeta m5 ON($wpdb->posts.ID = m5.post_id) ";

    if(isset($_GET['disability'])){
            $querystr .="LEFT JOIN $wpdb->postmeta m1 ON($wpdb->posts.ID = m1.post_id) "; 
    }
    if(isset($_GET['garden'])){
            $querystr .="LEFT JOIN $wpdb->postmeta m2 ON($wpdb->posts.ID = m2.post_id) ";
    }
    if(isset($_GET['shower'])){
            $querystr .="LEFT JOIN $wpdb->postmeta m3 ON($wpdb->posts.ID = m3.post_id) ";
    }
    if(isset($_GET['car'])){
            $querystr .="LEFT JOIN $wpdb->postmeta m4 ON($wpdb->posts.ID = m4.post_id) "; 
    }
    if(isset($_GET['shared_toilet'])){
                $querystr .="LEFT JOIN $wpdb->postmeta m6 ON($wpdb->posts.ID = m6.post_id) "; 
    }
    if(isset($_GET['shared_bath'])){
                $querystr .="LEFT JOIN $wpdb->postmeta m7 ON($wpdb->posts.ID = m7.post_id) "; 
    }
    
    if(!isset($_GET['available'])){
                $querystr .="LEFT JOIN $wpdb->postmeta m8 ON($wpdb->posts.ID = m8.post_id) "; 
    }
    $querystr .="LEFT JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)
	             LEFT JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
	             LEFT JOIN $wpdb->terms ON($wpdb->term_taxonomy.term_id = $wpdb->terms.term_id) WHERE $wpdb->posts.post_type = 'housing' AND $wpdb->posts.post_status = 'publish'  ";

    if(isset($_GET['property-type'])){
            $property_type = $_GET['property-type'];
            if ($property_type != "0"){
                $querystr .= " AND $wpdb->terms.slug = '".$property_type."'AND $wpdb->term_taxonomy.taxonomy = 'type'";        
            }
    }
  
    if(isset($_GET['disability'])){
            $querystr .= "	AND m1.meta_key = '_Wheelchair' AND m1.meta_value = 'Yes' ";
    }
    if(isset($_GET['shower'])){
            $querystr .= "	AND m3.meta_key = '_en_suite' AND m3.meta_value >= '0' ";
    }
    if(isset($_GET['car'])){
            $querystr .= "	AND m4.meta_key = '_parking_bay' AND m4.meta_value = 'Yes' ";
    }
    if(isset($_GET['garden'])){
            $querystr .= "	AND m2.meta_key = '_rear_garden' AND m2.meta_value = 'Yes' ";
        }
    if($minprice){
            $querystr .= "	AND m5.meta_key LIKE '_rent_%' AND m5.meta_value >= '".$minprice."' ";
    }
    if(isset($_GET['shared_bath'])){
            $querystr .= "  AND m7.meta_key LIKE '_shared_bath%' AND m7.meta_value >= '0' ";
    }
    if(isset($_GET['shared_toilet'])){
            $querystr .= "  AND m6.meta_key LIKE '_shared_toilet%' AND m6.meta_value >= '0' ";
    }
    if(!isset($_GET['available'])){
            $querystr .= "	AND m8.meta_key LIKE '_availability_this_year_%' AND m8.meta_value = 'Yes' ";
    }


    if($_GET['sort']=='price'){
            $querystr .= "	OR m5.meta_key LIKE '_rent_%'";
    }
    $minbeds = $_GET['minbeds'];
    $maxbeds = $_GET['maxbeds'];
    if($minbeds&&$minbeds!='all') {
            $querystr .= "	AND m5.meta_key LIKE '_rooms' AND m5.meta_value = '".$minbeds."' ";
    }
    $querystr .= " AND $wpdb->posts.post_status = 'publish'	GROUP BY $wpdb->posts.ID";

    $pageposts = $wpdb->get_results($querystr, ARRAY_A);
    ?>
    <?php foreach ($pageposts as $key=>$post):
        $rooms=get_post_meta($post['ID'], '_rooms', true);
        global $wpdb;
        global $post;
        $rentslowquery = "
    SELECT DISTINCT(CAST(meta_value AS UNSIGNED)) AS met 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '%_rent_%' AND post_id='".$post['ID']."'
	ORDER BY met ASC
	LIMIT 1
 ";
        $rentslow = $wpdb->get_results($rentslowquery, ARRAY_N);
        $rentshighquery = "
    SELECT DISTINCT(CAST(meta_value AS UNSIGNED)) AS met 
    FROM $wpdb->postmeta
    WHERE meta_key LIKE '%_rent_%' AND post_id='".$post['ID']."'
	ORDER BY met DESC
	LIMIT 1
 ";
        $rentshigh = $wpdb->get_results($rentshighquery, ARRAY_N);
        $pageposts[$key]['rooms']=$rooms;
        $pageposts[$key]['pricelow']=$rentslow[0][0];
        $pageposts[$key]['pricehigh']=$rentshigh[0][0];

        ?>
        <?php
        if(!empty($_GET['locations']))
        {
            $loc=$_GET['locations'];
        }
        else
        {
            $loc='_kent';
        }
        $distance= get_post_meta($post['ID'],$loc , true);
        $pageposts[$key]['distance']=$distance;
        ?>

    <?php endforeach;
    if(isset($_GET['dir']))
    {
        if($_GET['dir']=='low')
        {

            $pageposts=subval_sort($pageposts,'pricelow','asc');
            //	$querystr .= " ORDER BY  `wp_postmeta`.`meta_value` ASC ";
        }
        else if($_GET['dir']=='all')
        {
            //subval_sort($a,$subkey,$sr);
        }
        else
        {
            $pageposts=subval_sort($pageposts,'pricelow','desc');
        }
    }
    else if($_GET['sort']=='beds')
    {
        if($_GET['dir']=='low')
        {
            $pageposts=subval_sort($pageposts,'rooms','asc');
        }
        else if($_GET['dir']=='all')
        {
            //subval_sort($a,$subkey,$sr);
        }
        else
        {

        }
    }

    $pageposts=subval_sort($pageposts,'distance','asc');
// print_r($pageposts);
    ?>

    <?php if(isset($_GET['results'])||$_SESSION['view']=='results'){		 ?>
        <div class="container">
            <ol class="breadcrumb"><li><a href="http://leydon.code99.co.uk">Home</a></li><li>Property Search Results - By Picture</li></ol>
        </div>
        <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
        <div class="container">
        <div class="innerpage" id="fullsize">
        <p class="innerpagehead">
            Student Houses To Let - Results</p>
        <?php echo get_sidebar( 'search-bar' ); ?>
        <div class="grid">
            <ul>
                <?php foreach ($pageposts as $po): ?>

                    <?php
                    $postquery = "SELECT $wpdb->posts.*,$wpdb->postmeta.* FROM $wpdb->posts, $wpdb->postmeta     WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id AND $wpdb->posts.ID = '".$po['ID']."' AND $wpdb->posts.post_status = 'publish' AND $wpdb->posts.post_type = 'housing'";
                    $postquery .= "GROUP BY $wpdb->posts.ID";
                    $postquery .= " ORDER BY $wpdb->posts.post_date DESC";
                    $pq = $wpdb->get_results($postquery, OBJECT);
                    foreach ($pq as $post):

                        ?>

                        <?php setup_postdata($post); ?>

                        <?php  get_template_part( 'content', 'housing-grid' ); ?>
                    <?php endforeach;

                endforeach; ?>
            </ul>

        </div>
    <?php }
    else if(isset($_GET['map'])||$_SESSION['view']=='map')
    {
        ?>
        <div class="container">
            <ol class="breadcrumb"><li><a href="http://leydon.code99.co.uk">Home</a></li><li>Property Search Results - By Map</li></ol>
        </div>
        <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
        <div class="container">
        <div class="innerpage" id="fullsize">
        <p class="innerpagehead">
            Student Houses To Let - Results</p>
        <?php echo get_sidebar( 'search-bar' ); ?>
        <script>
                // ======= Global variable to remind us what to do next
                var master_store;
                var nextAddress = 0;
                var geocoder;
                var map;
                var addresses=[];
                var bounds;
                var latlng
                var lat_cache;
                var lng_cache; 
                var markerCount = 0;
                var markers = [];
                var last_zoom;
                var_len
                $.getJSON( "/wp-content/themes/leydon/new_map_data.json", function(data) {
                    master_store = data;
                });
                
                console.log("testsearch")

                  
                 jQuery(function(){
                    geocoder = new google.maps.Geocoder();
                    latlng = new google.maps.LatLng(51.280277,1.079282);
                    var styles = [
                        {
                            featureType: 'poi.school',
                            elementType: 'geometry.fill',
                            stylers: [
                                { color: '#4d6278'},
                                { lightness: '50'}
                            ]
                        },
                        {
                            featureType: 'poi.school',
                            elementType: 'geometry.stroke',
                            stylers: [
                                { color: '#4d6278'}
                            ]
                        },
                    ];
                    var mapOptions = {
                         mapTypeControlOptions: {
                                mapTypeIds: ['Styled']
                            },
                        zoom: 14,
                        center: latlng,
                        mapTypeId: 'Styled'
                    }
                    map = new google.maps.Map(document.getElementById('mapview'), mapOptions);
                    bounds = new google.maps.LatLngBounds();
                    var styledMapType = new google.maps.StyledMapType(styles, { name: 'Styled' });
                    map.mapTypes.set('Styled', styledMapType);
                    


                })
                theNext();

                // delay between geocode requests - at the time of writing, 100 miliseconds seems to work well
                var delay = 100;


                // ====== Create map objects ======
                var infowindow = new google.maps.InfoWindow();

                //  var geo = new google.maps.Geocoder();
                //  var map = new google.maps.Map(document.getElementById("mapview"), mapOptions);
                // var bounds = new google.maps.LatLngBounds();

                // ====== Geocoding ======
                function success(){
                }
                function findPurpose(purposeName) {
                        for (var i = 0,var len = master_store.length; i < len; i++) {
                            if (master_store[i]["address"]["key"] == purposeName){
                                lat_cache =  master_store[i]["address"]["lat"];
                                lng_cache =  master_store[i]["address"]["lng"];
                                return true;
                            }                 
                        }
                }
               
                function getAddress(searchi, next) {
                    
                    var cache_check = false;
                    var key = searchi[0].replace(/ /g,'');
                    nextAddress++;
                   
                    var check_cache = findPurpose(key);
                    
                    if(check_cache) { 

                        createMarker(searchi[0],lat_cache,lng_cache,searchi[1],searchi[2]);                        
                        next();
                    
                      } 
                    else
                    {
                    geocoder.geocode({address:searchi[0]}, function (results,status)
                        {


                            //console.log(searchi[0]);
                            // If that was successful
                            if (status == google.maps.GeocoderStatus.OK) {
                                // Lets assume that the first marker is the one we want
                                var p = results[0].geometry.location;
                                var lat=p.lat();
                                var lng=p.lng();
                                // Output the data
                                var msg = 'address="' + searchi[0]+ '" lat=' +lat+ ' lng=' +lng+ '(delay='+delay+'ms)<br>';
                                //   console.log(msg)
                                // Create a mamsgrker
                                createMarker(searchi[0],lat,lng,searchi[1],searchi[2]);


                                var cache_data = [];
                                cache_data[0] = searchi[0]+"key_lat";
                                cache_data[1] = searchi[0]+"key_lng";
                                cache_data[2] = lat=p.lat();
                                cache_data[3] = lng=p.lng();

                                var temp_store = 
                                    {
                                    "address":
                                        {
                                            "lat":[lat=p.lat()],
                                            "lng":[temp_storelng=p.lng()],
                                            "key":[key]
                                        }
                                    }

                                
                                master_store = $.merge(master_store,[temp_store])
                                
                                var to_send = JSON.stringify(master_store);
                                
                                if (temp_store != null){
                                       
                                    $.ajax({
                                          type: "POST",
                                          url: "/wp-content/themes/leydon/json.php",
                                          data: to_send,
                                          success: success,
                                           dataType: "json"
                                        });
                                }
                                
                            }
                            // ====== Decode the error status ======
                            else {
                                // === if we were sending the requests to fast, try this one again and increase the delay
                                if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
                                    var reason="Code "+status;
                                    var msg = 'address="' + searchi[0] + '" error=' +reason+ '(delay='+delay+'ms)<br>';
                                    //console.log(msg)
                                    nextAddress--;
                                    delay++;
                                } else {
                                    var reason="Code "+status;
                                    var msg = 'address="' + searchi[0] + '" error=' +reason+ '(delay='+delay+'ms)<br>';
                                    //console.log(msg)
                                    // document.getElementById("messages").innerHTML += msg;
                                }
                            }
                            next();
                        }

                    );
                    }
                }

                // ======= Function to create a marker
                function createMarker(add,lat,lng,infoss,image) {

                    var contentString = infoss;
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(lat,lng),
                        map: map,
                        icon: image,
                        zIndex: Math.round(latlng.lat()*-100000)<<5
                    }); 
                    markers[markerCount] = marker;


                    //when the map zoom changes, resize the icon based on the zoom level so the marker covers the same geographic area
                    


                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.setContent(contentString);
                        infowindow.open(map,marker);
                    });

                    bounds.extend(marker.position);
                    markerCount++;


                   google.maps.event.addListener(map, 'zoom_changed', function() {
                        var z = map.getZoom();

                        $.each(markers, function(s) {

                            if (! $.isFunction(s.getPosition)) return;

                            var w = s.getIcon().size.width;
                            var h = s.getIcon().size.height;

                            s.shape.setIcon(new google.maps.MarkerImage(
                                s.shape.getIcon().url, null, null, null, new google.maps.Size(
                                    w - Math.round(w / 12 * (last_zoom - z)),
                                    h - Math.round(h / 12 * (last_zoom - z)))
                                )
                            );

                        });

                        last_zoom = z;
                    });
                }


                var add=['CT2 8AN','','<?php echo get_bloginfo('template_url') ?>/images/station.png']
                addresses.push(add);
                var add=['CT1 2RB','','<?php echo get_bloginfo('template_url') ?>/images/station.png']
                addresses.push(add);
                var add=['CT2 7NZ','','<?php echo get_bloginfo('template_url') ?>/images/kent-logo-map.png']
                addresses.push(add);
                var add=['CT1 1QU','','<?php echo get_bloginfo('template_url') ?>/images/cccu-logo-map.png']
                addresses.push(add);
                var add=['CT1 3AN','','<?php echo get_bloginfo('template_url') ?>/images/uca-logo-map.png']
                addresses.push(add);

                // ======= Function to call the next Geocode operation when the reply comes back

                function theNext() {
                    if (nextAddress < addresses.length) {

                        setTimeout(getAddress(addresses[nextAddress],theNext), delay);


                        // console.log(nextAddress)
                    } else {
                        // We're done. Show map bounds
                        //   map.fitBounds(bounds);
                    }
                }
                //  setTimeout(function() { theNext(); }, 1000);
                // ======= Call that function for the first time =======


                // This Javascript is based on code provided by the
                // Community Church Javascript Team
                // http://www.bisphamchurch.org.uk/
                // http://econym.org.uk/gmap/

                //]]>

                

                
            </script>
        <div class="list map" id="mapview">
        </div>
        <?php foreach ($pageposts as $po): ?>
        <?php
        $postquery = "SELECT $wpdb->posts.*,$wpdb->postmeta.* FROM $wpdb->posts, $wpdb->postmeta     WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id AND $wpdb->posts.ID = '".$po['ID']."' AND $wpdb->posts.post_status = 'publish' AND $wpdb->posts.post_type = 'housing'";
        $postquery .= "GROUP BY $wpdb->posts.ID";
        $postquery .= " ORDER BY $wpdb->posts.post_date DESC";
        $pq = $wpdb->get_results($postquery, OBJECT);
        foreach ($pq as $post):
            ?>
            <?php setup_postdata($post); ?>
            <?php get_template_part( 'content', 'housing-map' ); ?>
        <?php endforeach; ?>
    <?php endforeach; ?>

    <?php
    }
    else {?>
        <div class="container">
            <ol class="breadcrumb"><li><a href="http://leydon.code99.co.uk">Home</a></li><li>Property Search Results - By Table</li></ol>
        </div>
        <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
        <div class="container">
        <div class="innerpage" id="fullsize">
        <p class="innerpagehead">
            Student Houses To Let - Results</p>
        <?php echo get_sidebar( 'search-bar' ); ?>
       
        <div class="flipTable_header">
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
                House Address
            </div>
             <div class="col-lg-3 col-md-3 hidden-xs hidden-sm">
                 House Includes
            </div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3">
                Postcode
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                Rooms Available 2014-2015
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                Rooms Available 2015-2016
            </div>
            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
                Distance from <?php 
                if ($loc == "_kent"){ echo "UKC";}
                else if ($loc == "_ucl"){ echo "CCCU";}
                else if ($loc == "_uca"){ echo "UCA";}
                else if ($loc == "_canterbury"){ echo "Canterbury City Centre";}
                else { echo "UKC"; }               ?>
            </div>
        </div>
        <div class="flipTable">
                <?php
                if($pageposts){
                    foreach ($pageposts as $po): ?>

                        <?php
                        $postquery = "SELECT $wpdb->posts.*,$wpdb->postmeta.* FROM $wpdb->posts, $wpdb->postmeta     WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id AND $wpdb->posts.ID = '".$po['ID']."' AND $wpdb->posts.post_status = 'publish' AND $wpdb->posts.post_type = 'housing'";
                        $postquery .= "GROUP BY $wpdb->posts.ID";
                        $postquery .= " ORDER BY $wpdb->posts.post_date DESC";
                        $pq = $wpdb->get_results($postquery, OBJECT);
                        foreach ($pq as $post):
                            ?>
                            <?php setup_postdata($post); ?>

                            <?php get_template_part( 'content', 'housing-list' ); ?>
                        <?php endforeach; ?>
                    <?php endforeach;}
                else
                {
                    ?>
                    <tr style="background-color:#fff !important;"> <td colspan="9">Sorry there are no properties matching your search. Please adjust your criteria and search again.</td></tr>
                <?php
                }?>
             
            
        </div>
    <?php } ?>




     <script>
        jQuery(document).ready(function(){
            jQuery('body').append('<div style="position:fixed; top:500px; padding:6px 15px;right:0px; z-index:999;" id="flip_all"> Show Prices </div>')
            $('.flipTable_item').mouseenter(function() {
                jQuery(this).find('.front').fadeOut();
                jQuery(this).find('.back').fadeIn();
            }).mouseleave(function() {
                if(!jQuery('#flip_all').hasClass('active')){
                jQuery(this).find('.back').fadeOut();
                jQuery(this).find('.front').fadeIn();
                }
            });
            jQuery('#flip_all').click(function() {
                if(jQuery(this).hasClass('active')){
                    jQuery(this).text('Show Prices');
                    jQuery(this).removeClass('active');
                    jQuery(".flipTable_item").flip(false);
                    jQuery('.back').fadeOut();
                    jQuery('.front').fadeIn();
                }
                else{
                    jQuery(this).text('Hide Prices');
                    jQuery(this).addClass('active');
                    jQuery(".flipTable_item").flip(true);
                    jQuery('.back').fadeIn();
                    jQuery('.front').fadeOut();
                }
                
            });
         

           
        });
</script>

    </div><!--innerpage-->
    </div>
    </div><!-- #content -->
    </div><!-- #primary -->

<?php } else { ?>
    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
            <div class="container">
                <div class="innerpage" id="fullsize">
                    <?php if ( have_posts() ) : ?>

                        <header class="page-header">

                            <p class="innerpagehead"><?php printf( __( 'Search Results for: %s', 'twentythirteen' ), get_search_query() ); ?></p>
                        </header>

                        <?php /* The loop */ ?>
                        <?php while ( have_posts() ) : the_post(); ?>
                            <?php get_template_part( 'content', get_post_format() ); ?>
                        <?php endwhile; ?>

                        <?php twentythirteen_paging_nav(); ?>

                    <?php else : ?>
                        <?php get_template_part( 'content', 'none' ); ?>
                    <?php endif; ?>
                </div>
            </div><!--contentallign-->
        </div><!-- #content -->
    </div>
<?php } ?>
<?php get_footer(); ?>